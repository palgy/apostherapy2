﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace WebApp1
{
    public partial class wfrmAppointment : System.Web.UI.Page
    {
        private static Rec2Load rec2Load = new Rec2Load();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PrApiCalls.initRestClient();
                PanelAppointment.Visible = false;
                txtAppointmentDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                rec2Load.AppointMentDate = DateTime.Today;

                // see: https://stackoverflow.com/questions/22745661/how-to-set-the-value-of-a-textbox-textmode-date-asp-net-c-sharp
                // afaik, the new textbox in .Net with textmode=date only supports the YYYY-MM-DD format.
            }
            else
            {
                if (Request.Form["__EVENTTARGET"] == "txtPassword" && string.IsNullOrEmpty(txtPassword.Text))
                {
                    lblPwdErr.Text = "Please enter your password";
                    lblPwdErr.ForeColor = Color.Red;
                    lblPwdErr.Visible = true;
                    txtPassword.Focus();
                }
            }
        }

        private void displayErrMsg(Label lbl, string errMsg)
        {
            lbl.Text = errMsg;
            lbl.ForeColor = Color.Red;
            lbl.Visible = true;
        }
        protected void txtUserName_TextChanged(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            //string passwordInDb = PrApiCalls.getUserPassword(txtUserName.Text, ref errMsg);
            string passwordInDb = PrApiCalls.getUserPasswordCustname(txtUserName.Text, ref rec2Load.CustName, ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                displayErrMsg(lblPwdErr, errMsg);
                txtUserName.Focus();
                return;
            }
            rec2Load.Passw = passwordInDb;
            txtPassword.Focus();
            //PanelAppointment.Enabled = false;
            PanelAppointment.Visible = false;
        }
        protected void txtPassword_TextChanged(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            if (txtPassword.Text == rec2Load.Passw)
            {
                lblPwdErr.Visible = false;
                txtClinicName.Text = PrApiCalls.getCustDes(rec2Load.CustName, ref errMsg);
                rec2Load.CustDes = txtClinicName.Text;
                txtAppointmentDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                rec2Load.AppointMentDate = DateTime.Today;
                // PanelAppointment.Enabled = true;
                PanelAppointment.Visible = true;
                dlstTherapist.DataBind();
                divCommnets.Visible = true;
            }
            else
            {
                displayErrMsg(lblPwdErr, "Wrong username or password");
            }
        }

        protected void rbInitEval_CheckedChanged(object sender, EventArgs e)
        {
            divInitEval.Visible = true;
            divFollowUp.Visible = false;
            divShipment.Visible = true;
            divCommnets.Visible = true;
        }
        protected void rbFollowUp_CheckedChanged(object sender, EventArgs e)
        {
            divInitEval.Visible = false;
            divFollowUp.Visible = true;
            divShipment.Visible = false;

        }
        protected void txtAppointmentDate_TextChanged(object sender, EventArgs e)
        {
            rec2Load.AppointMentDate = Convert.ToDateTime(txtAppointmentDate.Text);
        }
        protected void txtPatientID_TextChanged(object sender, EventArgs e)
        {
            rec2Load.PatientId = txtPatientID.Text;
        }
        protected void txtPatientBdate_TextChanged(object sender, EventArgs e)
        {
            rec2Load.PatientBdate = Convert.ToDateTime(txtPatientBDate.Text);
        }
        protected void rblGender_selectedIndexChanged(object sender, EventArgs e)
        { }
        protected void rbMale_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMale.Checked)
                rec2Load.PatientGender = "Male";
        }
        protected void rbFemale_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFemale.Checked)
                rec2Load.PatientGender = "Female";
        }
        public List<string> getTherapists()
        {
            string errMsg = string.Empty;
            if (string.IsNullOrEmpty(rec2Load.CustName))
                return null;

            if (!string.IsNullOrEmpty(errMsg))
            {
                if (rec2Load.AppointmentType == "Initial Evaluation")
                    displayErrMsg(lblGetTherapistsStat, errMsg);
                else
                    displayErrMsg(lblGetTherapistsFlUpStat, errMsg);

                return null;
            }
            else
            {
                lblGetPaymentTypesStat.Visible = false;
                List<TherapistClass> lstTherapists = PrApiCalls.getTherapists(rec2Load.CustName, ref errMsg);
                List<string> result = new List<string> { string.Empty };
                foreach (TherapistClass tp in lstTherapists)
                {
                    result.Add(string.Format("{0}#{1}#{2}", tp.NAME, tp.FIRSTNAME, tp.LASTNAME));
                }
                return result;
            }
        }
        protected void dlstTherapist_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.TherapistName = dlstTherapist.SelectedValue;
        }
        protected void txtComments_TextChanged(object sender, EventArgs e)
        {
            rec2Load.Comments = txtComments.Text;
        }
        public List<string> getAppointmentOutcomes()//getCUSTNOTETYPES ()
        {
            string errMsg = string.Empty;
            List<CustNoteTypeClass> custNoteTypes = PrApiCalls.getCUSTNOTETYPES(ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                //lblGetAppOutcomesStat.Text = errMsg;
                displayErrMsg(lblGetAppOutcomesStat, errMsg);
                return null;
            }
            else
            {
                lblGetAppOutcomesStat.Visible = false;
                List<string> result = new List<string> { string.Empty };
                foreach (CustNoteTypeClass cn in custNoteTypes)
                {
                    string openShFlag = (string.IsNullOrEmpty(cn.ERPG_OPENSHFLAG) ? "N" : cn.ERPG_OPENSHFLAG);
                    result.Add(cn.CUSTNOTETYPEDES + "#" + openShFlag); //string.Format("{0}#{1}}", cn.CUSTNOTETYPEDES, openShFlag));
                }
                return result;
            }
        }
        protected void dlstAppOutcome_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] strCustNoteType = dlstAppOutcome.SelectedValue.ToString().Split('#');
            rec2Load.AppointmentOutcome = strCustNoteType[0];
            rec2Load.OpenSh = strCustNoteType[1];
            
            if (rec2Load.OpenSh == "Y")
            {
                //divPayementInfo.Visible =
                updPannelPayment.Visible =
                lblPaymentType.Visible =
                dlstPaymentType.Visible = 
                updPanelStockProvided.Visible =   //divStockProvided.Visible =
                divShipment.Visible = true;
            }
            else
            {
                updPannelPayment.Visible =        //divPayementInfo.Visible =
                lblPaymentType.Visible =
                dlstPaymentType.Visible =
                updPanelStockProvided.Visible =   //divStockProvided.Visible =
                divShipment.Visible = false;
            }
        }
        protected void dlstTherapistFlUp_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.TherapistName = dlstTherapistFlUp.SelectedValue;
        }
        protected void rbAttended_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAttended.Checked)
            {
                divShipment.Visible = true;
                divCommnets.Visible = true;
                rec2Load.AttendedFollowUp = 'Y';
            }
           
        }
        protected void rbNotAttended_CheckedChanged(object sender, EventArgs e)
        {
            if (rbNotAttended.Checked)
            {
                divShipment.Visible = false;
                divCommnets.Visible = true;
                rec2Load.AttendedFollowUp = 'N';
            }

        }
        protected void rbBagGiven_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBagGiven.Checked)
            {
                rec2Load.AposTherapyBagGiven = 'Y';
            }

        }
        protected void rbBagNotGiven_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBagNotGiven.Checked)
            {
                rec2Load.AposTherapyBagGiven = 'N';
            }

        }

        public List<string> getPaymentTypes()
        {
            string errMsg = string.Empty;
            List<CustSpec1ValueClass> pTypes = PrApiCalls.getCustSpec1Vals(ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                //lblGetPaymentTypesStat.Text = errMsg;
                displayErrMsg(lblGetPaymentTypesStat, errMsg);
                return null;
            }
            else
            {
                List<string> result = new List<string> { string.Empty };
                foreach (CustSpec1ValueClass cs in pTypes)
                {
                    result.Add(cs.SPECVALUE + "#" + cs.ERPG_S1_NOINSURANCE);
                }
                return result;
            }
        }
        protected void dlstPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] strPayTypes = dlstPaymentType.SelectedValue.ToString().Split('#');
            rec2Load.PaymentType = strPayTypes[0];
            rec2Load.NoInsurance = strPayTypes[1];
            if (rec2Load.NoInsurance == "Y")
                divInsurer.Visible = false;
            else
                divInsurer.Visible = true;
        }
        private List<string> getSpecValues(int specNum, string formPrefix, Label lblErr)
        {
            string errMsg = string.Empty;
            List<SpecValueClass> pTypes = PrApiCalls.getSpecVals(formPrefix, specNum, ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                displayErrMsg(lblErr, errMsg);
                return null;
            }
            else
            {
                List<string> result = new List<string> { string.Empty };
                foreach (SpecValueClass sv in pTypes)
                {
                    result.Add(sv.SPECVALUE);
                }
                return result;
            }
        }
        public List<string> getInsurers()
        {
            int specNum = 2;
            specNum = 1;   //debug - licence problem !
            List<string> result = getSpecValues(specNum, "CUSTSPEC", lblGetInsurersStat);
            result.Add("other");
            return result;
        }
        protected void dlstInsurerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.InsurerName = dlstInsurerName.SelectedValue;
        }
        public List<string> getInsurancePackages()
        {
            int specNum = 3;
            specNum = 1;   //debug - licence problem !
            return getSpecValues(specNum, "CUSTSPEC", lblGetInsurancePackageStat);
        }
        protected void dlsPackageTaken_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.PackageTaken = dlsPackageTaken.SelectedValue;
        }
        public List<string> getDevices1()
        {
            int specNum = 1;
            return getSpecValues(specNum, "SPEC", lblGetDevicesStat);
        }
        protected void dlstDevice1_SelectedIndeChanged(object sender, EventArgs e)
        {
            rec2Load.Device1 = dlstDevice1.SelectedValue;
        }
        public List<string> getDevices2()
        {
            int specNum = 2;
            specNum = 1;     //debug - licence problem !
            return getSpecValues(specNum, "SPEC", lblGetDevicesStat);
        }
        protected void dlstDevice2_SelectedIndeChanged(object sender, EventArgs e)
        {
            rec2Load.Device2 = dlstDevice2.SelectedValue;
        }
        public List<string> getC85Profiles4()
        {
            int specNum = 4;
            specNum = 1;     //debug - licence problem !
            return getSpecValues(specNum, "SPEC", lblGetC85ProfileStat);
        }
        protected void dlstC85Profile4_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.C85Spec4 = dlstC85Profile4.SelectedValue;
        }
        protected void dlstC85Amount4_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.C85Amount4 = int.Parse(dlstC85Amount4.SelectedValue);
        }
        public List<string> getC85Profiles5()
        {
            int specNum = 5;
            specNum = 1;     //debug - licence problem !
            return getSpecValues(specNum, "SPEC", lblGetC85ProfileStat);
        }
        protected void dlstC85Profile5_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.C85Spec5 = dlstC85Profile5.SelectedValue;
        }
        protected void dlstC85Amount5_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.C85Amount5 = int.Parse(dlstC85Amount5.SelectedValue);
        }
        public List<string> getC95Profiles4()
        {
            int specNum = 4;
            specNum = 1;     //debug - licence problem !
            return getSpecValues(specNum, "SPEC", lblGetC85ProfileStat);
        }
        protected void dlstC95Profile4_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.C95Spec4 = dlstC95Profile4.SelectedValue;
        }
        protected void dlstC95Amount4_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.C95Amount4 = int.Parse(dlstC95Amount4.SelectedValue);
        }
        public List<string> getC95Profiles5()
        {
            int specNum = 5;
            specNum = 1;     //debug - licence problem !
            return getSpecValues(specNum, "SPEC", lblGetC95ProfileStat);
        }
        protected void dlstC95Profile5_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.C95Spec5 = dlstC95Profile5.SelectedValue;
        }
        protected void dlstC95Amount5_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.C95Amount5 = int.Parse(dlstC85Amount5.SelectedValue);
        }
        //--
        public List<string> getS85Profiles6()
        {
            int specNum = 6;
            specNum = 1;     //debug - licence problem !
            return getSpecValues(specNum, "SPEC", lblGetS85ProfileStat);
        }
        protected void dlstS85Profile6_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.S85Spec6 = dlstS85Profile6.SelectedValue;
        }
        protected void dlstS85Amount61_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.S85Amount61 = int.Parse(dlstS85Amount61.SelectedValue);
        }
        protected void dlstS85Amount62_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.S85Amount62 = int.Parse(dlstS85Amount62.SelectedValue);
        }
        public List<string> getS95Profiles6()
        {
            int specNum = 6;
            specNum = 1;     //debug - licence problem !
            return getSpecValues(specNum, "SPEC", lblGetS95ProfileStat);
        }
        protected void dlstS95Profile6_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.S95Spec6 = dlstS95Profile6.SelectedValue;
        }
        protected void dlstS95Amount61_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.S95Amount61 = int.Parse(dlstS95Amount61.SelectedValue);
        }
        protected void dlstS95Amount62_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.S95Amount62 = int.Parse(dlstS95Amount62.SelectedValue);
        }
        public List<string> getSpec7Values()
        {
            int specNum = 7;
            specNum = 1;     //debug - licence problem !
            return getSpecValues(specNum, "SPEC", lblGetSpec7ValuesStat);
        }
        protected void dlstWeights_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.Weights = dlstWeights.SelectedValue;
        }
        protected void dlstWeightAmount_SelectedIndexChanged(object sender, EventArgs e)
        {
            rec2Load.WeightAmount7 = int.Parse(dlstWeightAmount.SelectedValue);
        }
        protected void btnSubmit_click(object sender, EventArgs e)
        {
            DateTime x = rec2Load.PatientBdate;
        }
    }
}