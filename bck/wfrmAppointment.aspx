﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="wfrmAppointment.aspx.cs" Inherits="WebApp1.wfrmAppointment" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="MyStyles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div id="divTitle"
            class="segment_header" style="background:#FF0000;width:1000px;text-align:Center;">
            <h1 style="font-weight:bold;font-size:24px;padding:20px 1em ;color:white">Appointment Information</h1>

        </div> <!-- end divTitle -->
        <br />
        <div id="divLogin" runat="server">
            <asp:Label ID="lblUserName" runat="server" Text="UserName:"></asp:Label>
            &nbsp;&nbsp;
            <asp:TextBox ID="txtUserName" runat="server" ClientIDMode="Static" AutoPostBack="true"
                OnTextChanged="txtUserName_TextChanged"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblPassword" runat="server" Text="Password:"></asp:Label>
            &nbsp;&nbsp;
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" OnTextChanged="txtPassword_TextChanged"
                AutoPostBack="true"  >
            </asp:TextBox>
            &nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblPwdErr" runat="server" Text="" Visible="false"></asp:Label>
        </div>
        <!--end divLogin -->
        <br />
        <br />
        <asp:Panel ID="PanelAppointment" runat="server">
            
            <div id="divAppointmentHdr" runat="server">
                        <asp:Label ID="lblClinicName" runat="server" Text="Clinic Name"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtClinicName" runat="server"></asp:TextBox>
                        <br />
                        <asp:UpdatePanel ID="updPannelAppDate" runat="server">
                            <ContentTemplate>
                                <br /> Appointment Date &nbsp;&nbsp;
                                <asp:TextBox ID="txtAppointmentDate" TextMode="Date" dir="ltr" runat="server" AutoPostBack="true"
                                    ClientIDMode="Static" MaxLength="10"
                                    OnTextChanged="txtAppointmentDate_TextChanged" Width="177px">
                                </asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel> <!-- updPannelAppDate -->
                        <br />Appointment Type &nbsp;&nbsp;
                        <asp:RadioButton ID="rbInitEval" AutoPostBack="true" runat="server"
                            GroupName="AppointmentType" Text="Initial Evaluation" OnCheckedChanged="rbInitEval_CheckedChanged" />
                                        &nbsp;&nbsp;&nbsp;
                        <asp:RadioButton ID="rbFollowUp" AutoPostBack="true" runat="server"
                            GroupName="AppointmentType" Text="Follow Up" OnCheckedChanged="rbFollowUp_CheckedChanged" />
                        <br /><br />
                        <asp:UpdatePanel ID="updPanelPatientID" runat="server" >
                            <ContentTemplate>
                              <br /><br />Patient ID:   &nbsp;&nbsp;
                        <asp:TextBox ID="txtPatientID" runat="server" AutoPostBack="true"
                        OnTextChanged="txtPatientID_TextChanged" >
                        </asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                    </div>             <!-- end divAppointmentHdr -->

                    <div id="divInitEval" runat="server" visible="false">
                        <asp:UpdatePanel ID="updPanelBdate" runat="server">
                            <ContentTemplate>
                            <br />Patient Year of Birth :"
                        &nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtPatientBDate" type="date" dir="ltr" runat="server" ClientIDMode="Static" MaxLength="10"
                            OnTextChanged="txtPatientBdate_TextChanged" Width="177px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        <br />
                        <asp:UpdatePanel ID="updPanelGender" runat="server">
                           <ContentTemplate>
                               <br /> 
                               Patient Gender &nbsp;&nbsp;
                              <asp:RadioButton ID="rbMale" AutoPostBack="true" runat="server"
                                GroupName="Gender" Text="Male" OnCheckedChanged="rbMale_CheckedChanged" />
                                &nbsp;&nbsp;&nbsp;
                              <asp:RadioButton ID="rbFemale" AutoPostBack="true" runat="server"
                                GroupName="Gender" Text="Female" OnCheckedChanged="rbFemale_CheckedChanged" />
                           </ContentTemplate>
                        </asp:UpdatePanel>  <!-- end  updPaneGender -->

                        <asp:UpdatePanel ID="updPannelTherapist"  runat ="server">
                            <ContentTemplate>
                                <br />
                                 Physical Therapist Name: &nbsp;&nbsp;
                                <asp:DropDownList ID="dlstTherapist" runat="server"
                                     AutoPostBack ="true"
                                     SelectMethod ="getTherapists"
                                     OnSelectedIndexChanged ="dlstTherapist_SelectedIndexChanged" >
                                </asp:DropDownList>
                                <asp:Label ID="lblGetTherapistsStat" runat="server" Text="" Class="midCtl"></asp:Label>
                           </ContentTemplate>
                        </asp:UpdatePanel>  <!-- end updPannelTherapist-->
                       
                           <div id="divAppOutcome" runat="server">
                                <br />
                                Appointment Outcome: &nbsp;&nbsp;
                                <asp:DropDownList ID="dlstAppOutcome" runat="server" 
                                    AutoPostBack ="true"
                                    SelectMethod="getAppointmentOutcomes"
                                    OnSelectedIndexChanged ="dlstAppOutcome_SelectedIndexChanged" >
                                </asp:DropDownList>
                                <asp:Label ID="lblGetAppOutcomesStat" runat="server" Visible="false" Class="midCtl" ></asp:Label>
                            
                          </div>  <!-- end divAppOutcome -->
                        </div>       <!-- end divInitEval-->

                <div id="divFollowUp" runat="server" visible="false">
                        <br />
                        <asp:Label ID="lblTherapist2" runat="server" Text="Physical Therapist Name"></asp:Label>
                        <br />
                    
                       <asp:UpdatePanel ID="updPannelTherapistFlUp"  runat ="server">
                            <ContentTemplate>
                                <br />
                                 Physical Therapist Name: &nbsp;&nbsp;
                                <asp:DropDownList ID="dlstTherapistFlUp" runat="server"
                                     AutoPostBack ="true"
                                     SelectMethod ="getTherapists"
                                     OnSelectedIndexChanged ="dlstTherapistFlUp_SelectedIndexChanged" >
                                </asp:DropDownList>
                                <asp:Label ID="lblGetTherapistsFlUpStat" runat="server" Text="" Class="midCtl"></asp:Label>
                           </ContentTemplate>
                        </asp:UpdatePanel>  <!-- end updPannelTherapistFlUp-->
                            <br /><br />  Attended Followup ? &nbsp;&nbsp;
                              <asp:RadioButton ID="rbAttended" AutoPostBack="true" runat="server"
                                GroupName="AttendedFUp" Text="Yes" OnCheckedChanged="rbAttended_CheckedChanged" />
                                &nbsp;&nbsp;&nbsp;
                              <asp:RadioButton ID="rbNotAttended" AutoPostBack="true" runat="server"
                                GroupName="AttendedFUp" Text="No" OnCheckedChanged="rbNotAttended_CheckedChanged" />
                    </div>       <!--end divFollowUp -->
              <div id="divShipment" runat="server" visible="false">
                
                            <asp:UpdatePanel ID="updPannelPayment" runat="server">
                              <ContentTemplate>
                                  <br />
                            <asp:Label ID="lblPaymentType" runat="server" Text="Payment Type :" Visible="false"></asp:Label>
                            &nbsp;&nbsp;
                            <asp:DropDownList ID="dlstPaymentType" runat="server"  Visible="false"
                                AutoPostBack ="true"
                                SelectMethod ="getPaymentTypes"
                                OnSelectedIndexChanged ="dlstPaymentType_SelectedIndexChanged" >
                            </asp:DropDownList>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblGetPaymentTypesStat" runat="server" Text="" Visible="false" ></asp:Label>
                            <br />
                            <div id="divInsurer" runat="server" visible="false">
                                <br />
                                <asp:Label ID="lblInsurerName" runat="server" Text="Insurer Name:"></asp:Label>
                                &nbsp;&nbsp;
                                <asp:DropDownList ID="dlstInsurerName" runat="server" AutoPostBack="true"
                                    SelectMethod ="getInsurers"
                                    OnSelectedIndexChanged ="dlstInsurerName_SelectedIndexChanged">
                                </asp:DropDownList>
                                &nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblGetInsurersStat" runat="server" Text="" Visible="false"></asp:Label>
                                <br /><br />
                                <asp:Label ID="lblPackageTaken" runat="server" Text="Package Taken:"></asp:Label>
                                &nbsp;&nbsp;
                                <asp:DropDownList ID="dlsPackageTaken" runat="server" AutoPostBack="true"
                                    SelectMethod="getInsurancePackages"
                                    OnSelectedIndexChanged ="dlsPackageTaken_SelectedIndexChanged">
                                </asp:DropDownList>
                                &nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblGetInsurancePackageStat" runat="server" Text="" Visible="false"></asp:Label>

                            </div>    <!--end divInsurer -->
                                  </ContentTemplate>
                                </asp:UpdatePanel>  <!-- end updPannelPayment -->
                        
                <br />
                  <asp:UpdatePanel ID="updPanelStockProvided" runat="server" Visible="false">
                      <ContentTemplate>
                      
                        <div id="divStockProvided" runat="server" style="border:solid; border-color:blue; width:1000px">
                            <div class="segment_header" style="background:#FF0000;width:auto;text-align:Center;">
                                <h1 style="font-weight:bold;font-size:24px;padding:20px 1em ;">Stock provided to patient</h1>
                            </div>
                             <br /><br />
                          <a style="text-align:center;font-size:large;margin-left:150px">Device</a> <br />
                           <asp:DropDownList ID="dlstDevice1" runat="server" AutoPostBack="true"
                                SelectMethod="getDevices1"
                                OnSelectedIndexChanged ="dlstDevice1_SelectedIndeChanged">
                                </asp:DropDownList>
                                
                    <asp:DropDownList ID="dlstDevice2" runat="server" AutoPostBack="true"
                                SelectMethod="getDevices2"
                                OnSelectedIndexChanged ="dlstDevice2_SelectedIndeChanged"
                                class="midctl">
                                </asp:DropDownList>
                     
                     <asp:Label ID="lblGetDevicesStat" runat="server" Text="" Visible="false" class="midctl" ></asp:Label>
                                <br /><br />
              <div id="divC85" runat="server" style="border: thin dashed blue; width: 1000px">
                  <a class="lefttl">Convex Cap 85mm</a>
                  <a class="midttl">Convex Cap 85mm</a>
                  <br />
                  <br />
                  C85mm_1
                  <a style="margin-left:60px">Profile<a /><a style="margin-left:110px">Amount<a />
                  <a style="margin-left:80px">C85mm_2<a />
                  <a style="margin-left:70px">Profile<a /><a style="margin-left:110px">Amount<a />
                  <br />
                  <asp:Label ID="lblC85Spec3Val1" runat="server" Text="xxxxxxxx"></asp:Label>
                  <asp:DropDownList ID="dlstC85Profile4" runat="server" AutoPostBack="true"  style="margin-left:70px"
                   SelectMethod="getC85Profiles4"
                   OnSelectedIndexChanged ="dlstC85Profile4_SelectedIndexChanged" >
                  </asp:DropDownList>
                
                <asp:DropDownList ID="dlstC85Amount4" runat="server" AutoPostBack="true" style="margin-left:60px"
                   OnSelectedIndexChanged ="dlstC85Amount4_SelectedIndexChanged">
                    <asp:ListItem>0</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                </asp:DropDownList>
                  <asp:Label ID="lblC85spec3Val2" runat="server" Text ="xxxxxxxx" style="margin-left:100px"></asp:Label>
                  <asp:DropDownList ID="dlstC85Profile5" runat="server" AutoPostBack="true" style="margin-left:80px"
                      OnSelectedIndexChanged="dlstC85Profile5_SelectedIndexChanged" SelectMethod="getC85Profiles5">
                  </asp:DropDownList>
                  
                  <asp:DropDownList ID="dlstC85Amount5" runat="server" AutoPostBack="true" style="margin-left:60px"
                      OnSelectedIndexChanged="dlstC85Amount5_SelectedIndexChanged">
                      <asp:ListItem>0</asp:ListItem>
                      <asp:ListItem>1</asp:ListItem>
                      <asp:ListItem>2</asp:ListItem>
                      <asp:ListItem>3</asp:ListItem>
                      <asp:ListItem>4</asp:ListItem>
                  </asp:DropDownList>
                  <asp:Label ID="lblGetC85ProfileStat" runat="server" style="margin-left:100px"></asp:Label>
                                    <br />
          </div>   <!--end divC85 -->
                      <br />
          <br />
                      <div id="divC95" runat="server" style="border: thin dashed blue; width: 1000px">
                          <a class="lefttl">Convex Cap 95mm</a>
                          <a class="midttl">Convex Cap 95mm</a>
                  <br />
                  <br />
                  C95mm_1
                  <a style="margin-left:60px">Profile<a /><a style="margin-left:110px">Amount<a />
                  <a style="margin-left:80px">C95mm_2<a />
                  <a style="margin-left:70px">Profile<a /><a style="margin-left:110px">Amount<a />
                  <br />
                  <asp:Label ID="lblC95Spec3Val1" runat="server" Text="xxxxxxxx"></asp:Label>
                  <asp:DropDownList ID="dlstC95Profile4" runat="server" AutoPostBack="true"  style="margin-left:70px"
                   SelectMethod="getC95Profiles4"
                   OnSelectedIndexChanged ="dlstC95Profile4_SelectedIndexChanged" >
                  </asp:DropDownList>
                
                <asp:DropDownList ID="dlstC95Amount4" runat="server" AutoPostBack="true" style="margin-left:60px"
                   OnSelectedIndexChanged ="dlstC95Amount4_SelectedIndexChanged">
                    <asp:ListItem>0</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                </asp:DropDownList>
                  <asp:Label ID="lblC95spec3Val2" runat="server" Text ="xxxxxxxx" style="margin-left:100px"></asp:Label>
                  <asp:DropDownList ID="dlstC95Profile5" runat="server" AutoPostBack="true" style="margin-left:80px"
                      OnSelectedIndexChanged="dlstC95Profile4_SelectedIndexChanged" SelectMethod="getC95Profiles4">
                  </asp:DropDownList>
                  
                  <asp:DropDownList ID="dlstC95Amount5" runat="server" AutoPostBack="true" style="margin-left:60px"
                      OnSelectedIndexChanged="dlstC95Amount4_SelectedIndexChanged">
                      <asp:ListItem>0</asp:ListItem>
                      <asp:ListItem>1</asp:ListItem>
                      <asp:ListItem>2</asp:ListItem>
                      <asp:ListItem>3</asp:ListItem>
                      <asp:ListItem>4</asp:ListItem>
                  </asp:DropDownList>
                  <asp:Label ID="lblGetC95ProfileStat" runat="server" style="margin-left:100px"></asp:Label>
                                    <br />
                      </div>
                      <!--end divC95 -->
                      <br />
                      <div id="divS85" runat="server"  style="border: thin dashed blue; width: 1000px">
                          <a class="lefttl">Spacer 85mm</a>
                          <br />
                          <br />
                          S85mm_1
                          <a style="margin-left:60px">Profile<a /><a style="margin-left:110px">Amount 1<a />
                          <a style="margin-left:110px">Amount 2<a />
                          <br />
                          <asp:Label ID="lblS85Spec3Val1" runat="server" Text="xxxxxxxx"></asp:Label>
                          <asp:DropDownList ID="dlstS85Profile6" runat="server" AutoPostBack="true" style="margin-left:70px"
                              SelectMethod="getS85Profiles6"
                              OnSelectedIndexChanged = "dlstS85Profile6_SelectedIndexChanged"  >
                          </asp:DropDownList>
                          
                          <asp:DropDownList ID="dlstS85Amount61" runat="server" AutoPostBack="true" style="margin-left:60px"
                              OnSelectedIndexChanged ="dlstS85Amount61_SelectedIndexChanged">
                              <asp:ListItem>0</asp:ListItem>
                              <asp:ListItem >1</asp:ListItem>
                              <asp:ListItem >2</asp:ListItem>
                              <asp:ListItem >3</asp:ListItem>
                              <asp:ListItem >4</asp:ListItem>
                              <asp:ListItem >5</asp:ListItem>
                              <asp:ListItem >6</asp:ListItem>
                          </asp:DropDownList>
                          <asp:DropDownList ID="dlstS85Amount62" runat="server" AutoPostBack="true" style="margin-left:150px"
                              OnSelectedIndexChanged ="dlstS85Amount62_SelectedIndexChanged">
                              <asp:ListItem>0</asp:ListItem>
                              <asp:ListItem >1</asp:ListItem>
                              <asp:ListItem >2</asp:ListItem>
                              <asp:ListItem >3</asp:ListItem>
                              <asp:ListItem >4</asp:ListItem>
                              <asp:ListItem >5</asp:ListItem>
                              <asp:ListItem >6</asp:ListItem>
                          </asp:DropDownList>
                          <asp:Label ID="lblGetS85ProfileStat" runat="server" style="margin-left:100px"></asp:Label>
                      </div>
                      <!--end divS85 -->
                      <br />
                      <br />
                      <div id="divS95" runat="server"  style="border: thin dashed blue; width: 1000px">
                          <a class="lefttl">Spacer 95mm</a>
                          <br />
                          <br />
                          S95mm_1
                          <a style="margin-left:60px">Profile<a /><a style="margin-left:110px">Amount 1<a />
                          <a style="margin-left:110px">Amount 2<a />
                          <br />
                          <asp:Label ID="lblS95Spec3Val1" runat="server" Text="xxxxxxxx"></asp:Label>
                          <asp:DropDownList ID="dlstS95Profile6" runat="server" AutoPostBack="true" style="margin-left:70px"
                              SelectMethod="getS95Profiles6"
                              OnSelectedIndexChanged = "dlstS95Profile6_SelectedIndexChanged"  >
                          </asp:DropDownList>
                          
                          <asp:DropDownList ID="dlstS95Amount61" runat="server" AutoPostBack="true" style="margin-left:60px"
                              OnSelectedIndexChanged ="dlstS95Amount61_SelectedIndexChanged">
                              <asp:ListItem>0</asp:ListItem>
                              <asp:ListItem >1</asp:ListItem>
                              <asp:ListItem >2</asp:ListItem>
                              <asp:ListItem >3</asp:ListItem>
                              <asp:ListItem >4</asp:ListItem>
                              <asp:ListItem >5</asp:ListItem>
                              <asp:ListItem >6</asp:ListItem>
                          </asp:DropDownList>
                          <asp:DropDownList ID="dlstS95Amount62" runat="server" AutoPostBack="true" style="margin-left:150px"
                              OnSelectedIndexChanged ="dlstS95Amount62_SelectedIndexChanged">
                              <asp:ListItem>0</asp:ListItem>
                              <asp:ListItem >1</asp:ListItem>
                              <asp:ListItem >2</asp:ListItem>
                              <asp:ListItem >3</asp:ListItem>
                              <asp:ListItem >4</asp:ListItem>
                              <asp:ListItem >5</asp:ListItem>
                              <asp:ListItem >6</asp:ListItem>
                          </asp:DropDownList>
                          <asp:Label ID="lblGetS95ProfileStat" runat="server" style="margin-left:100px"></asp:Label>
                      </div>  <!--end divS95 -->
                      <br />
                      <asp:Label ID="lblWeights" runat="server" Text="Weights"></asp:Label>
                      
                      <asp:Label ID="lblAmount" runat="server" Text="Amount" Style="margin-left:90px"></asp:Label>
                      <br />
                      <asp:DropDownList ID="dlstWeights" runat="server"  AutoPostBack="true" 
                         SelectMethod ="getSpec7Values"
                        OnSelectedIndexChanged ="dlstWeights_SelectedIndexChanged"
                      >
                      </asp:DropDownList>
                      
            <asp:DropDownList ID="dlstWeightAmount" runat="server" style="margin-left:50px"
               OnSelectedIndexChanged ="dlstWeightAmount_SelectedIndexChanged" >
                <asp:ListItem>0</asp:ListItem>
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblGetSpec7ValuesStat" runat="server" style="margin-left:100px"></asp:Label>
                                <br />
                                <br />

                                <asp:Label ID="lblBagGiven" runat="server" Text="AposTherapy Bag Given?"></asp:Label>
                                <asp:RadioButtonList ID="rblBagGiven" runat="server">
                                    <asp:ListItem Value="False">Yes</asp:ListItem>
                                    <asp:ListItem Value="False">No</asp:ListItem>
                                </asp:RadioButtonList>

                               <br /><br />  AposTherapy Bag Given? &nbsp;&nbsp;
                              <asp:RadioButton ID="rbBagGiven" AutoPostBack="true" runat="server"
                                GroupName="BagGiven" Text="Yes" OnCheckedChanged="rbBagGiven_CheckedChanged" />
                                &nbsp;&nbsp;&nbsp;
                              <asp:RadioButton ID="rbBagNotGiven" AutoPostBack="true" runat="server"
                                GroupName="BagGiven" Text="No" OnCheckedChanged="rbBagNotGiven_CheckedChanged" />

                            </div>  <!--end divStockProvided -->
                          </a>
                      </ContentTemplate>
                  </asp:UpdatePanel> <!--end updPanelStockProvided -->
              </div>  <!-- end divShipment -->
              <div id="divCommnets" runat="server" Visible="false">
                  <br /><br />AdditionalComments <br />
                                <asp:TextBox ID="txtComments" runat="server" OnTextChanged="txtComments_TextChanged"
                                    TextMode="MultiLine"  Rows="5"  
                                    style="border:solid;border-color:blue;width: 1000px" >
                                </asp:TextBox>
              </div> <!-- end divComments -->

            </a>


        <br />
        <br />
        
        <asp:Button ID="btnSubmit" runat="server" Height="53px" Text="Submit" Width="423px" 
            style="margin-left:300px;color:black;background-color:#FFCC00;font-weight:bold;font-size:20px;" class="submit_button"
            OnClick ="btnSubmit_click"
        />
        </div>
        </asp:Panel>            <!-- end PanelAppointment-->            
        <br />
        <br />
        <br />
        <br />
        <br />
    </form>
</body>
</html>
