﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp1
{
    public class value
    {
            public string CUSTNAME { get; set; }
            public string CUSTDES { get; set; }
            public object CUSTDESLONG { get; set; }
            public object ECUSTDES { get; set; }
            public string STATDES { get; set; }
            public string OWNERLOGIN { get; set; }
            public object INACTIVEFLAG { get; set; }
            public DateTime CREATEDDATE { get; set; }
            public DateTime STATUSDATE { get; set; }
            public object PHONE { get; set; }
            public object FAX { get; set; }
            public object EMAIL { get; set; }
            public object BUSINESSTYPE { get; set; }
            public object MCUSTNAME { get; set; }
            public object MCUSTDES { get; set; }
            public object CTYPECODE { get; set; }
            public object CTYPENAME { get; set; }
            public object PCUSTNAME { get; set; }
            public object CTYPE2CODE { get; set; }
            public object PCUSTDES { get; set; }
            public object CTYPE2NAME { get; set; }
            public object CUSTPART { get; set; }
            public object NSFLAG { get; set; }
            public object STCODE { get; set; }
            public object STDES { get; set; }
            public object ZONECODE { get; set; }
            public object ZONEDES { get; set; }
            public object TRACK { get; set; }
            public string ADDRESS { get; set; }
            public object ADDRESS2 { get; set; }
            public object ADDRESS3 { get; set; }
            public string STATE { get; set; }
            public string STATEA { get; set; }
            public object STATENAME { get; set; }
            public object ZIP { get; set; }
            public object COUNTRYNAME { get; set; }
            public object WTAXNUM { get; set; }
            public object WTAXNUMEXPL { get; set; }
            public object VATNUM { get; set; }
            public object AGENTCODE { get; set; }
            public object AGENTNAME { get; set; }
            public object AGENTCODE2 { get; set; }
            public object AGENTNAME2 { get; set; }
            public object TERRITORYCODE { get; set; }
            public object TERRITORYDES { get; set; }
            public double COMMISSION { get; set; }
            public object ESTABLISHED { get; set; }
            public int EMPNUM { get; set; }
            public object BRANCHNAME { get; set; }
            public object BRANCHDES { get; set; }
            public object PAYCODE { get; set; }
            public object PAYDES { get; set; }
            public double MAX_CREDIT { get; set; }
            public double MAX_OBLIGO { get; set; }
            public string OBCODE { get; set; }
            public object DISTRLINECODE { get; set; }
            public object DISTRLINEDES { get; set; }
            public string UNLOADTIME { get; set; }
            public int DISTRORDER { get; set; }
            public object NOTALLOWFORECAST { get; set; }
            public object BONUSFLAG { get; set; }
            public object COMPETITORFLAG { get; set; }
            public object FORECAST { get; set; }
            public object CHANEL { get; set; }
            public object DISTRTYPECODE { get; set; }
            public object DISTRTYPEDES { get; set; }
            public object SECONDLANGTEXT { get; set; }
            public object CONFIDENTIAL { get; set; }
            public object HOSTNAME { get; set; }
            public object SPEC1 { get; set; }
            public object SPEC2 { get; set; }
            public object SPEC3 { get; set; }
            public object SPEC4 { get; set; }
            public object SPEC5 { get; set; }
            public object SPEC6 { get; set; }
            public object SPEC7 { get; set; }
            public object SPEC8 { get; set; }
            public object SPEC9 { get; set; }
            public object SPEC10 { get; set; }
            public object SPEC11 { get; set; }
            public object SPEC12 { get; set; }
            public object SPEC13 { get; set; }
            public object SPEC14 { get; set; }
            public object SPEC15 { get; set; }
            public object SPEC16 { get; set; }
            public object SPEC17 { get; set; }
            public object SPEC18 { get; set; }
            public object SPEC19 { get; set; }
            public object SPEC20 { get; set; }
            public object EXTFILEFLAG { get; set; }
            public object WAVESTRATEGYCODE { get; set; }
            public object WAVESTRATEGYDES { get; set; }
            public object PICKSTGCODE { get; set; }
            public object PICKSTGDES { get; set; }
            public object AUTOSHPFLAG { get; set; }
            public object GPSX { get; set; }
            public object GPSY { get; set; }
            public object QRANKCODE { get; set; }
            public object QRANKDES { get; set; }
            public int DCMONTHS { get; set; }
            public string CODE { get; set; }
            public string TAXCODE { get; set; }
            public string TAXDES { get; set; }
            public object CUSTREMARK { get; set; }
            public int PIKORDER { get; set; }
            public int MINEXPDAYS { get; set; }
            public int CUST { get; set; }
    }

    public class RootObject
    {
            public List<value> value { get; set; }
    }
    
}
