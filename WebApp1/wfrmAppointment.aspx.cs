﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.Configuration;


namespace WebApp1
{
    public partial class wfrmAppointment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ConfigurationManager.AppSettings["SystemType"] = "US";

                Rec2Load rec2Load = new Rec2Load();
                Session["rec2Load"] = rec2Load;

                PrApiCalls.initRestClient();
                PanelAppointment.Visible = false;
                txtAppointmentDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                rec2Load.AppointmentDate = DateTime.Today;

                // Can this section be combined and handled better?
                CustNoteTypes custNoteTypes = new CustNoteTypes();
                Session["custNoteTypes"] = custNoteTypes;
                CustSpec1Values custSpec1Values = new CustSpec1Values();
                Session["custSpec1Values"] = custSpec1Values;
                CustSpec2Values custSpec2Values = new CustSpec2Values();
                Session["custSpec2Values"] = custSpec2Values;

                // see: https://stackoverflow.com/questions/22745661/how-to-set-the-value-of-a-textbox-textmode-date-asp-net-c-sharp
                // afaik, the new textbox in .Net with textmode=date only supports the YYYY-MM-DD format.
                //btnSubmit.Enabled = false;
                //btnSubmit.Font.Bold = false;
                txtPassword.Enabled = false;
            }
            else
            {
                if (Request.Form["__EVENTTARGET"] == "txtPassword" && string.IsNullOrEmpty(txtPassword.Text))
                {
                    lblPwdErr.Text = "Please enter your password";
                    lblPwdErr.ForeColor = Color.Red;
                    lblPwdErr.Visible = true;
                    txtPassword.Focus();
                    return;
                }

                if (Request.Form["__EVENTTARGET"] != "btnSubmit")
                    lblSubmitStat.Text = string.Empty;              //Clear the status label - user hit 
                                                                    // some control in the middle of the form body
            }
        }

        protected void txtUserName_TextChanged(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            string custName = string.Empty;
            string passwordInDb = PrApiCalls.getUserPasswordCustname(txtUserName.Text, ref custName, ref errMsg);
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.CustName = custName;
            if (!string.IsNullOrEmpty(errMsg))
            {
                UiLogic.displayErrMsg(lblPwdErr, errMsg);
                txtUserName.Focus();
                return;
            }
            rec2Load.Passw = passwordInDb;
            txtPassword.Enabled = true;
            txtPassword.Focus();
            //PanelAppointment.Enabled = false;
            PanelAppointment.Visible = false;
        }
        protected void txtPassword_TextChanged(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (txtPassword.Text == rec2Load.Passw)
            {
                lblPwdErr.Visible = false;
                txtClinicName.Text = PrApiCalls.getCustDes(rec2Load.CustName, ref errMsg);
                if (!string.IsNullOrEmpty(errMsg))
                    UiLogic.displayErrMsg(lblCustDesErr, errMsg);

                rec2Load.CustDes = txtClinicName.Text;
                txtAppointmentDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                rec2Load.AppointmentDate = DateTime.Today;
                PanelAppointment.Visible = true;
                divComments.Visible = true;
                divLogin.Visible = false;
            }
            else
            {
                UiLogic.displayErrMsg(lblPwdErr, "Wrong username or password");
            }
        }

        public DataView getAppointmentType()
        {

            Dictionary<string, string> dictionary = new Dictionary<string, string>
            {
                {"Init","Initial Consultation"}, //Totally not sure this is the best way to do this...
                {"Follow","Follow Up"}
            };

            return UiLogic.getAppointmentType(dictionary);
        }

        protected void radioAppointmentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.AppointmentType = radioAppointmentType.SelectedItem.Text;
            string selected = radioAppointmentType.SelectedItem.Value;
            if (selected == "Init")
            {
                divInitEval.Visible = true;
                divFollowUp.Visible = false;
                divShipment.Visible = false;

                divAppOutcomeIEval.Visible = true;
                updPanelPayment.Visible = false;

            }
            else if (selected == "Follow")
            {
                divInitEval.Visible = false;
                divFollowUp.Visible = true;
                divAppOutcomeIEval.Visible = false;
                //divAppOutcomeFUp.Visible = false;
                divShipment.Visible = false;
            }
        }

        protected void txtAppointmentDate_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.AppointmentDate = Convert.ToDateTime(txtAppointmentDate.Text);
        }
        protected void txtPatientID_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.PatientId = txtPatientID.Text;
        }

        public List<string> getBYears()
        {
            return UiLogic.getBYears();
        }
        protected void dlstPatientBYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (dlstPatientBYear.SelectedValue != "--")
            {
                rec2Load.PatientByear = int.Parse(dlstPatientBYear.SelectedValue);
            }
        }

        public List<string> getRadioGender()
        {
            return new List<string> { "Male", "Female" };
        }
        protected void radioGender_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.PatientGender = PatientGender.SelectedValue;
        }

        public List<string> getRadioTherapists()
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            return UiLogic.getTherapists(lblGetTherapistsStat, rec2Load);
        }
        public List<string> getRadioTherapistsFlUp()
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            return UiLogic.getTherapists(lblGetTherapistsFlUpStat, rec2Load);
        }
        protected void radioTherapist_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.TherapistName = radioTherapist.SelectedValue;
        }

        protected void txtComments_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.Comments = txtComments.Text;
        }

        public List<string> getAppointmentOutcomes()
        {
            return UiLogic.getAppointmentOutcomes(lblGetAppOutcomesIEvalStat);
        }
        protected void radioAppOutcome_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioButtonList r = (RadioButtonList)sender;
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            CustNoteTypes custNoteTypes = (CustNoteTypes)Session["custNoteTypes"];

            rec2Load.AppointmentOutcome = r.SelectedItem.Text;

            char OpenSh = UiLogic.convertNull2N(custNoteTypes.value.Where(x => x.ERPG_CUSTNOTETYPEDES == r.SelectedValue).First().ERPG_OPEN_SH).First();

            rec2Load.OpenSh = OpenSh == 'Y' ? 'Y' : 'N';

            if (rec2Load.OpenSh == 'Y')
            {
                updPanelPayment.Visible =
                lblPaymentType.Visible =
                PaymentType.Visible =
                updPanelStockProvided.Visible =
                divShipment.Visible = true;
                divInsurer.Visible = false;
            }
            else
            {
                updPanelPayment.Visible =
                lblPaymentType.Visible =
                PaymentType.Visible =
                updPanelStockProvided.Visible =
                divShipment.Visible = false;
            }
        }

        public List<string> getAttendedFollowup()
        {
            return new List<string> { "Yes", "No", "Canceled" };
        }
        protected void radioAttendedFollowup_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.AttendedFollowUp = radioAttendedFollowup.SelectedValue;
            if (rec2Load.AttendedFollowUp == "Yes")
            {
                //divAppOutcomeFUp.Visible = true;
                divAppOutcomeIEval.Visible = true;
            }
            else
            {
                //divAppOutcomeFUp.Visible = false;
                divAppOutcomeIEval.Visible = false;
                divShipment.Visible = false;
            }
        }

        public List<string> getBagOptions()
        {
            return new List<string> { "Yes", "No" };
        }
        protected void radioBag_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.AposTherapyBagGiven = radioBagGiven.SelectedValue.First();
        }

        public List<string> getPaymentTypes()
        {
            return UiLogic.getPaymentTypes(lblGetPaymentTypesStat);
        }
        protected void radioPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            CustSpec1Values custSpec1Values = (CustSpec1Values)Session["custSpec1Values"];

            rec2Load.PaymentType = PaymentType.SelectedValue;

            char insured = UiLogic.convertNull2N(custSpec1Values.value.Where(x => x.SPECVALUE == PaymentType.SelectedValue).First().ERPG_INSURED).First();
            char selfPaid = UiLogic.convertNull2N(custSpec1Values.value.Where(x => x.SPECVALUE == PaymentType.SelectedValue).First().ERPG_SELF_PAYMENT).First();

            rec2Load.Insured = insured;
            rec2Load.SelfPaid = selfPaid;

            if (insured == 'Y')
            {
                divInsurer.Visible = true;
                //divPackage.Visible = true;
            }
            else if (selfPaid == 'Y')
            {
                divInsurer.Visible = false;
                //divPackage.Visible = true;
            }
            else
            {
                divInsurer.Visible = false;
                //divPackage.Visible = false;
            }
        }

        public List<string> getInsurers()
        {
            return UiLogic.getInsurers(lblGetInsurersStat);

        }
        protected void radioInsurerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioInsurerName.SelectedValue == "Other")
            {
                InsurerNameOther.Visible = true;
                InsurerNameOther.Focus();
            }
            else
            {
                InsurerNameOther.Text = "";
                InsurerNameOther.Visible = false;
                setInsurer(radioInsurerName.SelectedValue);
            }
        }
        protected void InsurerNameOther_TextChanged(object sender, EventArgs e)
        {
            setInsurer(InsurerNameOther.Text);
        }

        protected void setInsurer(string value)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.InsurerName = value;

            CustSpec2Values custSpec2Values = (CustSpec2Values)Session["custSpec2Values"];
            char No_Package;
            try
            {
                No_Package = UiLogic.convertNull2N(custSpec2Values.value.Where(x => x.SPECVALUE == value).First().ERPG_NO_PACKAGE).First();
            }
            catch
            {
                No_Package = 'N';
            }

            if (No_Package == 'Y')
            {
                divPackage.Visible = false;
            }
            else if (No_Package == 'N')
            {
                divPackage.Visible = true;
            }
        }
        public List<string> getInsurancePackages()
        {
            int specNum = 3;
            return UiLogic.getSpecValues(specNum, "ERPG_CUSTSPEC", lblGetInsurancePackageStat);
        }
        protected void radioPackageTaken_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.PackageTaken = radioPackageTaken.SelectedValue;
        }

        public List<string> getDevSizes()
        {
            int specNum = 2;
            List<string> values = new List<string>();
            values.Add("");
            values.AddRange(UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetDevicesStat));
            return values;
        }
        protected void dlstDevSize_SelectedIndeChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.DevSize = dlstDevSize.SelectedValue;
        }
        public List<string> getDevTypes()
        {
            int specNum = 1;
            List<string> values = new List<string>();
            values.Add("");
            values.AddRange(UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetDevicesStat));
            return values;
        }
        protected void dlstDevType_SelectedIndeChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.DevType = dlstDevType.SelectedValue;
        }
        public List<string> getSpec4()
        {
            int specNum = 4;
            List<string> values = new List<string>();
            values.Add("");
            values.AddRange(UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetC85ProfileStat));
            return values;
        }
        public List<string> getSpec5()
        {
            int specNum = 5;
            List<string> values = new List<string>();
            values.Add("");
            values.AddRange(UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetC85ProfileStat));
            return values;
        }
        protected void dlstC85Profile1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C85Spec4_1 = dlstC85Profile1.SelectedValue;
        }
        protected void dlstC85Amount1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C85Amount4_1 = int.Parse(dlstC85Amount1.SelectedValue);
        }
        protected void dlstC85Profile2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C85Spec4_2 = dlstC85Profile2.SelectedValue;
        }
        protected void dlstC85Amount2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C85Amount4_2 = int.Parse(dlstC85Amount2.SelectedValue);
        }
        protected void dlstC95Profile1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C95Spec4_1 = dlstC95Profile1.SelectedValue;
        }
        protected void dlstC95Amount1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C95Amount4_1 = int.Parse(dlstC95Amount1.SelectedValue);
        }
        protected void dlstC95Profile2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C95Spec4_2 = dlstC95Profile2.SelectedValue;
        }
        protected void dlstC95Amount2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C95Amount4_2 = int.Parse(dlstC95Amount2.SelectedValue);
        }


        public List<int> getSpacerList()
        {
            return new List<int> { 0, 1, 2, 3, 4, 5, 6 };
        }
        protected void radioSpacerS85_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.S85_Soft_Amount = int.Parse(RadioSpacerS85.SelectedValue);
        }
        protected void radioSpacerH85_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.S85_Hard_Amount = int.Parse(RadioSpacerH85.SelectedValue);
        }
        protected void radioSpacerS95_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.S95_Soft_Amount = int.Parse(RadioSpacerS95.SelectedValue);
        }
        protected void radioSpacerH95_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.S95_Hard_Amount = int.Parse(RadioSpacerH95.SelectedValue);
        }

        public List<int> getWeights()
        {
            return new List<int> { 0, 1, 2 };
        }
        protected void radioWeight85_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.Weight85Amount = int.Parse(Weight85.SelectedValue);
            rec2Load.Weight85 = "85mm"; //Should this be dynamic?
        }
        protected void radioWeight95_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.Weight95Amount = int.Parse(Weight95.SelectedValue);
            rec2Load.Weight95 = "95mm"; //Should this be dynamic?
        }


        protected void btnSubmit_click(object sender, EventArgs e)
        {
            UiLogic.btnSubmit_click(sender, lblSubmitStat);
            #region toDel
            //string errMsg = string.Empty;
            //Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            ////UiLogic.validateSelectionPairs(dlstWeight85, dlstWeight95, lblGetSpec7ValuesStat);

            //string payload4 =
            //"{\"LINE\": 23, \"CUSTNAME\": \"100002\",  \"CUSTDES\": \"Plantin BVBA\",  \"APPOINTMENTDATE\": \"2018-06-14\", \"APPOINTMENTTYPE\":"
            //+ "\"Initial Evaluation\", \"PATIENTID\": \"14\", \"PATIENTYOB\": 1999, \"PATIENTGENDER\": \"Male\", \"PHYSIONAME\": \"Moshe\","
            //+ "\"APPOINTMENTOUTCOME\": \"good\", \"ATTENDEDFOLLOWUP\": \"Y\"}";   //Created .

            //string payload5 = 
            //"{\"LINE\": 25, \"CUSTNAME\": \"100002\",  \"CUSTDES\": \"Plantin BVBA\",  \"APPOINTMENTDATE\": \"2018-06-14\", \"APPOINTMENTTYPE\":"
            //+ "\"Initial Evaluation\", \"PATIENTID\": \"14\", \"PATIENTYOB\": 1999, \"PATIENTGENDER\": \"Male\", \"PHYSIONAME\": \"Moshe\","
            //+ "\"APPOINTMENTOUTCOME\": \"good\", \"ATTENDEDFOLLOWUP\": \"Y\","   // end of parent form .
            //+ "\"ERPG_CUSTSHIP_SUBFORM\": [ {\"LINE\": 1,\"WARHSNAME\": \"WIP\", \"PARTNUM\": \"3-0155\", \"AMOUNT\": 2 }]}";  //Created in Highcon

            //string payload6 =
            //"{\"LINE\": 26, \"CUSTNAME\": \"100002\",  \"CUSTDES\": \"Plantin BVBA\",  \"APPOINTMENTDATE\": \"2018-06-14\", \"APPOINTMENTTYPE\":"
            //+ "\"Initial Evaluation\", \"PATIENTID\": \"14\", \"PATIENTYOB\": 1999, \"PATIENTGENDER\": \"Male\", \"PHYSIONAME\": \"Moshe\","
            //+ "\"APPOINTMENTOUTCOME\": \"good\", \"ATTENDEDFOLLOWUP\": \"Y\","   // end of parent form .
            //+ "\"ERPG_CUSTSHIP_SUBFORM\": [ {\"LINE\": 1,\"WARHSNAME\": \"WIP\", \"PARTNUM\": \"3-0155\", \"AMOUNT\": 2 },"      //line 1
            //                             + "{\"LINE\": 2,\"WARHSNAME\": \"WIP\", \"PARTNUM\": \"3-0156\", \"AMOUNT\": 3 } ]}";   //created  in Highcon


            //int nextLine = PrApiCalls.getERPG_SITEINTERFACE_nextLine();
            //int nextShipLine = PrApiCalls.getERPG_CUSTSHIP_nextLine(1);

            //Rec2API rec2Api = new Rec2API();
            //rec2Api.LINE = PrApiCalls.getERPG_SITEINTERFACE_nextLine();
            //rec2Api.CUSTNAME = "100002";
            //rec2Api.CUSTDES = "Plantin BVBA";
            //rec2Api.APPOINTMENTDATE = "2018-06-19";//DateTime.ParseExact(@"19/06/2018", @"dd/MM/yyyy", CultureInfo.CurrentCulture);
            //rec2Api.APPOINTMENTTYPE = "Initial Evaluation";
            //rec2Api.PATIENTID = "17";
            //rec2Api.PATIENTYOB = 2000;
            //rec2Api.PATIENTGENDER = "Female";
            //rec2Api.PHYSIONAME = "Avi";
            //rec2Api.APPOINTMENTOUTCOME = "Excellent";
            //rec2Api.ATTENDEDFOLLOWUP = "N";

            ////string payload7_1 = PrApiCalls.JsonSerializer<Rec2API>(rec2Api);

            //List<ERPG_CUSTSIP_SUBFORM_class> lstCustSips = new List<ERPG_CUSTSIP_SUBFORM_class>();
            //ERPG_CUSTSIP_SUBFORM_class custShip = new ERPG_CUSTSIP_SUBFORM_class();
            //custShip.LINE = 1;
            //custShip.WARHSNAME = "3444";
            //custShip.PARTNUM = "PLAPOS41";
            //custShip.AMOUNT = 1;
            //lstCustSips.Add(custShip);
            //custShip = new ERPG_CUSTSIP_SUBFORM_class();
            //custShip.LINE = 2;
            //custShip.WARHSNAME = "3444";
            //custShip.PARTNUM = "PLAPOS42";
            //custShip.AMOUNT = 1;
            //lstCustSips.Add(custShip);
            //rec2Api.ERPG_CUSTSHIP_SUBFORM = lstCustSips.ToArray();

            //string payload6_1 = PrApiCalls.JsonSerializer<Rec2API>(rec2Api);
            //PrApiCalls.SendToPriority("ERPG_SITEINTERFACE", payload6_1);     //Created in APPOS US firm (Company) .

            //PrApiCalls.SendToPriority("ERPG_SITEINTERFACE", payload6);  

            //if (!UiLogic.ready2Submit(rec2Load, ref errMsg))
            //{
            //    UiLogic.displayErrMsg(lblSubmitStat, errMsg);
            //    return;
            //}
            //else
            //{
            //    //do submit 
            //    string payLoad = @"{""CUSTNAME"":""9998"",""CUSTDES"":""Test 2 by Tsvika""}";
            //    //PrApiCalls.SendToPriority(payLoad);
            //    lblSubmitStat.Text = "Passed data to Priority";
            //    lblSubmitStat.ForeColor = Color.Green;
            //}
            #endregion toDel
        }
    }
}