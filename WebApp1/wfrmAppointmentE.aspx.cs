﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;

namespace WebApp1
{
    public partial class wfrmAppointmentE : System.Web.UI.Page
    {
        //private static Rec2Load rec2Load = new Rec2Load();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PrApiCalls.initRestClient();
                PanelAppointment.Visible = false;
                txtAppointmentDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
                if (rec2Load == null)
                {
                    rec2Load = new Rec2Load();
                    Session["rec2Load"] = rec2Load;
                }

                rec2Load.AppointmentDate = DateTime.Today;

                // see: https://stackoverflow.com/questions/22745661/how-to-set-the-value-of-a-textbox-textmode-date-asp-net-c-sharp
                // afaik, the new textbox in .Net with textmode=date only supports the YYYY-MM-DD format.
                //btnSubmit.Enabled = false;
                //btnSubmit.Font.Bold = false;
                txtPassword.Enabled = false;
            }
            else
            {
                if (Request.Form["__EVENTTARGET"] == txtPassword.ID && string.IsNullOrEmpty(txtPassword.Text))
                {
                    lblPwdErr.Text = "Please enter your password";
                    lblPwdErr.ForeColor = Color.Red;
                    lblPwdErr.Visible = true;
                    txtPassword.Focus();
                    return;
                }

                if (Request.Form["__EVENTTARGET"] != btnSubmit.ID)
                    lblSubmitStat.Text = string.Empty;              //Clear the status label - user hit 
                                                                    // some control inn the middle of the form body
            }

        }
        protected void txtUserName_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            UiLogic.txtUserName_TextChanged(sender, lblPwdErr, txtPassword, ref rec2Load);
            txtPassword.Enabled = true;
            PanelAppointment.Visible = false;
        }
        protected void txtPassword_TextChanged(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (txtPassword.Text == rec2Load.Passw)
            {
                lblPwdErr.Visible = false;
                txtClinicName.Text = PrApiCalls.getCustDes(rec2Load.CustName, ref errMsg);
                if (!string.IsNullOrEmpty(errMsg))
                    UiLogic.displayErrMsg(lblCustDesErr, errMsg);

                rec2Load.CustDes = txtClinicName.Text;
                txtAppointmentDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                rec2Load.AppointmentDate = DateTime.Today;
                PanelAppointment.Visible = true;
                //dlstTherapist.DataBind();
                divCommnets.Visible = true;
                divLogin.Visible = false;
            }
            else
            {
                UiLogic.displayErrMsg(lblPwdErr, "Wrong username or password");
            }
        }
        protected void rbInitEval_CheckedChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (rbInitEval.Checked || rbReview.Checked)
            {
                divInitEval.Visible = true;
                //divFollowUp.Visible = false;
                //divShipment.Visible = false;  // will be visible only after a task with OpenSH = 'Y' will be selectd from dlstAppOutcome 
                updPanelFollupNum.Visible = false;
                updPanelPricePaid.Visible = false;
                updPanelPayment.Visible = false;
                divAppOutcomeIEval.Visible = true;
                divAppOutcomeFUp.Visible = false;

                divCommnets.Visible = true;

                if (rbInitEval.Checked)
                    rec2Load.AppointmentType = "Initial Evaluation";
                else
                    rec2Load.AppointmentType = "Review";
            }
        }
        protected void rbFollowUp_CheckedChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (rbFollowUp.Checked)
            {
                divInitEval.Visible = false;
                divFollowUp.Visible = true;
                divAppOutcomeIEval.Visible = false;
                divAppOutcomeFUp.Visible = true;
                rec2Load.AppointmentType = "Follow Up";
            }

        }
        protected void txtAppointmentDate_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.AppointmentDate = Convert.ToDateTime(txtAppointmentDate.Text);
        }
        //protected void txtPatientBdate_TextChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    rec2Load.PatientBdate = Convert.ToDateTime(txtPatientBDate.Text);
        //}
        public List<string> getBYears()
        {
            List<string> bYears = new List<string>();
            bYears.Add("--");
            for (int bAge = 0; bAge <= 120; bAge++)
            {
                bYears.Add((DateTime.Now.Year - bAge).ToString());
            }
            return bYears;
        }
        protected void dlstPatientBYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (dlstPatientBYear.SelectedValue != "--")
            {
                rec2Load.PatientByear = int.Parse(dlstPatientBYear.SelectedValue);
            }
        }
        public List<string> getRadioGender()
        {
            return new List<string> { "Male", "Female" };
        }
        protected void radioGender_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.PatientGender = PatientGender.SelectedValue;
        }
        //protected void rbMale_CheckedChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    if (rbMale.Checked)
        //        rec2Load.PatientGender = "Male";
        //}
        //protected void rbFemale_CheckedChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    if (rbFemale.Checked)
        //        rec2Load.PatientGender = "Female";
        //}
        protected void txtPatientID_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.PatientId = txtPatientID.Text;
        }
        //public List<string> getTherapists()
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    return UiLogic.getTherapists(lblGetTherapistsStat, rec2Load);
        //}
        public List<string> getTherapists()
        {
            string errMsg = string.Empty;
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (string.IsNullOrEmpty(rec2Load.CustName))
            {
                errMsg = "Unexpected Error : CUSTNAME for this user (contact) is empty - please contact the application support team";
            }

            if (!string.IsNullOrEmpty(errMsg))
            {
                //if (rec2Load.AppointmentType == "Initial Evaluation")
                UiLogic.displayErrMsg(lblGetTherapistsStat, errMsg);
                //else
                //    UiLogic.displayErrMsg(lblGetTherapistsFlUpStat, errMsg);

                return null;
            }
            else
            {
                lblGetPaymentTypesStat.Visible = false;
                List<TherapistClass> lstTherapists = PrApiCalls.getTherapists(rec2Load.CustName, ref errMsg);
                if (lstTherapists == null)
                {
                    UiLogic.displayErrMsg(lblGetTherapistsStat, "No theraphists found for this user");
                    //return null;
                    return new List<string> { "----------" };
                }
                List<string> result = new List<string> { string.Empty };
                foreach (TherapistClass tp in lstTherapists)
                {
                    //result.Add(string.Format("{0}#{1}#{2}", tp.NAME, tp.FIRSTNAME, tp.LASTNAME));
                    result.Add(tp.NAME);
                }
                return result;
            }
        }
        public List<string> getRadioTherapists()
        {
            string errMsg = string.Empty;
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (string.IsNullOrEmpty(rec2Load.CustName))
            {
                errMsg = "Unexpected Error : CUSTNAME for this user (contact) is empty - please contact the application support team";
                UiLogic.displayErrMsg(lblGetTherapistsStat, errMsg);
                return null;
            }
            else
            {
                lblGetPaymentTypesStat.Visible = false;
                List<TherapistClass> lstTherapists = PrApiCalls.getTherapists(rec2Load.CustName, ref errMsg);
                if (lstTherapists == null)
                {
                    UiLogic.displayErrMsg(lblGetTherapistsStat, "No theraphists found for this user");
                    return null; // return new List<string> { "----------" };
                }
                List<string> result = new List<string>();
                foreach (TherapistClass tp in lstTherapists)
                {
                    result.Add(tp.NAME);
                }
                return result;
            }
        }
        //protected void dlstTherapist_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    rec2Load.TherapistName = dlstTherapist.SelectedValue;
        //}
        protected void radioTherapist_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.TherapistName = radioTherapist.SelectedValue;
        }
        protected void dlstTherapistFlUp_SelectedIndexChanged(object sender, EventArgs e)
        {
            //rec2Load.TherapistName = dlstTherapistFlUp.SelectedValue;
        }
        protected void txtPatientPostCode_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.PatientPostCode = txtPatientPostCode.Text;
        }
        public List<string> getLeadSources()
        {
            return UiLogic.getLeadSources(lblGetLeadSourcesStat);
        }
        protected void radioLeadSources_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.LeadSource = LeadSources.SelectedValue;
        }
        public List<string> getLeadTypes()
        {
            return UiLogic.getLeadTypes(lblGetLeadTypesStat);
        }
        protected void radioLeadTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.LeadType = LeadTypes.SelectedValue;
        }
        protected void LeadTypeOther_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2load = (Rec2Load)Session["rec2Load"];
            rec2load.LeadType = LeadTypeOther.Text;
        }

        protected void txtComments_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.Comments = txtComments.Text;
        }

        public DataView getCUSTNOTETYPES()
        {
            DataTable result = new DataTable();
            result.Columns.Add("ERPG_CUSTNOTETYPEDES", typeof(string));  //CUSTNOTETYPEDES
            result.Columns.Add("ERPG_OPEN_SH", typeof(string));

            string errMsg = string.Empty;
            List<CustNoteTypeClass> custNoteTypes = PrApiCalls.getCUSTNOTETYPES(ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                //lblGetAppOutcomesStat.Text = errMsg;
                UiLogic.displayErrMsg(lblGetAppOutcomesIEvalStat, errMsg);
                UiLogic.displayErrMsg(lblGetAppOutcomesFUpStat, errMsg);
                return null;
            }
            else
            {
                //lblGetAppOutcomesStat.Visible = false;
                //List<string> result = new List<string> { string.Empty };
                DataRow dr = result.NewRow();  // put empty row as the 1st row of the DataTable
                result.Rows.Add(dr);
                foreach (CustNoteTypeClass cn in custNoteTypes)
                {
                    string openShFlag = (string.IsNullOrEmpty(cn.ERPG_OPEN_SH) ? "N" : cn.ERPG_OPEN_SH);
                    //result.Add(cn.CUSTNOTETYPEDES + "#" + openShFlag); //string.Format("{0}#{1}}", cn.CUSTNOTETYPEDES, openShFlag));
                    dr = result.NewRow();
                    dr["ERPG_CUSTNOTETYPEDES"] = cn.ERPG_CUSTNOTETYPEDES;
                    dr["ERPG_OPEN_SH"] = openShFlag;

                    result.Rows.Add(dr);
                }
                return new DataView(result);
            }
        }
        protected void dlstAppOutcomeIEval_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dlst = (DropDownList)sender;
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            string strCustNoteType = String.Empty;
            //if (dlst.ID == dlstAppOutcomeIEval.ID)
            //strCustNoteType = dlstAppOutcomeIEval.SelectedValue.ToString().Split('#');
            //  else
            //    strCustNoteType = dlstAppOutcomeFUp.SelectedValue.ToString().Split('#');
            rec2Load.AppointmentOutcome = dlst.SelectedItem.Text;
            rec2Load.OpenSh = dlst.SelectedValue.ToString()[0];

            if (rec2Load.OpenSh == 'Y')
            {
                //divPayementInfo.Visible =
                updPanelPayment.Visible =
                lblPaymentType.Visible =
                //dlstPaymentType.Visible =
                PaymentType.Visible =
                updPanelStockProvided.Visible = true;  //divStockProvided.Visible =

                divInsurer.Visible = false;  // will be visible after Payment with NoInsurance == N will be selected
                //populateSpec3Labels();
            }
            else
            {
                updPanelPayment.Visible =        //divPayementInfo.Visible =
                lblPaymentType.Visible =
                //dlstPaymentType.Visible =
                PaymentType.Visible =
                updPanelStockProvided.Visible = false; //divStockProvided.Visible =
            }
        }
        protected void dlstAppOutcomeFUp_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            string strCustTopic = string.Empty;
            string strFUp_PayType_PPaid = string.Empty;
            strCustTopic = dlstAppOutcomeFUp.SelectedItem.Text;
            strFUp_PayType_PPaid = dlstAppOutcomeFUp.SelectedItem.Value.ToString();

            rec2Load.AppointmentOutcomeFUp = strCustTopic;
            // init the flags
            rec2Load.FollowupNumFlag = 'N';
            rec2Load.PaymentTypeFlag = 'N';
            rec2Load.PricePaidFlag = 'N';

            if (strFUp_PayType_PPaid.Length > 0)
                rec2Load.FollowupNumFlag = strFUp_PayType_PPaid[0];
            if (strFUp_PayType_PPaid.Length > 1)
                rec2Load.PricePaidFlag = strFUp_PayType_PPaid[2];
            if (strFUp_PayType_PPaid.Length > 2)
                updPanelStockProvided.Visible = true;
            //populateSpec3Labels();

            if (rec2Load.FollowupNumFlag == 'Y')
            {
                updPanelFollupNum.Visible = true;
                //updPanelStockProvided.Visible = false;
            }
            if (rec2Load.PaymentTypeFlag == 'Y')
            {
                updPanelPayment.Visible = true;
                lblPaymentType.Visible = true;
                //dlstPaymentType.Visible = true;
                PaymentType.Visible = true;
                divInsurer.Visible = false;  // will be visible after Payment with NoInsurance == N will be selected
                //updPanelStockProvided.Visible = false;
            }
            if (rec2Load.PricePaidFlag == 'Y')
            {
                updPanelPricePaid.Visible = true;
                //updPanelStockProvided.Visible = false;
            }
        }
        //public List<string> getCUSTTOPICS()//getAppointmentOutcomes
        //{
        //    return UiLogic.getCUSTTOPICS(lblGetAppOutcomesFUpStat);
        //}
        public DataView getCUSTTOPICS()
        {
            DataTable result = new DataTable();
            result.Columns.Add("TOPICDES", typeof(string));  //CUSTNOTETYPEDES
            result.Columns.Add("ERPG_FUP_PAYTYPE_PPAID", typeof(string));

            string errMsg = string.Empty;
            List<CustTopicClass> custTopics = PrApiCalls.getCUSTTOPICS(ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                UiLogic.displayErrMsg(lblGetAppOutcomesFUpStat, errMsg);
                return null;
            }
            else
            {
                //lblGetAppOutcomesStat.Visible = false;
                //List<string> result = new List<string> { string.Empty };
                DataRow dr = result.NewRow();  // put empty row as the 1st row of the DataTable
                result.Rows.Add(dr);
                foreach (CustTopicClass cn in custTopics)
                {
                    //string followUpFlag = (string.IsNullOrEmpty(cn.ERPG_FOLLOWUPNUMFLAG) ? "N" : cn.ERPG_FOLLOWUPNUMFLAG);
                    //string payMentTypeFlag = (string.IsNullOrEmpty(cn.ERPG_PAYMENTTYPEFLAG) ? "N" : cn.ERPG_PAYMENTTYPEFLAG);
                    //string pricePaidFlag = (string.IsNullOrEmpty(cn.ERPG_PRICEPAIDFLAG) ? "N" : cn.ERPG_PRICEPAIDFLAG);

                    dr = result.NewRow();
                    dr["TOPICDES"] = cn.TOPICDES;
                    //dr["ERPG_FUP_PAYTYPE_PPAID"] = followUpFlag + payMentTypeFlag + pricePaidFlag;
                    dr["ERPG_FUP_PAYTYPE_PPAID"] = UiLogic.convertNull2N(cn.ERPG_FOLLOWUPNUMFLAG) + UiLogic.convertNull2N(cn.ERPG_PAYMENTTYPEFLAG)
                                                                    + UiLogic.convertNull2N(cn.ERPG_PRICEPAIDFLAG);

                    result.Rows.Add(dr);
                }
                return new DataView(result);
            }
        }
        public List<string> getFollowupNums()
        {
            return UiLogic.getSpecValues(4, "ERPG_CUSTSPEC", lblFollowupNumsStat);
        }
        protected void dlstFollowupNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.FollowupNum = dlstFollowupNum.SelectedValue;
        }
        protected void txtPricePaid_TextChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.PricePaid = double.Parse(txtPricePaid.Text);
        }

        public DataView getPaymentTypes()
        {
            DataTable result = new DataTable();
            result.Columns.Add("SPECVALUE", typeof(string));  //CUSTNOTETYPEDES
            result.Columns.Add("INSURED_AND_SELF_PAYMENT", typeof(string));

            string errMsg = string.Empty;
            List<CustSpec1ValueClass> pTypes = PrApiCalls.getCustSpec1Vals(ref errMsg);  //we can't use PrApiCalls.getSpecVals , because
                                                                                         //specifically  for CustSpec1Vlaues we have also the flag ERPG_S1_NOINSURANCE
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (!string.IsNullOrEmpty(errMsg))
            {
                //lblGetPaymentTypesStat.Text = errMsg;
                UiLogic.displayErrMsg(lblGetPaymentTypesStat, errMsg);
                return null;
            }
            else
            {
                DataRow dr = result.NewRow();
                //dr["SPECVALUE"] = String.Empty;
                //dr["INSURED_AND_SELF_PAYMENT"] = string.Empty;
                //result.Rows.Add(dr);

                foreach (CustSpec1ValueClass cs in pTypes)
                {
                    // result.Add(cs.SPECVALUE + "#" + cs.ERPG_S1_NOINSURANCE);
                    dr = result.NewRow();
                    dr["SPECVALUE"] = cs.SPECVALUE;

                    //dr["INSURED_AND_SELF_PAYMENT"] = UiLogic.convertNull2N(cs.ERPG_INSURED) + UiLogic.convertNull2N(cs.ERPG_SELF_PAYMENT);
                    dr["INSURED_AND_SELF_PAYMENT"] = ((cs.ERPG_INSURED == "Y") || (cs.ERPG_SELF_PAYMENT == "Y")) ? "Y" : "N";
                    result.Rows.Add(dr);
                }
                return new DataView(result);
            }
        }
        protected void radioPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.PaymentType = PaymentType.SelectedItem.Text;
            rec2Load.Insured = PaymentType.SelectedItem.Value.First();
            if (rec2Load.Insured == 'Y')
                divInsurer.Visible = false;
            else
                divInsurer.Visible = true;
        }

        public List<string> getInsurers()
        {
            int specNum = 2;
            List<string> result = UiLogic.getSpecValues(specNum, "ERPG_CUSTSPEC", lblGetInsurersStat);
            result.Add("other");
            return result;
        }
        protected void dlstInsurerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.InsurerName = dlstInsurerName.SelectedValue;
        }
        public List<string> getInsurancePackages()
        {
            int specNum = 3;
            return UiLogic.getSpecValues(specNum, "ERPG_CUSTSPEC", lblGetInsurancePackageStat);
        }
        protected void dlstPackageTaken_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.PackageTaken = dlstPackageTaken.SelectedValue;
        }
        public List<string> getDevSizes()//getDevices1()
        {
            int specNum = 2;
            return UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetDevicesStat);
        }
        //protected void dlstDevSize_SelectedIndeChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    rec2Load.DevSize = dlstDevSize.SelectedValue;
        //}
        public List<string> getDevTypes()
        {
            int specNum = 1;
            return UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetDevicesStat);
        }
        protected void dlstDevice_SelectedIndeChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            DropDownList dlst = (DropDownList)sender;
            if (dlst.ID == dlstDevSize.ID)
                rec2Load.DevSize = dlst.SelectedValue;
            if (dlst.ID == dlstDevType.ID)
                rec2Load.DevType = dlstDevType.SelectedValue;

            string errMsg = string.Empty;
            if (!string.IsNullOrEmpty(rec2Load.DevType) && !string.IsNullOrEmpty(rec2Load.DevType))
            {
                string partName = string.Empty;
                if (!UiLogic.checkDeviceSizeAndType(ref partName, ref errMsg))
                {
                    if (!string.IsNullOrEmpty(errMsg))
                    {
                        UiLogic.displayErrMsg(lblGetDevicesStat, errMsg);
                        return;
                    }
                }
                else
                {
                    rec2Load.DevicePartName = partName;
                }
            }
        }

        #region moved to UiLogic
        //private bool checkDeviceSizeAndType(ref string partName, ref string errMsg)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    partName = string.Empty;
        //    errMsg = string.Empty;
        //    string filter = string.Format("SPEC1 eq '{0}' and SPEC2 eq '{1}'", rec2Load.DevSize, rec2Load.DevType);
        //    List<string> lstPartNames = PrApiCalls.getPartBySpecs(filter, ref errMsg);
        //    if (!string.IsNullOrEmpty(errMsg))
        //    {
        //        UiLogic.displayErrMsg(lblGetDevicesStat, errMsg);
        //        return false;
        //    }
        //    if (lstPartNames.Count != 1)
        //    {
        //        errMsg = string.Format("error: Size={0} and Type={1} do not identify a unique Part", rec2Load.DevSize, rec2Load.DevType);
        //        return false;
        //    }
        //    partName = lstPartNames[0];
        //    return true;
        //}
        #endregion moved to UiLogic
        public List<string> getSpec4()
        {
            int specNum = 4;
            return UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetC85ProfileStat);
        }
        protected void dlstC85Profile4_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            string errMsg = string.Empty;
            if (UiLogic.validateSelectionPairs(dlstC85Profile4, dlstC85Profile5, lblGetC85ProfileStat))
            {
                rec2Load.C85Spec4_1 = dlstC85Profile4.SelectedValue;
                if (UiLogic.ValidateAdjacentValsInRec2Load(rec2Load.C85Spec4_1, rec2Load.C85Spec4_2, dlstC85Profile4, lblGetC85ProfileStat))
                {
                    string filter = string.Format("SPEC3 eq '85mm' and SPEC4 eq '{0}'", rec2Load.C85Spec4_1);
                    string partName = string.Empty;
                    if (!UiLogic.findPartBySpecs(filter, ref partName, ref errMsg))
                    {
                        UiLogic.displayErrMsg(lblGetC85ProfileStat, errMsg);
                        return;
                    }
                    else
                    {
                        rec2Load.C85PartName4_1 = partName;
                    }
                }
            }
        }
        protected void dlstC85Amount4_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C85Amount4_1 = int.Parse(dlstC85Amount4.SelectedValue);
        }

        protected void dlstC85Profile5_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            string errMsg = string.Empty;
            if (UiLogic.validateSelectionPairs(dlstC85Profile5, dlstC85Profile4, lblGetC85ProfileStat))
            {
                rec2Load.C85Spec4_2 = dlstC85Profile5.SelectedValue;
                if (UiLogic.ValidateAdjacentValsInRec2Load(rec2Load.C85Spec4_2, rec2Load.C85Spec4_1, dlstC85Profile5, lblGetC85ProfileStat))
                {
                    string filter = string.Format("SPEC3 eq '85mm' and SPEC4 eq '{0}'", rec2Load.C85Spec4_2);
                    string partName = string.Empty;
                    if (!UiLogic.findPartBySpecs(filter, ref partName, ref errMsg))
                    {
                        UiLogic.displayErrMsg(lblGetC85ProfileStat, errMsg);
                        return;
                    }
                    else
                    {
                        rec2Load.C85PartName4_2 = partName;
                    }
                }
            }
        }
        protected void dlstC85Amount5_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C85Amount4_2 = int.Parse(dlstC85Amount5.SelectedValue);
        }
        //public List<string> getC95Profiles4()
        //{
        //    int specNum = 4;
        //    return UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetC85ProfileStat);
        //}
        protected void dlstC95Profile4_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            string errMsg = string.Empty;
            if (UiLogic.validateSelectionPairs(dlstC95Profile4, dlstC95Profile5, lblGetC95ProfileStat))
            {
                rec2Load.C95Spec4_1 = dlstC95Profile4.SelectedValue;
                if (UiLogic.ValidateAdjacentValsInRec2Load(rec2Load.C95Spec4_1, rec2Load.C95Spec4_2, dlstC95Profile4, lblGetC95ProfileStat))
                {
                    string filter = string.Format("SPEC3 eq '95mm' and SPEC4 eq '{0}'", rec2Load.C95Spec4_1);
                    string partName = string.Empty;
                    if (!UiLogic.findPartBySpecs(filter, ref partName, ref errMsg))
                    {
                        UiLogic.displayErrMsg(lblGetC95ProfileStat, errMsg);
                        return;
                    }
                    else
                    {
                        rec2Load.C95PartName4_1 = partName;
                    }
                }
            }
        }
        protected void dlstC95Amount4_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C95Amount4_1 = int.Parse(dlstC95Amount4.SelectedValue);
        }
        public List<string> getC95Profiles5()
        {
            int specNum = 5;
            return UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetC95ProfileStat);
        }
        protected void dlstC95Profile5_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            string errMsg = string.Empty;
            if (UiLogic.validateSelectionPairs(dlstC95Profile5, dlstC95Profile4, lblGetC95ProfileStat))
            {
                rec2Load.C95Spec4_2 = dlstC95Profile5.SelectedValue;
                if (UiLogic.ValidateAdjacentValsInRec2Load(rec2Load.C95Spec4_2, rec2Load.C95Spec4_1, dlstC95Profile5, lblGetC95ProfileStat))
                {
                    string filter = string.Format("SPEC3 eq '95mm' and SPEC4 eq '{0}'", rec2Load.C95Spec4_2);
                    string partName = string.Empty;
                    if (!UiLogic.findPartBySpecs(filter, ref partName, ref errMsg))
                    {
                        UiLogic.displayErrMsg(lblGetC95ProfileStat, errMsg);
                        return;
                    }
                    else
                    {
                        rec2Load.C95PartName4_2 = partName;
                    }
                }
            }

        }
        protected void dlstC95Amount5_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.C95Amount4_2 = int.Parse(dlstC85Amount5.SelectedValue);
        }
        //--
        public List<string> getSpec5()
        {
            int specNum = 5;
            return UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetC85ProfileStat);
        }

        //protected void dlstS85Profile6_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    rec2Load.S85_Soft = dlstS85Profile6.SelectedValue;
        //}
        //protected void dlstS85Amount61_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    rec2Load.S85_Soft_Amount = int.Parse(dlstS85Amount61.SelectedValue);
        //}
        protected void dlstS85Amount61_CheckedChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (Soft85_0.Checked)
            {
                rec2Load.S85_Soft_Amount = 0;
            }
            else if (Soft85_1.Checked)
            {
                rec2Load.S85_Soft_Amount = 1;
            }
            else if (Soft85_2.Checked)
            {
                rec2Load.S85_Soft_Amount = 2;
            }
            else if (Soft85_3.Checked)
            {
                rec2Load.S85_Soft_Amount = 3;
            }
            else if (Soft85_4.Checked)
            {
                rec2Load.S85_Soft_Amount = 4;
            }
            else if (Soft85_5.Checked)
            {
                rec2Load.S85_Soft_Amount = 5;
            }
            else if (Soft85_6.Checked)
            {
                rec2Load.S85_Soft_Amount = 6;
            }
            //rec2Load.S85_Soft_Amount = int.Parse(Soft85Group.SelectedValue);
        }
        protected void dlstH85Amount61_CheckedChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (Hard85_0.Checked)
            {
                rec2Load.S85_Hard_Amount = 0;
            }
            else if (Hard85_1.Checked)
            {
                rec2Load.S85_Hard_Amount = 1;
            }
            else if (Hard85_2.Checked)
            {
                rec2Load.S85_Hard_Amount = 2;
            }
            else if (Hard85_3.Checked)
            {
                rec2Load.S85_Hard_Amount = 3;
            }
            else if (Hard85_4.Checked)
            {
                rec2Load.S85_Hard_Amount = 4;
            }
            else if (Hard85_5.Checked)
            {
                rec2Load.S85_Hard_Amount = 5;
            }
            else if (Hard85_6.Checked)
            {
                rec2Load.S85_Hard_Amount = 6;
            }
            //rec2Load.S85_Soft_Amount = int.Parse(Soft85Group.SelectedValue);
        }
        protected void dlstS95Amount61_CheckedChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (Soft95_0.Checked)
            {
                rec2Load.S95_Soft_Amount = 0;
            }
            else if (Soft95_1.Checked)
            {
                rec2Load.S95_Soft_Amount = 1;
            }
            else if (Soft95_2.Checked)
            {
                rec2Load.S95_Soft_Amount = 2;
            }
            else if (Soft95_3.Checked)
            {
                rec2Load.S95_Soft_Amount = 3;
            }
            else if (Soft95_4.Checked)
            {
                rec2Load.S95_Soft_Amount = 4;
            }
            else if (Soft95_5.Checked)
            {
                rec2Load.S95_Soft_Amount = 5;
            }
            else if (Soft95_6.Checked)
            {
                rec2Load.S95_Soft_Amount = 6;
            }
            //rec2Load.S85_Soft_Amount = int.Parse(Soft85Group.SelectedValue);
        }
        protected void dlstH95Amount61_CheckedChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (Hard95_0.Checked)
            {
                rec2Load.S95_Hard_Amount = 0;
            }
            else if (Hard95_1.Checked)
            {
                rec2Load.S95_Hard_Amount = 1;
            }
            else if (Hard95_2.Checked)
            {
                rec2Load.S95_Hard_Amount = 2;
            }
            else if (Hard95_3.Checked)
            {
                rec2Load.S95_Hard_Amount = 3;
            }
            else if (Hard95_4.Checked)
            {
                rec2Load.S95_Hard_Amount = 4;
            }
            else if (Hard95_5.Checked)
            {
                rec2Load.S95_Hard_Amount = 5;
            }
            else if (Hard95_6.Checked)
            {
                rec2Load.S95_Hard_Amount = 6;
            }
            //rec2Load.S85_Soft_Amount = int.Parse(Soft85Group.SelectedValue);
        }
        //protected void dlstS85Amount62_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    rec2Load.S85_Hard_Amount = int.Parse(dlstS85Amount62.SelectedValue);
        //}
        //protected void dlstS95Profile6_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    rec2Load.S95_Soft = dlstS95Profile6.SelectedValue;
        //}
        //protected void dlstS95Amount61_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    rec2Load.S95_Soft_Amount = int.Parse(dlstS95Amount61.SelectedValue);
        //}
        //protected void dlstS95Amount62_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
        //    rec2Load.S95_Hard_Amount = int.Parse(dlstS95Amount62.SelectedValue);
        //}
        public List<string> getSpec6()
        {
            int specNum = 6;
            return UiLogic.getSpecValues(specNum, "ERPG_SPEC", lblGetS85ProfileStat);
        }
        public List<string> getWeights()
        {
            return new List<string> { "0", "1", "2" };
        }
        protected void radioWeight85_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.Weight85Amount = int.Parse(Weight85.SelectedValue);
            rec2Load.Weight85 = "85mm"; //Should this be dynamic?
        }
        protected void radioWeight95_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            rec2Load.Weight95Amount = int.Parse(Weight95.SelectedValue);
            rec2Load.Weight95 = "95mm"; //Should this be dynamic?
        }
        protected void rbBagGiven_CheckedChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (rbBagGiven.Checked)
            {
                rec2Load.AposTherapyBagGiven = 'Y';
            }

        }
        protected void rbBagNotGiven_CheckedChanged(object sender, EventArgs e)
        {
            Rec2Load rec2Load = (Rec2Load)Session["rec2Load"];
            if (rbBagNotGiven.Checked)
            {
                rec2Load.AposTherapyBagGiven = 'N';
            }

        }
        protected void btnSubmit_click(object sender, EventArgs e)
        {
            UiLogic.btnSubmit_click(sender, lblSubmitStat);
        }
    }
}
