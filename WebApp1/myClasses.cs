﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WebApp1
{
    //public class myClasses
    //{
    //}
    public class Rec2Load
    {
        public char Loaded { get; set; }
        public string ErrDescription { get; set; }
        public string CustDes { get; set; }
        public string CustName { get; set; }
        public DateTime AppointmentDate { get; set; } = DateTime.Today;
        public string AppointmentType { get; set; }
        public string PatientId { get; set; }
        //public DateTime PatientBdate { get; set; } //We don't ask for birthdate only birth year
        public int PatientByear { get; set; }
        public string PatientGender { get; set; }
        public string TherapistName { get; set; }
        public string PatientPostCode { get; set; }
        public string LeadSource { get; set; }
        public string LeadType { get; set; }
        public string AppointmentOutcome { get; set; }
        public char OpenSh { get; set; }
        public string PaymentType { get; set; }
        public char Insured { get; set; }
        public char SelfPaid { get; set; }
        public string InsurerName { get; set; }
        public string PackageTaken { get; set; }
        public string DevSize { get; set; }
        public string DevType { get; set; }
        public string DevicePartName { get; set; }
        public string C85Spec4_1 { get; set; }
        public int C85Amount4_1 { get; set; }
        public string C85PartName4_1 { get; set; }
        public string C85Spec4_2 { get; set; }
        public int C85Amount4_2 { get; set; }
        public string C85PartName4_2 { get; set; }
        public string C95Spec4_1 { get; set; }
        public int C95Amount4_1 { get; set; }
        public string C95PartName4_1 { get; set; }
        public string C95Spec4_2 { get; set; }
        public int C95Amount4_2 { get; set; }
        public string C95PartName4_2 { get; set; }
        public string S85_Soft { get; set; }
        public int S85_Soft_Amount { get; set; }
        public string S85_Hard { get; set; }
        public int S85_Hard_Amount { get; set; }
        public string S95_Soft { get; set; }
        public int S95_Soft_Amount { get; set; }
        public string S95_Hard { get; set; }
        public int S95_Hard_Amount { get; set; }
        public string Weight85 { get; set; }  //from spec7values
        public int Weight85Amount { get; set; }
        public string Weight95 { get; set; }  //from spec7values
        public int Weight95Amount { get; set; }
        public string AttendedFollowUp { get; set; }  //"Yes","No","Canceled"
        public char AposTherapyBagGiven { get; set; } = 'N';
        public string Comments { get; set; }
        public string AppointmentOutcomeFUp { get; set; }
        public char FollowupNumFlag { get; set; }
        public string FollowupNum { get; set; }
        public char PaymentTypeFlag { get; set; }
        public char PricePaidFlag { get; set; }
        public double PricePaid { get; set; }
        public string Passw { get; set; }

    }
    [DataContract]
    public  class ERPG_CUSTSIP_SUBFORM_class
    {
      [DataMember(Order = 1)]
        public int LINE { get; set; }
      [DataMember(Order = 2)]
        public string WARHSNAME { get; set; }
      [DataMember(Order = 3)]
        public string PARTNUM { get; set; }
      [DataMember(Order = 4)]
        public int AMOUNT { get; set; }
     }
    [DataContract]
    public class Rec2API
    {
        //public Rec2Load SiteInterface;
        //"{\"LINE\": 26, \"CUSTNAME\": \"100002\",  \"CUSTDES\": \"Plantin BVBA\",  \"APPOINTMENTDATE\": \"2018-06-14\", \"APPOINTMENTTYPE\":"
        //    + "\"Initial Evaluation\", \"PATIENTID\": \"14\", \"PATIENTYOB\": 1999, \"PATIENTGENDER\": \"Male\", \"PHYSIONAME\": \"Moshe\","
        //    + "\"APPOINTMENTOUTCOME\": \"good\", \"ATTENDEDFOLLOWUP\": \"Y\","   // 
       [DataMember (Order =1)]
        public int LINE { get; set; }
    [DataMember(Order = 2)]
        public string CUSTNAME { get; set; }
    [DataMember(Order = 3)]
        public string CUSTDES { get; set; }
    [DataMember(Order = 4)]
        //public DateTime APPOINTMENTDATE { get; set; }
        public String APPOINTMENTDATE { get; set; }
        [DataMember(Order = 5)]
        public string APPOINTMENTTYPE { get; set; }
    [DataMember(Order = 6)]
        public string PATIENTID { get;set;}
    [DataMember(Order = 7)]
        public int PATIENTYOB { get; set; }
    [DataMember(Order = 8)]
        public string PATIENTGENDER { get; set; }
    [DataMember(Order = 9)]
        public string PHYSIONAME { get; set; }
    [DataMember(Order = 10)]
        public string APPOINTMENTOUTCOME { get; set; }
    [DataMember(Order = 11)]
        public string ATTENDEDFOLLOWUP { get; set; }
    [DataMember(Order = 12)]
        public ERPG_CUSTSIP_SUBFORM_class[] ERPG_CUSTSHIP_SUBFORM;
    }
    public class PasswordClass
    {
        public string ERPG_PASSWORD { get; set; }
    }
    public class ValuesPwd
    {
        public List<PasswordClass> value { get; set; }
    }
    public class PasswordCustName
    {
        public string ERPG_PASSWORD { get; set; }
        public string CUSTNAME { get; set; }
    }
    public class ValuesPwdCustName
    {
        public List<PasswordCustName> value { get; set; }
    }
    public class CustDesClass
    {
        public string CUSTDES { get; set; }
    }
    public class ValuesCustDes
    {
        public List<CustDesClass> value { get; set; }
    }
    public class TherapistClass
    {
        public string NAME { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
    }
    public class ValuesTheraphists
    {
        public List<TherapistClass> CUSTPERSONNEL_SUBFORM { get; set; }
    }
    public class CustPersonnels
    {
        public List<ValuesTheraphists> value { get; set; }
    }
    public class LeadSourceClass
    {
        public string ERPG_LEADSOURCEDES { get; set; }
    }
    public class ValuesLeadSources
    {
        public List<LeadSourceClass> value { get; set; }
    }
    public class LeadTypeClass
    {
        public string ERPG_LEADTYPEDES { get; set; }
    }
    public class ValuesLeadTypes
    {
        public List<LeadTypeClass> value { get; set; }
    }
    public class CustNoteTypeClass // CustNoteTypeClass
    {
        public string ERPG_CUSTNOTETYPEDES { get; set; }
        public string ERPG_OPEN_SH { get; set; }
    }
    public class CustNoteTypes
    {
        public List<CustNoteTypeClass> value { get; set; }
    }
    //-
    public class CustTopicClass // CustNoteTypeClass
    {
        public string TOPICDES { get; set; }
        public string ERPG_FOLLOWUPNUMFLAG { get; set; }
        public string ERPG_PAYMENTTYPEFLAG { get; set; }
        public string ERPG_PRICEPAIDFLAG { get; set; }
    }
    /*public class MyCustTopicClass
    {
        public string TOPICDES { get; set; }
        public string ERPG_FUP_PAYTYPE_PPAID { get; set; }
    }*/
    public class CustTopics
    {
        public List<CustTopicClass> value { get; set; }
    }
    //-
    public class CustSpec1ValueClass
    {
        public string SPECVALUE { get; set; }
        public string ERPG_INSURED { get; set; }
        public string ERPG_SELF_PAYMENT { get; set; }
    }
    public class CustSpec1Values
    {
        public List<CustSpec1ValueClass> value { get; set; }
    }
    public class CustSpec2ValueClass
    {
        public string SPECVALUE { get; set; }
        public string ERPG_NO_PACKAGE { get; set; }
    }
    public class CustSpec2Values
    {
        public List<CustSpec2ValueClass> value { get; set; }
    }
    public class SpecValueClass
    {
        public string SPECVALUE { get; set; }
    }
    public class SpecValues
    {
        public List<SpecValueClass> value { get; set; }
    }
    public class SpecValDesClass
    {
        public string SPECVALDES { get; set; }
    }
    public class SpecValDescriptions
    {
        public List<SpecValDesClass> value { get; set; }
    }
    public class PartNameClass
    {
        public string PARTNAME { get; set; }
    }
    public class PartNameValues
    {
        public List<PartNameClass> value { get; set; }
    }
    public class LineClass
    {
        public int LINE { get; set; }
    }
    public class LineValues
    {
        public List<LineClass> value { get; set; }
    }
    public class SubFormLines
    {
        public int LINE;
        public LineClass[] ERPG_CUSTSHIP_SUBFORM;
    }
}