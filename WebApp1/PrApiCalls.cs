﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Text;

namespace WebApp1
{
    public static class PrApiCalls
    {
        public static RestClient restClient = new RestClient();
        public enum SystemTypeEnum
        {
            NONE,
            US,
            UK,
        };

        public static SystemTypeEnum systemType = SystemTypeEnum.US;
        public static SystemTypeEnum getSystemType()
        {
            string sysType = ConfigurationManager.AppSettings["SystemType"].ToString();
            switch (sysType)
            {
                case "US":
                    return SystemTypeEnum.US;
                case "UK":
                    return SystemTypeEnum.UK;
            }
            return SystemTypeEnum.NONE;
        }
        public static void initRestClient()
        {
            try
            {
                string locale = getSystemType().ToString() == "UK" ? "UK" : "US";

                /* APOSTHERAPY TEST SERVER */
                restClient.BaseUrl = new Uri(string.Format("{0}{1}/{2}", ConfigurationManager.AppSettings["AposTestHost"], ConfigurationManager.AppSettings["AposTestINI"],
                                             ConfigurationManager.AppSettings["AposTestENV_" + locale]));
                restClient.Authenticator = new HttpBasicAuthenticator(ConfigurationManager.AppSettings["AposTestUser"],
                                                 ConfigurationManager.AppSettings["AposTestPWD"]);

                /* HIghCon/EZ-ERP Test Server */
                //restClient.BaseUrl = new Uri(string.Format("{0}{1}/{2}", ConfigurationManager.AppSettings["PriorityHost"], ConfigurationManager.AppSettings["PriorityINI"],
                //             ConfigurationManager.AppSettings["PriorityENV"]));
                //restClient.Authenticator = new HttpBasicAuthenticator(ConfigurationManager.AppSettings["PriorityUser"],
                //                                 ConfigurationManager.AppSettings["PriorityPWD"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string getUserPassword(string userName, ref string errMsg)
        {

            RestRequest request = new RestRequest();
            request.Resource = string.Format("PHONEBOOK?$filter=ERPG_USERNAME eq '{0}'&$select=ERPG_PASSWORD", userName);
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                ValuesPwd val = JsonConvert.DeserializeObject<ValuesPwd>(response.Content);
                return val.value[0].ERPG_PASSWORD;
            }
            else
            {
                errMsg = string.Format("Priority Web API error: {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }

            #region toDel
            //string custName = "000038";
            //request.Resource = string.Format("CUSTOMERS?$filter=CUSTNAME eq '{0}'&$format=application/json;odata.metadata=none&$select=CUST,CREATEDDATE", custName);
            //IRestResponse response = restClient.Execute(request);
            //string jsonValue = string.Empty;
            //if (response.IsSuccessful)
            //{
            //    var passwObjDefinition = new { ERPG_PASSWORD = string.Empty};
            //    jsonValue = response.Content;
            //    var passwObj = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(jsonValue, passwObjDefinition);
            //    return passwObj.ERPG_PASSWORD;
            //}
            //else
            //{
            //    errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
            //    throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
            //}
            //return jsonValue;
            #endregion toDel
        }
        public static string getUserPasswordCustname(string userName, ref string custName, ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = string.Format("PHONEBOOK?$filter=ERPG_USERNAME eq '{0}'&$select=ERPG_PASSWORD,CUSTNAME", userName);
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                ValuesPwdCustName val = JsonConvert.DeserializeObject<ValuesPwdCustName>(response.Content);
                if (val.value.Count > 0)
                {
                    custName = val.value[0].CUSTNAME;
                    if (string.IsNullOrEmpty(custName.Trim()))
                        errMsg = "No Clinic found for the current User";
                    return val.value[0].ERPG_PASSWORD;
                }
                else
                {
                    errMsg = "Wrong UserName or Passowrd";
                    return string.Empty;
                }
            }
            else
            {
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }


        }
        public static string getCustDes(string custName, ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = string.Format("CUSTOMERS?$filter=CUSTNAME eq '{0}'&$select=CUSTDES", custName);
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                ValuesCustDes val = JsonConvert.DeserializeObject<ValuesCustDes>(response.Content);
                string custDes = val.value[0].CUSTDES;
                return custDes;
            }
            else
            {
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }
        public static List<TherapistClass> getTherapists(string custName, ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = string.Format("CUSTOMERS?$filter=CUSTNAME eq '{0}'&$select=CUSTNAME&$expand=CUSTPERSONNEL_SUBFORM($select=NAME,FIRSTNAME,LASTNAME)", custName);
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                CustPersonnels val = JsonConvert.DeserializeObject<CustPersonnels>(response.Content);
                //string custDes = val.value[0].CUSTDES;
                //val.value[0]).CUSTPERSONNEL_SUBFORM[0].NAME
                List<TherapistClass> result = (List<TherapistClass>)val.value[0].CUSTPERSONNEL_SUBFORM;
                return (List<TherapistClass>)val.value[0].CUSTPERSONNEL_SUBFORM;
                //return null;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return null;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }
        public static List<LeadSourceClass> getLeadSources(ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = "LEADSOURCES?$filter=INACTIVE ne 'Y'&$select=ERPG_LEADSOURCEDES";
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                ValuesLeadSources val = JsonConvert.DeserializeObject<ValuesLeadSources>(response.Content);
                List<LeadSourceClass> val1 = new List<LeadSourceClass>();  //val.value;
                foreach (LeadSourceClass ls in val.value)
                {
                    val1.Add(ls);
                }
                return val1;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return null;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }
        public static List<LeadTypeClass> getLeadTypes(ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = "LEADTYPES?$select=ERPG_LEADTYPEDES";
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                ValuesLeadTypes val = JsonConvert.DeserializeObject<ValuesLeadTypes>(response.Content);
                List<LeadTypeClass> val1 = new List<LeadTypeClass>();  //val.value;
                foreach (LeadTypeClass ls in val.value)
                {
                    if (ls != null)
                        val1.Add(ls);
                }
                if (val1.Count == 0)
                {
                    errMsg = "All ERPG_LEADTPEDES fields in LEADTYPES table are empty !";
                    LeadTypeClass ld = new LeadTypeClass();
                    ld.ERPG_LEADTYPEDES = "---";
                    val1.Add(ld);
                }
                return val1;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return null;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }
        public static List<CustNoteTypeClass> getCUSTNOTETYPES(ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = "CUSTNOTETYPES?$select=ERPG_CUSTNOTETYPEDES,ERPG_OPEN_SH";
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                CustNoteTypes val = JsonConvert.DeserializeObject<CustNoteTypes>(response.Content);
                List<CustNoteTypeClass> val1 = new List<CustNoteTypeClass>();  //val.value;
                foreach (CustNoteTypeClass cn in val.value)
                {
                    if (cn.ERPG_CUSTNOTETYPEDES != null)
                    {
                        val1.Add(cn);
                    }
                }
                return val1;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return null;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }
        public static List<CustTopicClass> getCUSTTOPICS(ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = "CUSTTOPICS?$select=TOPICDES,ERPG_FOLLOWUPNUMFLAG,ERPG_PAYMENTTYPEFLAG,ERPG_PRICEPAIDFLAG";
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                CustTopics val = JsonConvert.DeserializeObject<CustTopics>(response.Content);
                List<CustTopicClass> val1 = new List<CustTopicClass>();
                foreach (CustTopicClass cn in val.value)
                {
                    if (cn.TOPICDES != null)
                    {
                        val1.Add(cn);
                    }
                }
                return val1;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return null;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }
        public static List<CustSpec1ValueClass> getCustSpec1Vals(ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = "ERPG_CUSTSPECVALUES?$filter=SPECNUM eq 1&$select=SPECVALUE,ERPG_INSURED,ERPG_SELF_PAYMENT";
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                CustSpec1Values vals = JsonConvert.DeserializeObject<CustSpec1Values>(response.Content);
                List<CustSpec1ValueClass> val1 = new List<CustSpec1ValueClass>();
                foreach (CustSpec1ValueClass cs in vals.value)
                {
                    val1.Add(cs);
                }
                return val1;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return null;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }

        public static List<CustSpec2ValueClass> getCustSpec2Vals(ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = "ERPG_CUSTSPECVALUES?$filter=SPECNUM eq 2&$select=SPECVALUE,ERPG_NO_PACKAGE";
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                CustSpec2Values vals = JsonConvert.DeserializeObject<CustSpec2Values>(response.Content);
                List<CustSpec2ValueClass> val1 = new List<CustSpec2ValueClass>();
                foreach (CustSpec2ValueClass cs in vals.value)
                {
                    val1.Add(cs);
                }
                return val1;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return null;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }

        public static List<SpecValueClass> getSpecVals(string formPrefix, int specNum, ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = string.Format("{0}VALUES?$filter=SPECNUM eq {1}&$select=SPECVALUE", formPrefix, specNum);
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                //CustSpecValues val = JsonConvert.DeserializeObject<CustSpecValues>(response.Content);
                SpecValues vals = JsonConvert.DeserializeObject<SpecValues>(response.Content);
                List<SpecValueClass> val1 = new List<SpecValueClass>();  //val.value;
                foreach (SpecValueClass sv in vals.value)
                {
                    val1.Add(sv);
                }
                return val1;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return null;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }

        public static string getSpecValByDes(string formPrefix, int specNum, string specValue, ref string errMsg) //"85mm")
        {
            //getSpecVals("SPEC", 3, ref errMsg);
            RestRequest request = new RestRequest();
            //  SPEC1VALUES?$filter=SPECVALDES eq '85mm'&$select=SPECVALUE
            //request.Resource = string.Format("{0}{1}VALUES?$filter=SPECVALDES eq '{2}'&$select=SPECVALUE", formPrefix, specNum, specValue);
            request.Resource = string.Format("{0}VALUES?$filter=SPECVALDES eq '{2}' and SPECNUM eq {1}&$select=SPECVALUE", formPrefix, specNum, specValue);
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                //CustSpecValues val = JsonConvert.DeserializeObject<CustSpecValues>(response.Content);
                SpecValues vals = JsonConvert.DeserializeObject<SpecValues>(response.Content);
                List<SpecValueClass> val1 = new List<SpecValueClass>();  //val.value;
                if (vals.value.Count != 1)
                {
                    errMsg = string.Format("error: {0} is not unique SPECVALDES in {1}{2}VALUES table", specValue, formPrefix, specNum);
                    return string.Empty;
                }
                foreach (SpecValueClass sv in vals.value)
                {
                    val1.Add(sv);
                }
                return val1[0].SPECVALUE;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return string.Empty;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                //throw new Exception(response.StatusCode.ToString() + ": " + response.StatusDescription);
                return null;
            }
        }
        public static List<string> getPartBySpecs(string filter, ref string errMsg)
        {
            RestRequest request = new RestRequest();
            request.Resource = string.Format("LOGPART?$filter={0}&$select=PARTNAME", filter);
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                PartNameValues vals = JsonConvert.DeserializeObject<PartNameValues>(response.Content);
                List<PartNameClass> result = new List<PartNameClass>();
                //if (vals.value.Count != 1)
                //{
                //    errMsg = string.Format(@"error: ""filter={0} does not identify a unique Part", filter);
                //    return null;
                //}
                List<string> lstResult = new List<string>();
                foreach (PartNameClass sv in vals.value)
                {
                    result.Add(sv);
                    lstResult.Add(sv.PARTNAME);
                }
                return lstResult;

            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return null;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                return null;
            }
        }
        public static int getERPG_SITEINTERFACE_nextLine()
        {
            string errMsg = string.Empty;
            RestRequest request = new RestRequest();
            request.Resource = "ERPG_SITEINTERFACE?$select=LINE&$orderby=LINE desc";
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                LineValues vals = JsonConvert.DeserializeObject<LineValues>(response.Content);
                if (vals == null || vals.value.Count == 0)
                    return 1;

                LineClass lc1 = (LineClass)vals.value[0];
                return lc1.LINE + 1;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return 1;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                return 1;
            }
        }
        public static int getERPG_CUSTSHIP_nextLine(int HeaderLine)
        {
            string errMsg = string.Empty;
            RestRequest request = new RestRequest();
            request.Resource = string.Format("ERPG_SITEINTERFACE({0})?$select=LINE&$expand=ERPG_CUSTSHIP_SUBFORM($select=LINE)", HeaderLine);
            IRestResponse response = restClient.Execute(request);
            if (response.IsSuccessful)
            {
                string jsonValue = response.Content;
                SubFormLines vals = JsonConvert.DeserializeObject<SubFormLines>(response.Content);
                //vals.ERPG_CUSTSHIP_SUBFORM[0].LINE

                int maxLine = (vals.ERPG_CUSTSHIP_SUBFORM).Max(t => t.LINE);
                return maxLine + 1;
            }
            else
            {
                if (response.StatusDescription.ToLower() == "not found")
                    return 1;
                errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
                return 1;
            }
        }
        #region toDel
        //public static List<string> getPartBySpecs12(string spec1, string spec2, ref string errMsg)
        //{
        //    RestRequest request = new RestRequest();
        //    request.Resource = string.Format("LOGPART?$filter=SPEC1 eq '{0}' and SPEC2 eq {1}&$select=PARTNAME", spec1, spec2);
        //    IRestResponse response = restClient.Execute(request);
        //    if (response.IsSuccessful)
        //    {
        //        string jsonValue = response.Content;
        //        PartNameValues vals = JsonConvert.DeserializeObject<PartNameValues>(response.Content);
        //        List<PartNameClass> result = new List<PartNameClass>(); 
        //        if (vals.value.Count != 1)
        //        {
        //            errMsg = string.Format("error: Size={0} and Type={1} do not identify a unique Part", spec1, spec2);
        //            return null;
        //        }
        //        List<string> lstResult = new List<string>();
        //        foreach (PartNameClass sv in vals.value)
        //        {
        //            result.Add(sv);
        //            lstResult.Add(sv.PARTNAME);
        //        }
        //        return lstResult;

        //    }
        //    else
        //    {
        //        if (response.StatusDescription.ToLower() == "not found")
        //            return null;
        //        errMsg = string.Format("Priority Web API error : {0}", response.StatusDescription);
        //        return null;
        //    }
        //}
        #endregion
        public static string JsonSerializer<T>(T t)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }
        /// <summary>
        /// Send payload to Priority
        /// </summary>
        /// <param name="Payload">The payload to send to Priority a json record </param>
        /// <returns>IRestResponse Object reguardless of success or failure</returns>
        public static IRestResponse SendToPriority(string form, string Payload)
        {

            //RestClient client = new RestClient();
            //client.BaseUrl = new System.Uri(Program.config["APIURL"]);
            //client.Authenticator = new HttpBasicAuthenticator(Program.config["PriorityUser"], Program.config["PriorityPWD"]);

            RestRequest request = new RestRequest();
            request.Method = Method.POST;
            request.RequestFormat = DataFormat.Json;
            request.Resource = form; //"CUSTOMERS";  // name of the form to populate

            request.AddParameter("application/json", Payload, ParameterType.RequestBody);

            IRestResponse response = restClient.Execute(request);
            string errMsg;
            if (!response.IsSuccessful)
                errMsg = response.StatusDescription;

            return response;
        }

    }
}