﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Drawing;


namespace WebApp1
{
    public static class UiLogic
    {
        public static void displayErrMsg(Label lbl, string errMsg)
        {
            lbl.Text = errMsg;
            lbl.ForeColor = Color.Red;
            lbl.Visible = true;
        }
        public static void txtUserName_TextChanged(object sender, Label lblPwdErr, TextBox txtPassword, ref Rec2Load rec2Load)
        {
            string errMsg = string.Empty;
            TextBox txtUserName = (TextBox)sender;
            string custName = string.Empty;
            string passwordInDb = PrApiCalls.getUserPasswordCustname(txtUserName.Text, ref custName, ref errMsg);
            //Rec2load rec2Load = (Rec2Load)Session["rec2Load"];

            rec2Load.CustName = custName;
            if (!string.IsNullOrEmpty(errMsg))
            {
                UiLogic.displayErrMsg(lblPwdErr, errMsg);
                txtUserName.Focus();
                return;
            }
            rec2Load.Passw = passwordInDb;
            txtPassword.Focus();
        }
        public static List<string> getTherapists(Label lblMsg, Rec2Load rec2Load)
        {
            string errMsg = string.Empty;
            if (string.IsNullOrEmpty(rec2Load.CustName))
            {
                UiLogic.displayErrMsg(lblMsg, "Unexpected Error - The Current User is not connected to a Clinic");
                return new List<string> { string.Empty }; //null;

                //if (rec2Load.AppointmentType == "Initial Evaluation" || rec2Load.AppointmentType == "Review")
                //{
                //    UiLogic.displayErrMsg(lblMsg, errMsg);
                //    return null;
                //}
                //else
                //UiLogic.displayErrMsg(lblGetTherapistsFlUpStat, errMsg);

                // return null;
            }
            else
            {

                List<TherapistClass> lstTherapists = PrApiCalls.getTherapists(rec2Load.CustName, ref errMsg);
                List<string> result = new List<string> { string.Empty };
                if (lstTherapists == null)
                {
                    UiLogic.displayErrMsg(lblMsg, "No therapists found for this user");
                    return result;
                }
                foreach (TherapistClass tp in lstTherapists)
                {
                    //result.Add(string.Format("{0}#{1}#{2}", tp.NAME, tp.FIRSTNAME, tp.LASTNAME));
                    result.Add(tp.NAME);
                }
                return result;
            }
        }
        public static List<string> getLeadSources(Label lblMsg)
        {
            string errMsg = string.Empty;

            List<LeadSourceClass> lstLeadSources = PrApiCalls.getLeadSources(ref errMsg);
            List<string> result = new List<string>();
            foreach (LeadSourceClass ls in lstLeadSources)
            {
                result.Add(ls.ERPG_LEADSOURCEDES);
            }
            return result;

        }
        public static List<string> getLeadTypes(Label lblMsg)
        {
            string errMsg = string.Empty;

            List<LeadTypeClass> lstLeadTypes = PrApiCalls.getLeadTypes(ref errMsg);
            List<string> result = new List<string>();
            foreach (LeadTypeClass ls in lstLeadTypes)
            {
                if (ls.ERPG_LEADTYPEDES != null)
                    result.Add(ls.ERPG_LEADTYPEDES);
            }
            result.Add("other");
            return result;
        }
        public static List<string> getCUSTNOTETYPES(Label lblMsg)//getCUSTNOTETYPES ()
        {
            string errMsg = string.Empty;
            List<CustNoteTypeClass> custNoteTypes = PrApiCalls.getCUSTNOTETYPES(ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                displayErrMsg(lblMsg, errMsg);
                return null;
            }
            else
            {
                List<string> result = new List<string> { string.Empty };
                foreach (CustNoteTypeClass cn in custNoteTypes)
                {
                    string openShFlag = (string.IsNullOrEmpty(cn.ERPG_OPEN_SH) ? "N" : cn.ERPG_OPEN_SH);
                    // result.Add(cn.CUSTNOTETYPEDES + "#" + openShFlag); //string.Format("{0}#{1}}", cn.CUSTNOTETYPEDES, openShFlag));
                    result.Add(cn.ERPG_CUSTNOTETYPEDES);
                }
                return result;
            }
        }
        public static List<string> getCUSTTOPICS(Label lblMsg)
        {
            string errMsg = string.Empty;
            List<CustTopicClass> custTopics = PrApiCalls.getCUSTTOPICS(ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                displayErrMsg(lblMsg, errMsg);
                return null;
            }
            else
            {
                List<string> result = new List<string> { string.Empty };
                foreach (CustTopicClass cn in custTopics)
                {
                    string followUpFlag = (string.IsNullOrEmpty(cn.ERPG_FOLLOWUPNUMFLAG) ? "N" : cn.ERPG_FOLLOWUPNUMFLAG);
                    string paymentTypeFlag = (string.IsNullOrEmpty(cn.ERPG_PAYMENTTYPEFLAG) ? "N" : cn.ERPG_PAYMENTTYPEFLAG);
                    string pricePaidFlag = (string.IsNullOrEmpty(cn.ERPG_PRICEPAIDFLAG) ? "N" : cn.ERPG_PRICEPAIDFLAG);
                    //result.Add(cn.TOPICDES + "#" + followUpFlag + "#" + paymentTypeFlag + "#" + pricePaidFlag);
                    result.Add(cn.TOPICDES);
                }
                return result;
            }
        }
        public static List<string> getSpecValues(int specNum, string formPrefix, Label lblErr)
        {
            string errMsg = string.Empty;
            List<SpecValueClass> pTypes = PrApiCalls.getSpecVals(formPrefix, specNum, ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                UiLogic.displayErrMsg(lblErr, errMsg);
                return null;
            }
            if (pTypes == null)
            {
                UiLogic.displayErrMsg(lblErr, string.Format("Form {0}VALUES returns no values with SPECNUM={1}", formPrefix, specNum));
                return null;
            }
            else
            {
                List<string> result = new List<string> { string.Empty };
                foreach (SpecValueClass sv in pTypes)
                {
                    result.Add(sv.SPECVALUE);
                }
                return result;
            }
        }

        public static string convertNull2N(string str)
        {
            string val = string.Empty;
            val = (str == null || string.IsNullOrEmpty(str) ? "N" : str[0].ToString());
            return val;
        }
        public static bool checkDeviceSizeAndType(ref string partName, ref string errMsg)
        {
            Rec2Load rec2Load = (Rec2Load)HttpContext.Current.Session["rec2Load"];
            partName = string.Empty;
            errMsg = string.Empty;
            string filter = string.Format("SPEC1 eq '{0}' and SPEC2 eq '{1}'", rec2Load.DevType, rec2Load.DevSize);
            List<string> lstPartNames = PrApiCalls.getPartBySpecs(filter, ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
                return false;

            if (lstPartNames.Count == 0)
            {
                errMsg = string.Format("error: There is no part with : Size={0} and Type={1}", rec2Load.DevSize, rec2Load.DevType);
                return false;
            }
            if (lstPartNames.Count > 1)
            {
                errMsg = string.Format("error: there are {0} parts with : Size={0} and Type={1}", lstPartNames.Count, rec2Load.DevSize, rec2Load.DevType);
                return false;
            }
            partName = lstPartNames[0];
            return true;
        }
        public static bool findPartBySpecs(string filter, ref string partName, ref string errMsg)
        {
            List<string> lstPartNames = PrApiCalls.getPartBySpecs(filter, ref errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return false;
            }
            if (lstPartNames.Count == 0)
            {
                errMsg = string.Format("error:  No Part satisfies the condition: {0}", filter);
                return false;
            }
            if (lstPartNames.Count > 1)
            {
                errMsg = string.Format("error:  There are {0} parts that satisfy the condition: {1}", lstPartNames.Count, filter);
                return false;
            }
            partName = lstPartNames[0];
            return true;
        }

        public static void btnSubmit_click(object sender, Label lblSubmitStat)
        {
            string errMsg = string.Empty;
            Rec2Load rec2Load = (Rec2Load)HttpContext.Current.Session["rec2Load"];
            //UiLogic.validateSelectionPairs(dlstWeight85, dlstWeight95, lblGetSpec7ValuesStat);

            ////string payload4 =
            ////"{\"LINE\": 23, \"CUSTNAME\": \"100002\",  \"CUSTDES\": \"Plantin BVBA\",  \"APPOINTMENTDATE\": \"2018-06-14\", \"APPOINTMENTTYPE\":"
            ////+ "\"Initial Evaluation\", \"PATIENTID\": \"14\", \"PATIENTYOB\": 1999, \"PATIENTGENDER\": \"Male\", \"PHYSIONAME\": \"Moshe\","
            ////+ "\"APPOINTMENTOUTCOME\": \"good\", \"ATTENDEDFOLLOWUP\": \"Y\"}";   //Created .

            ////string payload5 =
            ////"{\"LINE\": 25, \"CUSTNAME\": \"100002\",  \"CUSTDES\": \"Plantin BVBA\",  \"APPOINTMENTDATE\": \"2018-06-14\", \"APPOINTMENTTYPE\":"
            ////+ "\"Initial Evaluation\", \"PATIENTID\": \"14\", \"PATIENTYOB\": 1999, \"PATIENTGENDER\": \"Male\", \"PHYSIONAME\": \"Moshe\","
            ////+ "\"APPOINTMENTOUTCOME\": \"good\", \"ATTENDEDFOLLOWUP\": \"Y\","   // end of parent form .
            ////+ "\"ERPG_CUSTSHIP_SUBFORM\": [ {\"LINE\": 1,\"WARHSNAME\": \"WIP\", \"PARTNUM\": \"3-0155\", \"AMOUNT\": 2 }]}";  //Created in Highcon

            ////string payload6 =
            ////"{\"LINE\": 26, \"CUSTNAME\": \"100002\",  \"CUSTDES\": \"Plantin BVBA\",  \"APPOINTMENTDATE\": \"2018-06-14\", \"APPOINTMENTTYPE\":"
            ////+ "\"Initial Evaluation\", \"PATIENTID\": \"14\", \"PATIENTYOB\": 1999, \"PATIENTGENDER\": \"Male\", \"PHYSIONAME\": \"Moshe\","
            ////+ "\"APPOINTMENTOUTCOME\": \"good\", \"ATTENDEDFOLLOWUP\": \"Y\","   // end of parent form .
            ////+ "\"ERPG_CUSTSHIP_SUBFORM\": [ {\"LINE\": 1,\"WARHSNAME\": \"WIP\", \"PARTNUM\": \"3-0155\", \"AMOUNT\": 2 },"      //line 1
            ////                             + "{\"LINE\": 2,\"WARHSNAME\": \"WIP\", \"PARTNUM\": \"3-0156\", \"AMOUNT\": 3 } ]}";   //created  in Highcon


            int nextLine = PrApiCalls.getERPG_SITEINTERFACE_nextLine();
            //int nextShipLine = PrApiCalls.getERPG_CUSTSHIP_nextLine(1);

            Rec2API rec2Api = new Rec2API();
            rec2Api.LINE = PrApiCalls.getERPG_SITEINTERFACE_nextLine();
            rec2Api.CUSTNAME = rec2Load.CustName;
            rec2Api.CUSTDES = rec2Load.CustDes; ;
            rec2Api.APPOINTMENTDATE = string.Format("{0}-{1}-{2}", rec2Load.AppointmentDate.Year.ToString("0000"), rec2Load.AppointmentDate.Month.ToString("00"),
                                                                   rec2Load.AppointmentDate.Day.ToString("00"));
            rec2Api.APPOINTMENTTYPE = rec2Load.AppointmentType;
            rec2Api.PATIENTID = rec2Load.PatientId;
            rec2Api.PATIENTYOB = rec2Load.PatientByear;
            rec2Api.PATIENTGENDER = rec2Load.PatientGender;
            rec2Api.PHYSIONAME = rec2Load.TherapistName;
            rec2Api.APPOINTMENTOUTCOME = rec2Load.AppointmentOutcome;
            rec2Api.ATTENDEDFOLLOWUP = rec2Load.AttendedFollowUp.ToString();

            //rec2Api.CUSTNAME = "100002";
            //rec2Api.CUSTDES = "Plantin BVBA";
            //rec2Api.APPOINTMENTDATE = "2018-06-19";//DateTime.ParseExact(@"19/06/2018", @"dd/MM/yyyy", CultureInfo.CurrentCulture);
            //rec2Api.APPOINTMENTTYPE = "Initial Evaluation";
            //rec2Api.PATIENTID = "17";
            //rec2Api.PATIENTYOB = 2000;
            //rec2Api.PATIENTGENDER = "Female";
            //rec2Api.PHYSIONAME = "Avi";
            //rec2Api.APPOINTMENTOUTCOME = "Excellent";
            //rec2Api.ATTENDEDFOLLOWUP = "N";

            if (rec2Load.OpenSh == 'Y')
            {
                List<ERPG_CUSTSIP_SUBFORM_class> lstCustSips = new List<ERPG_CUSTSIP_SUBFORM_class>();
                ERPG_CUSTSIP_SUBFORM_class custShip = new ERPG_CUSTSIP_SUBFORM_class();
                //  assumming Device part is mandatory , creating LINE 1 of the Ship order for it.
                custShip.LINE = 1;
                //custShip.WARHSNAME = "3444";
                custShip.PARTNUM = rec2Load.DevicePartName;
                custShip.AMOUNT = 1;
                lstCustSips.Add(custShip);
                custShip = new ERPG_CUSTSIP_SUBFORM_class();
                int lastShipLine = 1;

                if (!string.IsNullOrEmpty(rec2Load.C85PartName4_1))
                {
                    custShip.LINE = lastShipLine + 1;
                    //custShip.WARHSNAME = "3444";
                    custShip.PARTNUM = rec2Load.C85PartName4_1;
                    custShip.AMOUNT = rec2Load.C85Amount4_1;
                    lstCustSips.Add(custShip);
                    lastShipLine = custShip.LINE;
                }
                if (!string.IsNullOrEmpty(rec2Load.C85PartName4_2))
                {
                    custShip.LINE = lastShipLine + 1;
                    custShip.PARTNUM = rec2Load.C85PartName4_2;
                    custShip.AMOUNT = rec2Load.C85Amount4_2;
                    lstCustSips.Add(custShip);
                    lastShipLine = custShip.LINE;
                }
                if (!string.IsNullOrEmpty(rec2Load.C95PartName4_1))
                {
                    custShip.LINE = lastShipLine + 1;
                    custShip.PARTNUM = rec2Load.C95PartName4_1;
                    custShip.AMOUNT = rec2Load.C95Amount4_1;
                    lstCustSips.Add(custShip);
                    lastShipLine = custShip.LINE;
                }
                if (!string.IsNullOrEmpty(rec2Load.C95PartName4_2))
                {
                    custShip.LINE = lastShipLine + 1;
                    custShip.PARTNUM = rec2Load.C95PartName4_2;
                    custShip.AMOUNT = rec2Load.C95Amount4_2;
                    lstCustSips.Add(custShip);
                    lastShipLine = custShip.LINE;
                }

                rec2Api.ERPG_CUSTSHIP_SUBFORM = lstCustSips.ToArray();
            }
            string payload6_1 = PrApiCalls.JsonSerializer<Rec2API>(rec2Api);
            PrApiCalls.SendToPriority("ERPG_SITEINTERFACE", payload6_1);     //Created in APPOS US firm (Company) .

            //PrApiCalls.SendToPriority("ERPG_SITEINTERFACE", payload6);

            if (!UiLogic.ready2Submit(rec2Load, ref errMsg))
            {
                UiLogic.displayErrMsg(lblSubmitStat, errMsg);
                return;
            }
            else
            {
                //do submit 
                lblSubmitStat.Text = "Passed data to Priority";
                lblSubmitStat.ForeColor = Color.Green;
            }
        }
        public static bool ready2Submit(Rec2Load rec2Load, ref string errMsg)
        {
            string errTtl = "One of the required fields :";
            string errEnd = "<br/>didn't get value or got a wrong value";

            bool ready = rec2Load.AppointmentDate >= DateTime.Now.Date
                         && !string.IsNullOrEmpty(rec2Load.AppointmentOutcome)
                         && (rec2Load.OpenSh == 'Y' || rec2Load.OpenSh == 'N')
                         && !string.IsNullOrEmpty(rec2Load.AppointmentType)
                         && !string.IsNullOrEmpty(rec2Load.CustDes)
                         && !string.IsNullOrEmpty(rec2Load.CustName)
                         && !string.IsNullOrEmpty(rec2Load.PatientId)
                         && !string.IsNullOrEmpty(rec2Load.TherapistName);
            if (!ready)
            {
                errMsg = errTtl;
                errMsg += " Appointment Date, AppointmentOutcome, Appointment Type, "
                         + @"<br/>  Clinic, Patient ID, Therapist Name "
                         + errEnd;                     // didn't get value or got a wrong value";
                return false;
            }

            if (rec2Load.AppointmentType == "Initial Evaluation")
            {
                ready &= !string.IsNullOrEmpty(rec2Load.PatientGender)
                        && (rec2Load.PatientGender == "Male" || rec2Load.PatientGender == "Female")
                        && rec2Load.PatientByear > 0; // Checking Patient Year of Birth is a positive number
                                                      //&& rec2Load.PatientBdate > DateTime.Now.AddYears(-120);  //just checking that PatientBdate is not empty 

                if (!ready)
                {
                    if (string.IsNullOrEmpty(errMsg))
                        errMsg = errTtl;

                    errMsg += @"<br/> Patient's Gender, Patient's Birth Year "
                              + errEnd;
                    return false;
                }
            }

            if (rec2Load.AppointmentType == "Follow Up")
            {
                ready &= (rec2Load.AttendedFollowUp == 'Y' || rec2Load.AttendedFollowUp == 'N');
                if (!ready)
                {
                    if (string.IsNullOrEmpty(errMsg))
                        errMsg = errTtl;

                    errMsg += @"<br/> AttendedFollowup" + errEnd;
                    return false;
                }
            }

            if (rec2Load.AppointmentType == "Initial Evaluation"
                || (rec2Load.AppointmentType == "Follow Up" && rec2Load.AttendedFollowUp == 'Y'))
            {
                ready &= !string.IsNullOrEmpty(rec2Load.DevSize)
                         && !string.IsNullOrEmpty(rec2Load.DevType)
                         && !string.IsNullOrEmpty(rec2Load.C85Spec4_1)
                         && !string.IsNullOrEmpty(rec2Load.C85Spec4_2)
                         && !string.IsNullOrEmpty(rec2Load.C95Spec4_1)
                         && !string.IsNullOrEmpty(rec2Load.C95Spec4_2)
                         && !string.IsNullOrEmpty(rec2Load.S85_Soft)
                         && !string.IsNullOrEmpty(rec2Load.S95_Soft);


                if (!ready)
                {
                    if (string.IsNullOrEmpty(errMsg))
                        errMsg = errTtl;

                    errMsg += @"<br/> Device1, Device2, Convex Cap 85mm, 95mm, Spacer 85mm, 95mm"
                             + errEnd;
                    return false;
                }

                ready &= (rec2Load.C85Amount4_1 + rec2Load.C85Amount4_2 +
                             rec2Load.C95Amount4_1 + rec2Load.C95Amount4_2 != 4);

                if (!ready)
                {
                    if (string.IsNullOrEmpty(errMsg))
                        errMsg = errTtl;

                    errMsg += @"<br/> Convex Cap 85mm and 95mm total amounts is not 4"
                              + errEnd;
                    return false;
                }

                //This check is incorrect.  Weight85 (& Weight95) should be loaded automatically 
                ready &= !string.IsNullOrEmpty(rec2Load.Weight85)
                         && (rec2Load.AposTherapyBagGiven == 'Y' || rec2Load.AposTherapyBagGiven == 'N');
                if (!ready)
                {
                    if (string.IsNullOrEmpty(errMsg))
                        errMsg = errTtl;

                    errMsg += @"<br/> Convex Cap 85mm and 95mm total amounts is not 4"
                              + errEnd;
                    return false;
                }
            }

            ready &= rec2Load.OpenSh == 'Y'
                     && !string.IsNullOrEmpty(rec2Load.PaymentType)
                     && rec2Load.Insured == 'N'
                     && !string.IsNullOrEmpty(rec2Load.InsurerName)
                     && !string.IsNullOrEmpty(rec2Load.PackageTaken);

            if (!ready)
            {
                if (string.IsNullOrEmpty(errMsg))
                    errMsg = errTtl;

                errMsg += @"<br/> Payment Type, Insurer Name, Package Taken";
                errMsg += @"<br/> didn't get value or got a wrong value";
                return false;
            }



            return ready;
        }
        // I think this is no longer nessasary - Abe (20/6/18)
        public static bool validateSelectionPairs(DropDownList currDlst, DropDownList adjacentDlst, Label lblMsg)
        {
            if (currDlst.SelectedValue == null
                || adjacentDlst.SelectedValue == null
                || string.IsNullOrEmpty(currDlst.SelectedValue)
                || string.IsNullOrEmpty(adjacentDlst.SelectedValue))
                return true;

            if (currDlst.SelectedValue == adjacentDlst.SelectedValue)
            {
                displayErrMsg(lblMsg, "You must select two different values in a line");
                currDlst.ClearSelection();
                return false;
            }
            else if (!string.IsNullOrEmpty(lblMsg.Text))
            {
                lblMsg.Text = string.Empty;
                return true;
            }
            return true;
        }
        public static bool ValidateAdjacentValsInRec2Load(string val1, string val2, DropDownList currDlst, Label lblMsg)
        {
            if (!string.IsNullOrEmpty(val1) && !string.IsNullOrEmpty(val2))
            {
                if (val1 == val2)
                {
                    displayErrMsg(lblMsg, "You must select two different values in a line");
                    currDlst.ClearSelection();
                    return false;
                }
                else
                    return true;
            }
            else if (!string.IsNullOrEmpty(val1) || !string.IsNullOrEmpty(val2))
                return true;
            else
                return false;   //Both values are empty - do not try to get partNumebr for these.
        }

    }
}