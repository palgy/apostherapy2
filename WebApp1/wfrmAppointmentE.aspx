﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="wfrmAppointmentE.aspx.cs" Inherits="WebApp1.wfrmAppointmentE" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="MyStyles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="formE" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <img src="Images/Apos_Logo.jpg" class="logo" />
        <div id="Title" class="segment_header">
            <h1>Appointment Information</h1>
        </div>
        <!-- end Title -->
        <div id="divLogin" runat="server" class="segment">
            <asp:Label ID="lblUserName" runat="server" Text="UserName" CssClass="label"></asp:Label>
            <asp:TextBox ID="txtUserName" runat="server" ClientIDMode="Static" AutoPostBack="true"
                OnTextChanged="txtUserName_TextChanged"></asp:TextBox>
            <asp:Label ID="lblPassword" runat="server" Text="Password" CssClass="label"></asp:Label>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" OnTextChanged="txtPassword_TextChanged"
                AutoPostBack="true">
            </asp:TextBox>
            <asp:Label ID="lblPwdErr" runat="server" Text="" Visible="false"></asp:Label>
        </div>
        <!--end divLogin -->
        <asp:Panel ID="PanelAppointment" runat="server">
            <div id="divAppointmentHdrE" runat="server">
                <asp:Label ID="lblClinicName" runat="server" Text="Clinic Name" CssClass="label"></asp:Label>
                <asp:TextBox ID="txtClinicName" runat="server" ReadOnly="true"></asp:TextBox>
                <asp:Label ID="lblCustDesErr" runat="server" Text=""></asp:Label>
                <asp:UpdatePanel ID="updPannelTherapist" runat="server">
                    <ContentTemplate>
                        <label>Physiotherapist Name</label>
                        <asp:RadioButtonList ID="radioTherapist" runat="server"
                            AutoPostBack="true"
                            SelectMethod="getRadioTherapists"
                            RepeatLayout="UnorderedList"
                            OnSelectedIndexChanged="radioTherapist_SelectedIndexChanged">
                        </asp:RadioButtonList>

                        <asp:Label ID="lblGetTherapistsStat" runat="server" Text="" CssClass="midCtl"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- end updPannelTherapist-->
                <asp:UpdatePanel ID="updPannelAppDate" runat="server">
                    <ContentTemplate>
                        <label>Appointment Date</label>
                        <asp:TextBox ID="txtAppointmentDate" TextMode="Date" runat="server" AutoPostBack="true"
                            ClientIDMode="Static" MaxLength="10"
                            OnTextChanged="txtAppointmentDate_TextChanged">
                        </asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- updPannelAppDate -->

                <asp:UpdatePanel ID="updPanelBdate" runat="server">
                    <ContentTemplate>
                        <label>Patient Year of Birth</label>
                        <asp:DropDownList ID="dlstPatientBYear" runat="server"
                            AutoPostBack="true"
                            SelectMethod="getBYears"
                            OnSelectedIndexChanged="dlstPatientBYear_SelectedIndexChanged">
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- end updPanelBdate -->
                <asp:UpdatePanel ID="updPanelGender" runat="server">
                    <ContentTemplate>
                        <label>Patient Gender</label>
                        <asp:RadioButtonList ID="PatientGender" runat="server"
                            AutoPostBack="true"
                            SelectMethod="getRadioGender"
                            RepeatLayout="UnorderedList"
                            OnSelectedIndexChanged="radioGender_SelectedIndexChanged">
                        </asp:RadioButtonList>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- end  updPaneGender -->

                <asp:UpdatePanel ID="updPanelPostCode" runat="server">
                    <ContentTemplate>
                        <label>Patient Postcode</label>
                        <asp:TextBox ID="txtPatientPostCode" runat="server" OnTextChanged="txtPatientPostCode_TextChanged"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!--end updPanelPostCode -->

                <asp:UpdatePanel ID="updPannelLeadSource" runat="server">
                    <ContentTemplate>
                        <label>Lead Source</label>
                        <asp:RadioButtonList ID="LeadSources" runat="server"
                            AutoPostBack="true"
                            SelectMethod="getLeadSources"
                            RepeatLayout="UnorderedList"
                            OnSelectedIndexChanged="radioLeadSources_SelectedIndexChanged">
                        </asp:RadioButtonList>
                        <asp:Label ID="lblGetLeadSourcesStat" runat="server" Text="" CssClass="midCtl"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- end updPannelLeadSource-->

                <asp:UpdatePanel ID="updPannelLeadType" runat="server">
                    <ContentTemplate>
                        <label class="no-colon">Where did the Patient hear about AposTherapy?</label>
                        <asp:CheckBoxList ID="LeadTypes" runat="server"
                            AutoPostBack="true"
                            SelectMethod="getLeadTypes"
                            RepeatLayout="UnorderedList"
                            OnSelectedIndexChanged="checkboxLeadTypes_SelectedIndexChanged">
                        </asp:CheckBoxList>
<%--                        <asp:RadioButtonList ID="LeadTypes" runat="server"
                            AutoPostBack="true"
                            SelectMethod="getLeadTypes"
                            RepeatLayout="UnorderedList"
                            OnSelectedIndexChanged="radioLeadTypes_SelectedIndexChanged">
                        </asp:RadioButtonList>--%>
                        <asp:TextBox ID="LeadTypeOther" runat="server" AutoPostBack="true" OnTextChanged="LeadTypeOther_TextChanged" Visible="false" CssClass="otherOption"></asp:TextBox>
                        <asp:Label ID="lblGetLeadTypesStat" runat="server" Text="" CssClass="midCtl"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- end updPannelLeadType-->

                <asp:UpdatePanel ID="updPanelPatientID" runat="server">
                    <ContentTemplate>
                        <label>Unique Patient ID</label>
                        <asp:TextBox ID="txtPatientID" runat="server" AutoPostBack="true"
                            OnTextChanged="txtPatientID_TextChanged">
                        </asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div id="divAppType" runat="server">
                    <label>Appointment Type</label>
                    <asp:RadioButtonList ID="radioAppointmentType" runat="server"
                        AutoPostBack="true"
                        SelectMethod="getAppointmentType"
                        DataTextField="APPTYPE"
                        DataValueField="APPTYPECODE"
                        RepeatLayout="UnorderedList"
                        OnSelectedIndexChanged="radioAppointmentType_SelectedIndexChanged">
                    </asp:RadioButtonList>
                </div>
                <!-- end divAppType -->
            </div>
            <!-- end divAppointmentHdrE -->

            <div id="divInitEval" runat="server" visible="false">
                <div id="divAppOutcomeIEval" runat="server">
                    <label>Appointment Outcome</label>
                    <asp:RadioButtonList ID="radioAppOutcomeIEval" runat="server"
                        AutoPostBack="true"
                        SelectMethod="getCUSTNOTETYPES"
                        RepeatLayout="UnorderedList"
                        OnSelectedIndexChanged="radioAppOutcomeIEval_SelectedIndexChanged">
                    </asp:RadioButtonList>
                    <asp:Label ID="lblGetAppOutcomesIEvalStat" runat="server" Visible="false" CssClass="midCtl"></asp:Label>

                </div>
                <!-- end divAppOutcomeIEval -->
            </div>
            <!-- end divInitEval-->
            <div id="divFollowUp" runat="server" visible="false">
                <div id="divAppOutcomeFUp" runat="server">
                    <label>Appointment Outcome</label>
                    <asp:RadioButtonList ID="radioAppOutcomeFUp" runat="server"
                        AutoPostBack="true"
                        SelectMethod="getCUSTTOPICS"
                        RepeatLayout="UnorderedList"
                        OnSelectedIndexChanged="radioAppOutcomeFUp_SelectedIndexChanged">
                    </asp:RadioButtonList>
                    <asp:Label ID="lblGetAppOutcomesFUpStat" runat="server" Visible="false" CssClass="midCtl"></asp:Label>
                </div>
                <!-- end divAppOutcomeFup -->
            </div>
            <!-- end divFollowUp -->
            <asp:UpdatePanel ID="updPanelFollupNum" runat="server" Visible="false">
                <ContentTemplate>
                    <label>Followup Number</label>
                    <asp:RadioButtonList ID="radioFollowupNum" runat="server" Visible="false"
                        AutoPostBack="true"
                        SelectMethod="getFollowupNums"
                        RepeatLayout="UnorderedList"
                        OnSelectedIndexChanged="radioFollowupNum_SelectedIndexChanged">
                    </asp:RadioButtonList>
                    <asp:Label ID="lblFollowupNumsStat" runat="server" Visible="false" CssClass="midCtl"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- end updPanelFollupNum -->
            <asp:UpdatePanel ID="updPanelPayment" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblPaymentType" runat="server" Text="Payment Type" Visible="false" CssClass="label"></asp:Label>
                    <asp:RadioButtonList ID="PaymentType" runat="server" Visible="false"
                        AutoPostBack="true"
                        SelectMethod="getPaymentTypes"
                        RepeatLayout="UnorderedList"
                        OnSelectedIndexChanged="radioPaymentType_SelectedIndexChanged">
                    </asp:RadioButtonList>
                    <asp:Label ID="lblGetPaymentTypesStat" runat="server" Text="" Visible="false"></asp:Label>

                    <div id="divInsurer" runat="server" visible="false">
                        <asp:Label ID="lblInsurerName" runat="server" Text="Insurer Name" CssClass="label"></asp:Label>
                        <asp:RadioButtonList ID="radioInsurerName" runat="server"
                            AutoPostBack="true"
                            SelectMethod="getInsurers"
                            RepeatLayout="UnorderedList"
                            OnSelectedIndexChanged="radioInsurerName_SelectedIndexChanged">
                        </asp:RadioButtonList>
                        <asp:TextBox ID="InsurerNameOther" runat="server" AutoPostBack="true" OnTextChanged="InsurerNameOther_TextChanged" Visible="false" CssClass="otherOption"></asp:TextBox>
                        <asp:Label ID="lblGetInsurersStat" runat="server" Text="" Visible="false"></asp:Label>
                    </div>
                    <!--end divInsurer -->
                        <div id="divPackage" runat="server" visible="false">
                            <asp:Label ID="lblPackageTaken" runat="server" Text="Package Taken" CssClass="label"></asp:Label>
                            <asp:RadioButtonList ID="radioPackageTaken" runat="server"
                                AutoPostBack="true"
                                RepeatLayout="UnorderedList"
                                SelectMethod="getInsurancePackages"
                                OnSelectedIndexChanged="radioPackageTaken_SelectedIndexChanged">
                            </asp:RadioButtonList>
                            <asp:Label ID="lblGetInsurancePackageStat" runat="server" Text="" Visible="false"></asp:Label>
                        </div>
                    <!--end divPackage -->
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- end updPanelPayment -->
            <asp:UpdatePanel ID="updPanelPricePaid" runat="server" Visible="false">
                <ContentTemplate>
                    <label>Price Paid</label>
                    <asp:TextBox ID="txtPricePaid" runat="server" AutoPostBack="true"
                        OnTextChanged="txtPricePaid_TextChanged"></asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- end updPanelPricePaid -->

            <asp:UpdatePanel ID="updPanelStockProvided" runat="server" Visible="false">
                <ContentTemplate>
                    <div id="divStockProvided" runat="server">
                        <div id="stock" class="segment_header">
                            <h1>Stock Provided To Patient</h1>
                        </div>
                        <div id="device" class="section">
                            <asp:Table ID="table3" runat="server" CellPadding="10" GridLines="Both" CssClass="stockTable" Caption="Device">
                                <asp:TableHeaderRow CssClass="headerRow">
                                    <asp:TableHeaderCell>
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                            Size
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                            Type
                                    </asp:TableHeaderCell>
                                </asp:TableHeaderRow>
                                <asp:TableRow>
                                    <asp:TableHeaderCell CssClass="headerCell">
                                        <asp:Label ID="Label2" runat="server" Text="Choose Size and Type"></asp:Label>
                                    </asp:TableHeaderCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstDevSize" runat="server" AutoPostBack="true"
                                            SelectMethod="getDevSizes"
                                            OnSelectedIndexChanged="dlstDevice_SelectedIndeChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstDevType" runat="server" AutoPostBack="true"
                                            SelectMethod="getDevTypes"
                                            OnSelectedIndexChanged="dlstDevice_SelectedIndeChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Label ID="lblGetDevicesStat" runat="server" Text="" Visible="false" CssClass="midctl"></asp:Label>
                        </div>

                        <div id="divC85" runat="server" class="section">
                            <asp:Table ID="tableC85_left" runat="server" CellPadding="10" GridLines="Both" CssClass="stockTable" Caption="Convex Cap 85mm">
                                <asp:TableHeaderRow CssClass="headerRow">
                                    <asp:TableHeaderCell>
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                    Profile
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                    Amount
                                    </asp:TableHeaderCell>
                                </asp:TableHeaderRow>
                                <asp:TableRow>
                                    <asp:TableHeaderCell CssClass="headerCell">
                                        <asp:Label ID="lblC85Spec3Val1" runat="server" Text="Pods"></asp:Label>
                                    </asp:TableHeaderCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstC85Profile4" runat="server" AutoPostBack="true" SelectMethod="getSpec4" OnSelectedIndexChanged="dlstC85Profile4_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstC85Amount4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dlstC85Amount4_SelectedIndexChanged">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>

                            <asp:Table ID="tableC85_right" runat="server" CellPadding="10" GridLines="Both" CssClass="stockTable" Caption="Convex Cap 85mm">
                                <asp:TableHeaderRow CssClass="headerRow">
                                    <asp:TableHeaderCell>
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                    Profile
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                    Amount
                                    </asp:TableHeaderCell>
                                </asp:TableHeaderRow>
                                <asp:TableRow>
                                    <asp:TableHeaderCell CssClass="headerCell">
                                        <asp:Label ID="lblC85spec3Val2" runat="server" Text="Pods"></asp:Label>
                                    </asp:TableHeaderCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstC85Profile5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dlstC85Profile5_SelectedIndexChanged" SelectMethod="getSpec4">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstC85Amount5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dlstC85Amount5_SelectedIndexChanged">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Label ID="lblGetC85ProfileStat" runat="server"></asp:Label>
                        </div>
                        <!--end divC85 -->

                        <div id="divC95" runat="server" class="section">
                            <asp:Table ID="tableC95_left" runat="server" CellPadding="10" GridLines="Both" CssClass="stockTable" Caption="Convex Cap 95mm">
                                <asp:TableHeaderRow CssClass="headerRow">
                                    <asp:TableHeaderCell>
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                    Profile
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                    Amount
                                    </asp:TableHeaderCell>
                                </asp:TableHeaderRow>
                                <asp:TableRow>
                                    <asp:TableHeaderCell CssClass="headerCell">
                                        <asp:Label ID="lblC95Spec3Val1" runat="server" Text="Pods"></asp:Label>
                                    </asp:TableHeaderCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstC95Profile4" runat="server" AutoPostBack="true" SelectMethod="getSpec4" OnSelectedIndexChanged="dlstC95Profile4_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstC95Amount4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dlstC95Amount4_SelectedIndexChanged">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>

                            <asp:Table ID="tableC95_right" runat="server" CellPadding="10" GridLines="Both" CssClass="stockTable" Caption="Convex Cap 95mm">
                                <asp:TableHeaderRow CssClass="headerRow">
                                    <asp:TableHeaderCell>
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                    Profile
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell>
                                    Amount
                                    </asp:TableHeaderCell>
                                </asp:TableHeaderRow>
                                <asp:TableRow>
                                    <asp:TableHeaderCell CssClass="headerCell">
                                        <asp:Label ID="lblC95spec3Val2" runat="server" Text="Pods"></asp:Label>
                                    </asp:TableHeaderCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstC95Profile5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dlstC95Profile5_SelectedIndexChanged" SelectMethod="getSpec4">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="dlstC95Amount5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dlstC95Amount4_SelectedIndexChanged">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Label ID="lblGetC95ProfileStat" runat="server"></asp:Label>
                        </div>
                        <!--end divC95 -->
                        <div id="divSpacers" runat="server" class="section">
                            <div id="divS85" runat="server" class="stockRadio">
                                <asp:Label ID="lblS85Spec3Val1" runat="server" Text="85mm Soft Spacer" CssClass="label"></asp:Label>
                                <asp:RadioButtonList ID="RadioSpacerS85" runat="server"
                                    AutoPostBack="true"
                                    TextAlign="left"
                                    SelectMethod="getSpacerList"
                                    RepeatLayout="UnorderedList"
                                    OnSelectedIndexChanged="radioSpacerS85_SelectedIndexChanged">
                                </asp:RadioButtonList>
                            </div>
                            <!--end divS85 -->

                            <div id="divH85" runat="server" class="stockRadio">
                                <asp:Label ID="Label1" runat="server" Text="85mm Hard Spacer" CssClass="label"></asp:Label>
                                <asp:RadioButtonList ID="RadioSpacerH85" runat="server"
                                    AutoPostBack="true"
                                    TextAlign="left"
                                    SelectMethod="getSpacerList"
                                    RepeatLayout="UnorderedList"
                                    OnSelectedIndexChanged="radioSpacerH85_SelectedIndexChanged">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblGetS85ProfileStat" runat="server"></asp:Label>
                            </div>
                            <!--end divH85 -->
                            <div id="divS95" runat="server" class="stockRadio">
                                <asp:Label ID="lblS95Spec3Val1" runat="server" Text="95mm Soft Spacer" CssClass="label"></asp:Label>
                                <asp:RadioButtonList ID="RadioSpacerS95" runat="server"
                                    AutoPostBack="true"
                                    TextAlign="left"
                                    SelectMethod="getSpacerList"
                                    RepeatLayout="UnorderedList"
                                    OnSelectedIndexChanged="radioSpacerS95_SelectedIndexChanged">
                                </asp:RadioButtonList>
                            </div>
                            <!--end divS95 -->

                            <div id="divH95" runat="server" class="stockRadio">
                                <asp:Label ID="Label3" runat="server" Text="95mm Hard Spacer" CssClass="label"></asp:Label>
                                <asp:RadioButtonList ID="RadioSpacerH95" runat="server"
                                    AutoPostBack="true"
                                    TextAlign="left"
                                    SelectMethod="getSpacerList"
                                    RepeatLayout="UnorderedList"
                                    OnSelectedIndexChanged="radioSpacerH95_SelectedIndexChanged">
                                </asp:RadioButtonList>
                                <asp:Label ID="lblGetS95ProfileStat" runat="server"></asp:Label>
                            </div>
                            <!--end divH95 -->
                        </div>
                        <!--end divSpacers -->

                        <!--- Weights section-->
                        <div id="divWeights" runat="server" class="section">
                            <div id="weights85" class="stockRadio">
                                <asp:Label ID="lblWeights85" runat="server" Text="85mm Weights" CssClass="label"></asp:Label>
                                <asp:RadioButtonList ID="Weight85" runat="server"
                                    AutoPostBack="true"
                                    TextAlign="left"
                                    SelectMethod="getWeights"
                                    RepeatLayout="UnorderedList"
                                    OnSelectedIndexChanged="radioWeight85_SelectedIndexChanged">
                                </asp:RadioButtonList>
                            </div>
                            <div id="weights95" class="stockRadio">
                                <asp:Label ID="lblWeighs95" runat="server" Text="95mm Weights" CssClass="label"></asp:Label>
                                <asp:RadioButtonList ID="Weight95" runat="server"
                                    AutoPostBack="true"
                                    TextAlign="left"
                                    SelectMethod="getWeights"
                                    RepeatLayout="UnorderedList"
                                    OnSelectedIndexChanged="radioWeight95_SelectedIndexChanged">
                                </asp:RadioButtonList>
                            </div>
                            <asp:Label ID="lblGetSpec7ValuesStat" runat="server" Style="margin-left: 100px"></asp:Label>
                        </div>

                        <div id="bagGiven">
                            <label class="no-colon">AposTherapy Bag Given?</label>
                            <asp:RadioButtonList ID="radioBagGiven" runat="server"
                                AutoPostBack="true"
                                TextAlign="left"
                                SelectMethod="getBagOptions"
                                RepeatLayout="UnorderedList"
                                OnSelectedIndexChanged="radioBag_SelectedIndexChanged">
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <!--end divStockProvided -->
                </ContentTemplate>
            </asp:UpdatePanel>
            <!--end updPanelStockProvided -->

            <div id="divCommnets" runat="server" visible="false">
                <label>AdditionalComments</label>
                <asp:TextBox ID="txtComments" runat="server" OnTextChanged="txtComments_TextChanged"
                    TextMode="MultiLine" Rows="5">
                </asp:TextBox>
            </div>
            <!-- end divComments -->

            <asp:UpdatePanel ID="updPanelSubmit" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="submit_button" OnClick="btnSubmit_click" />
                    <asp:Label ID="lblSubmitStat" runat="server" Text=""></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- end updPanelSubmit -->
        </asp:Panel>
        <!-- end PanelAppointment-->
    </form>
</body>
</html>
