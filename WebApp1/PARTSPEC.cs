//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApp1
{
    using System;
    using System.Collections.Generic;
    
    public partial class PARTSPEC
    {
        public long PART { get; set; }
        public string SPEC1 { get; set; }
        public string SPEC2 { get; set; }
        public string SPEC3 { get; set; }
        public string SPEC4 { get; set; }
        public string SPEC5 { get; set; }
        public string SPEC6 { get; set; }
        public string SPEC7 { get; set; }
        public string SPEC8 { get; set; }
        public string SPEC9 { get; set; }
        public string SPEC10 { get; set; }
        public string WEBLEVEL { get; set; }
        public string SPEC11 { get; set; }
        public string SPEC12 { get; set; }
        public string SPEC13 { get; set; }
        public string SPEC14 { get; set; }
        public string SPEC15 { get; set; }
        public string SPEC16 { get; set; }
        public string SPEC17 { get; set; }
        public string SPEC18 { get; set; }
        public string SPEC19 { get; set; }
        public string SPEC20 { get; set; }
        public string SPECDES1 { get; set; }
        public string SPECDES2 { get; set; }
        public string SPECDES3 { get; set; }
        public string SPECDES4 { get; set; }
        public string SPECDES5 { get; set; }
        public string SPECDES6 { get; set; }
        public string SPECDES7 { get; set; }
        public string SPECDES8 { get; set; }
        public string SPECDES9 { get; set; }
        public string SPECDES10 { get; set; }
        public string SPECDES11 { get; set; }
        public string SPECDES12 { get; set; }
        public string SPECDES13 { get; set; }
        public string SPECDES14 { get; set; }
        public string SPECDES15 { get; set; }
        public string SPECDES16 { get; set; }
        public string SPECDES17 { get; set; }
        public string SPECDES18 { get; set; }
        public string SPECDES19 { get; set; }
        public string SPECDES20 { get; set; }
        public string SPECEDES1 { get; set; }
        public string SPECEDES2 { get; set; }
        public string SPECEDES3 { get; set; }
        public string SPECEDES4 { get; set; }
        public string SPECEDES5 { get; set; }
        public string SPECEDES6 { get; set; }
        public string SPECEDES7 { get; set; }
        public string SPECEDES8 { get; set; }
        public string SPECEDES9 { get; set; }
        public string SPECEDES10 { get; set; }
        public string SPECEDES11 { get; set; }
        public string SPECEDES12 { get; set; }
        public string SPECEDES13 { get; set; }
        public string SPECEDES14 { get; set; }
        public string SPECEDES15 { get; set; }
        public string SPECEDES16 { get; set; }
        public string SPECEDES17 { get; set; }
        public string SPECEDES18 { get; set; }
        public string SPECEDES19 { get; set; }
        public string SPECEDES20 { get; set; }
    }
}
