echo Begin of Upgrade 7.0.12
echo Title: יפרתסופא
DBI <<\EOF
FOR TABLE CUSTSPECVALUES INSERT ERPG_SELF_PAYMENT
(CHAR,1,'Self Payment');
EOF
DBI <<\EOF
FOR TABLE CUSTTOPICS INSERT FOLLOWUPNUMBERFLAG
(CHAR,1,'Show Follow Up Numbe');
EOF
DBI <<\EOF
FOR TABLE CUSTTOPICS INSERT ERPG_PAYMANTTYPEFLAG
(CHAR,1,'Show Payment Type');
EOF
DBI <<\EOF
FOR TABLE CUSTTOPICS INSERT ERPG_PRICEPAID
(CHAR,1,'Show Price Paid');
EOF
DBI <<\EOF
FOR TABLE PHONEBOOK INSERT ERPG_USERNAME
(CHAR,20,'שמתשמ םש');
EOF
DBI <<\EOF
FOR TABLE PHONEBOOK INSERT ERPG_PASSWORD
(CHAR,20,'אמסיס');
EOF
DBI <<\EOF
FOR TABLE PHONEBOOK
INSERT NONUNIQUE(ERPG_USERNAME)
WITH PRIORITY 12 ;
EOF
DBI <<\EOF
FOR TABLE CUSTSPECVALUES INSERT ERPG_S1_NOINSURANCE
(CHAR,1,'חוטיב אלל');
EOF
DBI <<\EOF
FOR TABLE CUSTNOTETYPES INSERT ERPG_OPENSHFLAG
(CHAR,1,'חולשמ תדועת תחיתפ');
EOF
DBI <<\EOF
FOR TABLE CUSTSPECVALUES
COLUMN ERPG_S1_NOINSURANCE
CHANGE NAME TO ERPG_INSURED ;
EOF
DBI <<\EOF
FOR TABLE CUSTNOTETYPES
COLUMN ERPG_OPENSHFLAG
CHANGE NAME TO ERPG_OPEN_SH ;
EOF
DBI <<\EOF
FOR TABLE CUSTTOPICS
COLUMN ERPG_PRICEPAIDFLAG
CHANGE TITLE TO 'Show Price Paid?' ;
EOF
DBI <<\EOF
FOR TABLE CUSTTOPICS
COLUMN ERPG_PAYMENTTYPEFLAG
CHANGE TITLE TO 'Show Payment Type?' ;
EOF
DBI <<\EOF
FOR TABLE CUSTTOPICS
COLUMN FOLLOWUPNUMBERFLAG
CHANGE NAME TO ERPG_FOLLOWUPNUMFLAG ;
EOF
DBI <<\EOF
FOR TABLE CUSTTOPICS
COLUMN ERPG_PRICEPAID
CHANGE NAME TO ERPG_PRICEPAIDFLAG ;
EOF
DBI <<\EOF
FOR TABLE CUSTTOPICS
COLUMN ERPG_FOLLOWUPNUMFLAG
CHANGE TITLE TO 'Show Followup Num ?' ;
EOF
DBI <<\EOF
FOR TABLE CUSTTOPICS
COLUMN ERPG_PAYMANTTYPEFLAG
CHANGE NAME TO ERPG_PAYMENTTYPEFLAG ;
EOF
DBI <<\EOF
FOR TABLE CUSTSPECVALUES
COLUMN ERPG_SELF_PAYMENT
CHANGE TITLE TO 'Self Payment?' ;
EOF
DBI <<\EOF
FOR TABLE CUSTSPECVALUES
COLUMN ERPG_INSURED
CHANGE TITLE TO 'Insured ?' ;
EOF
BRING << \EOF
18 14 CUSTSPECVALUES0 
56 14 CUSTSPECVALUES28 חוקלל םירטמרפל םיירשפא םיכרע0 
19 14 CUSTSPECVALUES6 17 ERPG_SELF_PAYMENT4 CHAR1 1 13 Self Payment?
19 14 CUSTSPECVALUES5 12 ERPG_INSURED4 CHAR1 1 9 Insured ?
19 14 CUSTSPECVALUES1 7 SPECNUM3 INT2 4 10 רטמרפ רפסמ
33 14 CUSTSPECVALUES7 SPECNUM0 48 
19 14 CUSTSPECVALUES2 9 SPECVALUE5 RCHAR32 32 3 ךרע
19 14 CUSTSPECVALUES3 4 SORT3 INT3 4 4 ןוימ
33 14 CUSTSPECVALUES4 SORT0 48 
19 14 CUSTSPECVALUES4 8 PIKORDER3 INT6 8 9 טוקיל רדס
33 14 CUSTSPECVALUES8 PIKORDER0 48 
20 14 CUSTSPECVALUES85 1 
21 14 CUSTSPECVALUES1 7 SPECNUM1 
21 14 CUSTSPECVALUES1 9 SPECVALUE2 
17 19 ERPG_CUSTSPECVALUES26 חוקל ירטמרפל םיירשפא םיכרע14 CUSTSPECVALUES70 0 0 81 78 4 ERPG0 0 
57 10 יטרפ חותיפ1 
58 1 19 ERPG_CUSTSPECVALUES70 
13 19 ERPG_CUSTSPECVALUES70 14 CUSTSPECVALUES12 ERPG_INSURED0 12 ERPG_INSURED0 0 1 0 50 0 0 0 0 0 0 0 1 
13 19 ERPG_CUSTSPECVALUES70 14 CUSTSPECVALUES17 ERPG_SELF_PAYMENT0 17 ERPG_SELF_PAYMENT0 0 1 0 60 0 0 0 0 0 0 0 1 
13 19 ERPG_CUSTSPECVALUES70 14 CUSTSPECVALUES8 PIKORDER0 8 PIKORDER0 0 6 48 40 0 0 0 0 0 0 0 1 
13 19 ERPG_CUSTSPECVALUES70 14 CUSTSPECVALUES4 SORT0 4 SORT0 0 3 48 30 0 0 0 0 0 0 0 1 
13 19 ERPG_CUSTSPECVALUES70 14 CUSTSPECVALUES7 SPECNUM0 7 SPECNUM0 0 2 48 10 1 0 0 0 0 0 0 1 
13 19 ERPG_CUSTSPECVALUES70 14 CUSTSPECVALUES9 SPECVALUE0 9 SPECVALUE0 0 32 0 20 2 0 0 0 0 0 0 1 
18 10 SPECVALUES1 
56 10 SPECVALUES28 רצומל םירטמרפל םיירשפא םיכרע0 
19 10 SPECVALUES7 11 ESPECVALDES4 CHAR48 48 14 יזעול ךרע רואת
19 10 SPECVALUES1 7 SPECNUM3 INT2 4 10 רטמרפ רפסמ
33 10 SPECVALUES7 SPECNUM0 48 
19 10 SPECVALUES2 9 SPECVALUE5 RCHAR48 48 3 ךרע
19 10 SPECVALUES3 10 SPECVALDES5 RCHAR48 48 8 ךרע רואת
19 10 SPECVALUES4 12 PARTNAMECODE4 CHAR10 10 15 ט"קמ לוליחל דוק
19 10 SPECVALUES5 4 SORT3 INT3 4 8 הגצה רדס
33 10 SPECVALUES4 SORT0 48 
19 10 SPECVALUES6 6 FAMILY3 INT13 8 10 (ID) החפשמ
33 10 SPECVALUES6 FAMILY0 48 
20 10 SPECVALUES85 1 
21 10 SPECVALUES1 7 SPECNUM1 
21 10 SPECVALUES1 9 SPECVALUE2 
20 10 SPECVALUES78 2 
21 10 SPECVALUES2 7 SPECNUM1 
17 15 ERPG_SPECVALUES26 רצומ ירטמרפל םיירשפא םיכרע10 SPECVALUES70 0 0 81 78 4 ERPG0 0 
57 10 יטרפ חותיפ1 
58 1 15 ERPG_SPECVALUES70 
13 15 ERPG_SPECVALUES70 10 SPECVALUES11 ESPECVALDES0 11 ESPECVALDES0 0 48 0 70 0 0 0 0 0 0 0 1 
13 15 ERPG_SPECVALUES70 10 SPECVALUES6 FAMILY0 6 FAMILY0 0 13 48 60 0 0 0 0 0 0 0 1 
13 15 ERPG_SPECVALUES70 10 SPECVALUES12 PARTNAMECODE0 12 PARTNAMECODE0 0 10 0 40 0 0 0 0 0 0 0 1 
13 15 ERPG_SPECVALUES70 10 SPECVALUES4 SORT0 4 SORT0 0 3 48 50 0 0 0 0 0 0 0 1 
13 15 ERPG_SPECVALUES70 10 SPECVALUES7 SPECNUM0 7 SPECNUM0 0 2 48 10 0 0 0 0 0 0 0 1 
13 15 ERPG_SPECVALUES70 10 SPECVALUES10 SPECVALDES0 10 SPECVALDES0 0 48 0 30 0 0 0 0 0 0 0 1 
13 15 ERPG_SPECVALUES70 10 SPECVALUES9 SPECVALUE0 9 SPECVALUE0 0 48 0 20 0 0 0 0 0 0 0 1 
1017 15 ERPG_SPECVALUES26 רצומ ירטמרפל םיירשפא םיכרע10 SPECVALUES70 0 0 81 78 4 ERPG0 0 
57 10 יטרפ חותיפ1 
58 1 15 ERPG_SPECVALUES70 
18 10 CUSTTOPICS1 
56 10 CUSTTOPICS11 תומישמ ידוק0 
19 10 CUSTTOPICS14 7 NOVISIT4 CHAR1 1 9 ?רוקיב יא
19 10 CUSTTOPICS15 6 NOSALE4 CHAR1 1 9 ?הריכמ יא
19 10 CUSTTOPICS1 5 TOPIC3 INT13 4 10 (ID) המישמ
33 10 CUSTTOPICS5 TOPIC0 48 
19 10 CUSTTOPICS2 9 TOPICCODE4 CHAR3 3 9 המישמ דוק
19 10 CUSTTOPICS3 8 TOPICDES5 RCHAR22 22 10 המישמ רואת
19 10 CUSTTOPICS16 20 ERPG_FOLLOWUPNUMFLAG4 CHAR1 1 19 Show Followup Num ?
19 10 CUSTTOPICS17 20 ERPG_PAYMENTTYPEFLAG4 CHAR1 1 18 Show Payment Type?
19 10 CUSTTOPICS18 18 ERPG_PRICEPAIDFLAG4 CHAR1 1 16 Show Price Paid?
19 10 CUSTTOPICS4 5 COLOR3 INT13 4 8 (ID) עבצ
33 10 CUSTTOPICS5 COLOR0 48 
19 10 CUSTTOPICS5 8 MAILFLAG4 CHAR1 1 11 ?ראוד חולשל
19 10 CUSTTOPICS6 9 ETOPICDES4 CHAR22 22 18 תילגנאב המישמ רואת
19 10 CUSTTOPICS7 9 MILESTONE4 CHAR1 1 13 םזימב ךרד ןבא
19 10 CUSTTOPICS8 5 ENDED4 CHAR1 1 8 שומשב אל
19 10 CUSTTOPICS9 8 CCSTATUS3 INT13 4 19 הריכמ תונמדזה סוטטס
33 10 CUSTTOPICS8 CCSTATUS0 48 
19 10 CUSTTOPICS10 7 OUTLOOK4 CHAR1 1 15 ?Outlook-ל קשממ
19 10 CUSTTOPICS11 7 BILLING4 CHAR1 1 5 ?היבג
19 10 CUSTTOPICS12 7 MEETING4 CHAR1 1 7 ? השיגפ
19 10 CUSTTOPICS13 6 QUESTF3 INT13 8 16 (ID) ןולאש טמרופ
33 10 CUSTTOPICS6 QUESTF0 48 
20 10 CUSTTOPICS65 1 
21 10 CUSTTOPICS1 5 TOPIC1 
20 10 CUSTTOPICS85 2 
21 10 CUSTTOPICS2 9 TOPICCODE1 
20 10 CUSTTOPICS78 3 
21 10 CUSTTOPICS3 8 TOPICDES1 
20 10 CUSTTOPICS78 4 
21 10 CUSTTOPICS4 9 ETOPICDES1 
1013 10 CUSTTOPICS70 10 CUSTTOPICS20 ERPG_FOLLOWUPNUMFLAG0 20 ERPG_FOLLOWUPNUMFLAG0 0 1 0 120 10 0 0 0 0 0 0 1 
1013 10 CUSTTOPICS70 10 CUSTTOPICS20 ERPG_PAYMENTTYPEFLAG0 20 ERPG_PAYMENTTYPEFLAG0 0 1 0 130 10 0 0 0 0 0 0 1 
1013 10 CUSTTOPICS70 10 CUSTTOPICS18 ERPG_PRICEPAIDFLAG0 18 ERPG_PRICEPAIDFLAG0 0 1 0 140 10 0 0 0 0 0 0 1 
205 13 CUSTNOTETYPES70 15 ERPG_OPENSHFLAG
18 13 CUSTNOTETYPES0 
56 13 CUSTNOTETYPES11 תומישמ יגוס0 
19 13 CUSTNOTETYPES6 12 ERPG_OPEN_SH4 CHAR1 1 17 חולשמ תדועת תחיתפ
19 13 CUSTNOTETYPES1 12 CUSTNOTETYPE3 INT13 4 14 (ID) המישמ גוס
33 13 CUSTNOTETYPES12 CUSTNOTETYPE0 48 
19 13 CUSTNOTETYPES2 15 CUSTNOTETYPEDES5 RCHAR8 8 9 המישמ גוס
19 13 CUSTNOTETYPES3 7 OUTLOOK4 CHAR1 1 15 ?Outlook-ל קשממ
19 13 CUSTNOTETYPES4 10 MARKETFLAG4 CHAR1 1 5 ?קווש
19 13 CUSTNOTETYPES5 11 SENDMESSAGE4 CHAR1 1 10 תועדוה חלש
20 13 CUSTNOTETYPES65 1 
21 13 CUSTNOTETYPES1 12 CUSTNOTETYPE1 
20 13 CUSTNOTETYPES85 2 
21 13 CUSTNOTETYPES2 15 CUSTNOTETYPEDES1 
1013 13 CUSTNOTETYPES70 13 CUSTNOTETYPES12 ERPG_OPEN_SH0 12 ERPG_OPEN_SH0 0 1 0 500 10 0 0 0 0 0 0 1 
18 18 ERPG_SITEINTERFACE0 
56 18 ERPG_SITEINTERFACE29 Incoming interfaces from site0 
19 18 ERPG_SITEINTERFACE16 8 CUSTNOTE3 INT13 8 11 task number
33 18 ERPG_SITEINTERFACE8 CUSTNOTE0 48 
19 18 ERPG_SITEINTERFACE17 8 CUSTNAME4 CHAR20 20 16 CUSTOMERS NUMBER
19 18 ERPG_SITEINTERFACE18 7 CUSTDES4 CHAR20 20 20 CUSTOMER DESCRIPTION
19 18 ERPG_SITEINTERFACE1 4 LINE3 INT13 8 4 LINE
33 18 ERPG_SITEINTERFACE4 LINE0 48 
19 18 ERPG_SITEINTERFACE2 10 UPLOADDATE4 DATE14 8 11 Upload Date
19 18 ERPG_SITEINTERFACE3 6 LOADED4 CHAR1 1 7 Loaded?
19 18 ERPG_SITEINTERFACE4 6 ERRORS4 CHAR1 1 7 Errors?
19 18 ERPG_SITEINTERFACE5 9 ERRORDESC4 CHAR120 120 17 Error Description
19 18 ERPG_SITEINTERFACE6 15 APPOINTMENTDATE4 DATE8 8 16 Appointment Date
19 18 ERPG_SITEINTERFACE7 15 APPOINTMENTTYPE4 CHAR6 6 16 Appointment Type
19 18 ERPG_SITEINTERFACE8 9 PATIENTID4 CHAR24 24 10 Patient ID
19 18 ERPG_SITEINTERFACE9 10 PATIENTDOB4 DATE8 8 14 Patient D.O.B.
19 18 ERPG_SITEINTERFACE10 13 PATIENTGENDER4 CHAR6 6 14 Patient Gender
19 18 ERPG_SITEINTERFACE11 10 PHYSIONAME4 CHAR40 40 20 Physiotherapist Name
19 18 ERPG_SITEINTERFACE12 18 APPOINTMENTOUTCOME4 CHAR3 3 19 Appointment Outcome
19 18 ERPG_SITEINTERFACE13 16 ATTENDEDFOLLOWUP4 CHAR3 3 19 Attended Follow Up?
19 18 ERPG_SITEINTERFACE14 18 ADDITIONALCOMMENTS4 CHAR120 120 19 Additional Comments
19 18 ERPG_SITEINTERFACE15 3 DOC3 INT13 8 15 Document Number
33 18 ERPG_SITEINTERFACE3 DOC0 48 
20 18 ERPG_SITEINTERFACE85 1 
21 18 ERPG_SITEINTERFACE1 4 LINE1 
1013 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE4 LINE0 4 LINE0 0 13 48 10 0 0 0 0 0 0 0 1 
18 13 ERPG_CUSTSHIP0 
56 13 ERPG_CUSTSHIP18 Customer Shipments0 
19 13 ERPG_CUSTSHIP3 5 WARHS3 INT13 8 17 Sending Warehouse
33 13 ERPG_CUSTSHIP5 WARHS0 48 
19 13 ERPG_CUSTSHIP4 7 PARTNUM4 CHAR24 24 11 Part Number
19 13 ERPG_CUSTSHIP5 6 AMOUNT4 REAL12 8 6 Amount
33 13 ERPG_CUSTSHIP6 AMOUNT3 51 
19 13 ERPG_CUSTSHIP6 6 LOADED4 CHAR1 1 7 Loaded?
19 13 ERPG_CUSTSHIP7 6 ERRORS4 CHAR1 1 7 Errors?
19 13 ERPG_CUSTSHIP8 8 ERRORDES4 CHAR120 120 17 Error Description
19 13 ERPG_CUSTSHIP9 8 CUSTNAME4 CHAR20 20 16 CUSTOMERS NUMBER
19 13 ERPG_CUSTSHIP1 10 HEADERLINE3 INT13 8 11 Header Line
33 13 ERPG_CUSTSHIP10 HEADERLINE0 48 
19 13 ERPG_CUSTSHIP2 4 LINE3 INT13 8 4 Line
33 13 ERPG_CUSTSHIP4 LINE0 48 
20 13 ERPG_CUSTSHIP85 1 
21 13 ERPG_CUSTSHIP1 10 HEADERLINE1 
21 13 ERPG_CUSTSHIP1 4 LINE2 
1013 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP4 LINE0 4 LINE0 0 13 48 20 0 0 0 0 0 0 0 1 
205 15 CUSTSPEC1VALUES70 19 ERPG_S1_NOINSURANCE
1013 15 CUSTSPEC1VALUES70 14 CUSTSPECVALUES12 ERPG_INSURED0 12 ERPG_INSURED0 0 1 0 22 10 0 0 0 0 0 0 1 
1013 15 CUSTSPEC1VALUES70 14 CUSTSPECVALUES17 ERPG_SELF_PAYMENT0 17 ERPG_SELF_PAYMEMT0 0 1 0 23 10 0 0 0 0 0 0 1 
205 13 CUSTPERSONNEL70 13 ERPG_PASSWORD
205 13 CUSTPERSONNEL70 13 ERPG_USERNAME
18 9 PHONEBOOK1 
56 9 PHONEBOOK8 רשק ישנא0 
19 9 PHONEBOOK46 9 TERRITORY3 INT13 8 13 (ID) הירוטירט
33 9 PHONEBOOK9 TERRITORY0 48 
19 9 PHONEBOOK1 4 NAME5 RCHAR37 37 2 םש
19 9 PHONEBOOK2 4 FIRM5 RCHAR48 48 4 הרבח
19 9 PHONEBOOK3 8 PHONENUM4 CHAR20 20 5 ןופלט
19 9 PHONEBOOK4 5 PHONE3 INT13 4 12 (ID) רשק שיא
33 9 PHONEBOOK5 PHONE0 48 
19 9 PHONEBOOK5 9 CPROFFLAG4 CHAR1 1 17 (C) העצהל רשק שיא
19 9 PHONEBOOK6 4 CUST3 INT13 4 15 (ID) חוקלל רושק
33 9 PHONEBOOK4 CUST0 48 
19 9 PHONEBOOK7 3 SUP3 INT13 4 9 קפסל רושק
33 9 PHONEBOOK3 SUP0 48 
19 9 PHONEBOOK8 8 POSITION3 INT13 4 14 (ID) דיקפת דוק
33 9 PHONEBOOK8 POSITION0 48 
19 9 PHONEBOOK9 9 HOMEPHONE4 CHAR20 20 10 תיבב ןופלט
19 9 PHONEBOOK10 5 EMAIL4 CHAR48 48 13 ינורטקלא ראוד
19 9 PHONEBOOK11 9 CELLPHONE4 CHAR20 20 10 דיינ ןופלט
19 9 PHONEBOOK12 3 FAX4 CHAR20 20 3 סקפ
19 9 PHONEBOOK47 13 ERPG_USERNAME4 CHAR20 20 8 שמתשמ םש
19 9 PHONEBOOK48 13 ERPG_PASSWORD4 CHAR20 20 5 אמסיס
19 9 PHONEBOOK13 7 ORDFLAG4 CHAR1 1 14 הנמזהל רשק שיא
19 9 PHONEBOOK14 7 DOCFLAG4 CHAR1 1 14 חולשמל רשק שיא
19 9 PHONEBOOK15 7 CIVFLAG4 CHAR1 1 16 תינובשחל רשק שיא
19 9 PHONEBOOK16 7 FNCFLAG4 CHAR1 1 14 ש"חנהל רשק שיא
19 9 PHONEBOOK17 10 PDPROFFLAG4 CHAR1 1 19 (קפס) העצהל רשק שיא
19 9 PHONEBOOK18 8 PORDFLAG4 CHAR1 1 20 (קפס) הנמזהל רשק שיא
19 9 PHONEBOOK19 7 PIVFLAG4 CHAR1 1 20 (ס) תינובשחל רשק שיא
19 9 PHONEBOOK20 8 LEADFLAG4 CHAR1 1 5 ? דיל
19 9 PHONEBOOK21 8 DEALFLAG4 CHAR1 1 13 קוושל רשק שיא
19 9 PHONEBOOK22 7 TIVFLAG4 CHAR1 1 13 לויגל רשק שיא
19 9 PHONEBOOK23 12 SERVCALLFLAG4 CHAR1 1 13 תורשל רשק שיא
19 9 PHONEBOOK24 8 INDEXNUM4 CHAR8 8 9 Index Num
19 9 PHONEBOOK25 11 OFFICEPHONE4 CHAR20 20 11 דרשמב ןופלט
19 9 PHONEBOOK26 11 POSITIONDES5 RCHAR32 32 11 דיקפת תרדגה
19 9 PHONEBOOK27 8 TITLEDES5 RCHAR5 5 4 ראות
19 9 PHONEBOOK28 8 LEADSTAT3 INT13 8 14 (ID )דיל סוטטס
33 9 PHONEBOOK8 LEADSTAT0 48 
19 9 PHONEBOOK29 9 LEADOWNER3 INT13 8 15 (ID) לופיטל דיל
33 9 PHONEBOOK9 LEADOWNER0 48 
19 9 PHONEBOOK30 10 PHONEBSTAT3 INT13 8 18 (ID) רשק שיא סוטטס
33 9 PHONEBOOK10 PHONEBSTAT0 48 
19 9 PHONEBOOK31 11 PHONEBOWNER3 INT13 8 19 (ID) לופיטל רשק שיא
33 9 PHONEBOOK11 PHONEBOWNER0 48 
19 9 PHONEBOOK32 5 AGENT3 INT13 8 9 (ID) ןכוס
33 9 PHONEBOOK5 AGENT0 48 
19 9 PHONEBOOK33 8 INACTIVE4 CHAR1 1 7 ליעפ אל
19 9 PHONEBOOK34 5 CTYPE3 INT13 8 13 (ID) חוקל גוס
33 9 PHONEBOOK5 CTYPE0 48 
19 9 PHONEBOOK35 13 CONSINGEEFLAG4 CHAR1 1 17 אוצי קיתל רשק שיא
19 9 PHONEBOOK36 10 NOTIFYFLAG4 CHAR1 1 18 אוציל ףסונ רשק שיא
19 9 PHONEBOOK37 10 IMPORTFLAG4 CHAR1 1 5 ?אובי
19 9 PHONEBOOK38 12 INACTIVELEAD4 CHAR1 1 11 ליעפ אל דיל
19 9 PHONEBOOK39 8 CAMPAIGN3 INT13 8 9 (ID) םזימ
33 9 PHONEBOOK8 CAMPAIGN0 48 
19 9 PHONEBOOK40 7 ENDCUST3 INT13 8 14 (ID) יפוס חוקל
33 9 PHONEBOOK7 ENDCUST0 48 
19 9 PHONEBOOK41 12 PHONEENDCUST3 INT13 8 18 יפוס חוקלל רשק שיא
33 9 PHONEBOOK12 PHONEENDCUST0 48 
19 9 PHONEBOOK42 10 SALECHANEL3 INT13 8 14 (ID)הריכמ ץורע
33 9 PHONEBOOK10 SALECHANEL0 48 
19 9 PHONEBOOK43 15 PHONESALECHANEL3 INT13 8 17 (ID)ץורעב רשק שיא
33 9 PHONEBOOK15 PHONESALECHANEL0 48 
19 9 PHONEBOOK44 10 INTEGRATOR3 INT13 8 14 (ID) רוטרגטניא
33 9 PHONEBOOK10 INTEGRATOR0 48 
19 9 PHONEBOOK45 15 PHONEINTEGRATOR3 INT13 8 18 רוטרגטניאל רשק שיא
33 9 PHONEBOOK15 PHONEINTEGRATOR0 48 
20 9 PHONEBOOK85 2 
21 9 PHONEBOOK2 4 NAME1 
21 9 PHONEBOOK2 4 FIRM2 
20 9 PHONEBOOK65 1 
21 9 PHONEBOOK1 5 PHONE1 
20 9 PHONEBOOK78 3 
21 9 PHONEBOOK3 4 CUST1 
20 9 PHONEBOOK78 4 
21 9 PHONEBOOK4 3 SUP1 
20 9 PHONEBOOK78 5 
21 9 PHONEBOOK5 4 FIRM1 
20 9 PHONEBOOK78 6 
21 9 PHONEBOOK6 8 POSITION1 
20 9 PHONEBOOK78 7 
21 9 PHONEBOOK7 5 EMAIL1 
20 9 PHONEBOOK78 8 
21 9 PHONEBOOK8 8 INDEXNUM1 
20 9 PHONEBOOK78 9 
21 9 PHONEBOOK9 8 LEADSTAT1 
20 9 PHONEBOOK78 10 
21 9 PHONEBOOK10 10 PHONEBSTAT1 
20 9 PHONEBOOK78 11 
21 9 PHONEBOOK11 8 CAMPAIGN1 
20 9 PHONEBOOK78 12 
21 9 PHONEBOOK12 13 ERPG_USERNAME1 
1013 9 PHONEBOOK70 9 PHONEBOOK13 ERPG_USERNAME0 13 ERPG_USERNAME0 0 20 0 230 0 0 0 0 0 0 0 1 
1013 9 PHONEBOOK70 9 PHONEBOOK13 ERPG_PASSWORD0 13 ERPG_PASSWORD0 0 20 0 235 0 0 0 0 0 0 0 1 
24 10 11 CHECK-FIELD67 
1008 9 PHONEBOOK70 13 ERPG_USERNAME11 CHECK-FIELD67 
1014 9 PHONEBOOK70 13 ERPG_USERNAME11 CHECK-FIELD67 66 ERRMSG 501 WHERE EXISTS (SELECT ERPG_USERNAME FROM PHONEBOOK WHERE1 1 
1016 9 PHONEBOOK70 27 תכרעמב םייק רבכ הז שמתשמ םש501 
1014 9 PHONEBOOK70 13 ERPG_USERNAME11 CHECK-FIELD67 62 :$.@ = ERPG_USERNAME AND :$.PHONE <> PHONE AND :$.PHONE <> 0);2 2 
24 1 10 PRE-INSERT70 
1011 15 CUSTSPEC1VALUES70 10 PRE-INSERT70 
22 15 CUSTSPEC1VALUES70 10 PRE-INSERT70 34 #INCLUDE CUSTSPEC1VALUES/ERPG-BUF11 1 
24 11 10 POST-FIELD67 
1008 9 PHONEBOOK70 13 ERPG_USERNAME10 POST-FIELD67 
1014 9 PHONEBOOK70 13 ERPG_USERNAME10 POST-FIELD67 63 SELECT PHONE INTO :$.PHONE FROM PHONEBOOK WHERE ERPG_USERNAME =1 1 
1014 9 PHONEBOOK70 13 ERPG_USERNAME10 POST-FIELD67 5 :$.@;2 2 
24 5 10 PRE-UPDATE70 
1011 15 CUSTSPEC1VALUES70 10 PRE-UPDATE70 
22 15 CUSTSPEC1VALUES70 10 PRE-UPDATE70 34 #INCLUDE CUSTSPEC1VALUES/ERPG-BUF11 1 
24 9067 9 ERPG-BUF170 
1011 15 CUSTSPEC1VALUES70 9 ERPG-BUF170 
22 15 CUSTSPEC1VALUES70 9 ERPG-BUF170 65 ERRMSG 501 WHERE :$.ERPG_INSURED = 'Y' AND :$.ERPG_SELF_PAYMEMT =1 1 
1016 15 CUSTSPEC1VALUES70 55 It's not valid to specify both INSURED and SELF PAYMENT501 
22 15 CUSTSPEC1VALUES70 9 ERPG-BUF170 4 'Y';2 2 
1016 15 CUSTSPEC1VALUES70 55 It's not valid to specify both INSURED and SELF PAYMENT501 
1016 9 PHONEBOOK70 27 תכרעמב םייק רבכ הז שמתשמ םש501 
3 12 SALES_MODULE77 300 18 ERPG_SITEINTERFACE70 
3 14 CUSTSPECVALUES77 0 19 ERPG_CUSTSPECVALUES70 
EOF
echo End of Upgrade 7.0.12
