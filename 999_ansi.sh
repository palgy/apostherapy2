echo Begin of Upgrade 7.0.159
echo Title: ����� ������
DBI <<\EOF
FOR TABLE CUSTNOTETYPES INSERT ERPG_OPEN_SH
(CHAR,1,'OPEN_SH');
EOF
DBI <<\EOF
CREATE TABLE ERPG_SITEINTERFACE 'Incoming interfaces from site' 0
LINE(INT,13,'LINE')
UPLOADDATE(DATE,14,'Upload Date')
LOADED(CHAR,1,'Loaded?')
ERRORS(CHAR,1,'Errors?')
ERRORDESC(CHAR,120,'Error Description')
CUSTOMER(INT,13,'Customer')
CUSTOMERNUM(CHAR,24,'Customer Number')
APPOINTMENTDATE(DATE,8,'Appointment Date')
APPOINTMENTTYPE(CHAR,6,'Appointment Type')
PATIENTID(CHAR,24,'Patient ID')
PATIENTDOB(DATE,8,'Patient D.O.B.')
PATIENTGENDER(CHAR,6,'Patient Gender')
PHYSIONAME(CHAR,40,'Physiotherapist Name')
APPOINTMENTOUTCOME(CHAR,3,'Appointment Outcome')
ATTENDEDFOLLOWUP(CHAR,3,'Attended Follow Up?')
ADDITIONALCOMMENTS(CHAR,120,'Additional Comments')
DOC(INT,13,'Document Number')
UNIQUE(LINE)
;
EOF
DBI <<\EOF
CREATE TABLE ERPG_CUSTSHIP 'Customer Shipments' 0
HEADERLINE(INT,13,'Header Line')
LINE(INT,13,'Line')
CUSTOMER(INT,13,'Customer')
WARHS(INT,13,'Sending Warehouse')
PARTNUM(CHAR,24,'Part Number')
AMOUNT(REAL,12,3,'Amount')
LOADED(CHAR,1,'Loaded?')
ERRORS(CHAR,1,'Errors?')
ERRORDES(CHAR,120,'Error Description')
UNIQUE(HEADERLINE,LINE)
;
EOF
DBI <<\EOF
FOR TABLE ERPG_SITEINTERFACE
COLUMN UPLOADDATE
CHANGE WIDTH TO 8 ;
EOF
DBI <<\EOF
FOR TABLE ERPG_SITEINTERFACE INSERT CUSTNOTE
(INT,13,'task number');
EOF
DBI <<\EOF
FOR TABLE ERPG_CUSTSHIP
DELETE CUSTOMER ;
EOF
DBI <<\EOF
FOR TABLE ERPG_SITEINTERFACE
DELETE CUSTOMER ;
EOF
DBI <<\EOF
FOR TABLE ERPG_SITEINTERFACE
DELETE CUSTOMERNUM ;
EOF
DBI <<\EOF
FOR TABLE ERPG_SITEINTERFACE INSERT CUSTNAME
(CHAR,20,'CUSTOMERS NUMBER');
EOF
DBI <<\EOF
FOR TABLE ERPG_SITEINTERFACE INSERT CUSTDES
(CHAR,20,'CUSTOMER DESCRIPTION');
EOF
DBI <<\EOF
FOR TABLE ERPG_CUSTSHIP INSERT CUSTNAME
(CHAR,20,'CUSTOMERS NUMBER');
EOF
BRING << \EOF
18 18 ERPG_SITEINTERFACE0 
56 18 ERPG_SITEINTERFACE29 Incoming interfaces from site0 
19 18 ERPG_SITEINTERFACE1 4 LINE3 INT13 8 4 LINE
33 18 ERPG_SITEINTERFACE4 LINE0 48 
19 18 ERPG_SITEINTERFACE2 10 UPLOADDATE4 DATE8 8 11 Upload Date
19 18 ERPG_SITEINTERFACE3 6 LOADED4 CHAR1 1 7 Loaded?
19 18 ERPG_SITEINTERFACE4 6 ERRORS4 CHAR1 1 7 Errors?
19 18 ERPG_SITEINTERFACE5 9 ERRORDESC4 CHAR120 120 17 Error Description
19 18 ERPG_SITEINTERFACE6 15 APPOINTMENTDATE4 DATE8 8 16 Appointment Date
19 18 ERPG_SITEINTERFACE7 15 APPOINTMENTTYPE4 CHAR6 6 16 Appointment Type
19 18 ERPG_SITEINTERFACE8 9 PATIENTID4 CHAR24 24 10 Patient ID
19 18 ERPG_SITEINTERFACE9 10 PATIENTDOB4 DATE8 8 14 Patient D.O.B.
19 18 ERPG_SITEINTERFACE10 13 PATIENTGENDER4 CHAR6 6 14 Patient Gender
19 18 ERPG_SITEINTERFACE11 10 PHYSIONAME4 CHAR40 40 20 Physiotherapist Name
19 18 ERPG_SITEINTERFACE12 18 APPOINTMENTOUTCOME4 CHAR3 3 19 Appointment Outcome
19 18 ERPG_SITEINTERFACE13 16 ATTENDEDFOLLOWUP4 CHAR3 3 19 Attended Follow Up?
19 18 ERPG_SITEINTERFACE14 18 ADDITIONALCOMMENTS4 CHAR120 120 19 Additional Comments
19 18 ERPG_SITEINTERFACE15 3 DOC3 INT13 8 15 Document Number
33 18 ERPG_SITEINTERFACE3 DOC0 48 
19 18 ERPG_SITEINTERFACE16 8 CUSTNOTE3 INT13 8 11 task number
33 18 ERPG_SITEINTERFACE8 CUSTNOTE0 48 
19 18 ERPG_SITEINTERFACE17 8 CUSTNAME4 CHAR20 20 16 CUSTOMERS NUMBER
19 18 ERPG_SITEINTERFACE18 7 CUSTDES4 CHAR20 20 20 CUSTOMER DESCRIPTION
20 18 ERPG_SITEINTERFACE85 1 
21 18 ERPG_SITEINTERFACE1 4 LINE1 
17 18 ERPG_SITEINTERFACE29 Incoming interfaces from site18 ERPG_SITEINTERFACE70 0 0 0 78 4 EPRG0 0 
57 10 ���� �����1 
58 1 18 ERPG_SITEINTERFACE70 
24 2 11 POST-INSERT70 
11 18 ERPG_SITEINTERFACE70 11 POST-INSERT70 
22 18 ERPG_SITEINTERFACE70 11 POST-INSERT70 32 #INCLUDE ERPG_SITEINTERFACE/BUF11 1 
24 6 11 POST-UPDATE70 
11 18 ERPG_SITEINTERFACE70 11 POST-UPDATE70 
22 18 ERPG_SITEINTERFACE70 11 POST-UPDATE70 32 #INCLUDE ERPG_SITEINTERFACE/BUF11 1 
24 21 4 BUF170 
11 18 ERPG_SITEINTERFACE70 4 BUF170 
22 18 ERPG_SITEINTERFACE70 4 BUF170 17 /*open custnote*/1 1 
22 18 ERPG_SITEINTERFACE70 4 BUF170 8 GOTO 9992 2 
22 18 ERPG_SITEINTERFACE70 4 BUF170 24 WHERE EXISTS (SELECT 'X'3 3 
22 18 ERPG_SITEINTERFACE70 4 BUF170 18 FROM CUSTNOTETYPES4 4 
22 18 ERPG_SITEINTERFACE70 4 BUF170 59 WHERE CUSTNOTETYPES.CUSTNOTETYPEDES = :$.APPOINTMENTOUTCOME5 5 
22 18 ERPG_SITEINTERFACE70 4 BUF170 38 AND CUSTNOTETYPES.ERPG_OPEN_SH = 'Y');6 6 
22 18 ERPG_SITEINTERFACE70 4 BUF170 40 SELECT SQL.TMPFILE INTO :GEN FROM DUMMY;7 7 
22 18 ERPG_SITEINTERFACE70 4 BUF170 25 LINK GENERALLOAD TO :GEN;8 8 
22 18 ERPG_SITEINTERFACE70 4 BUF170 29 GOTO 9999 WHERE :RETVAL <= 0;9 9 
22 18 ERPG_SITEINTERFACE70 4 BUF170 49 INSERT INTO GENERALLOAD (LINE, RECORDTYPE, TEXT6)10 10 
22 18 ERPG_SITEINTERFACE70 4 BUF170 29 VALUES (1, '1', :$.CUSTNAME);11 11 
22 18 ERPG_SITEINTERFACE70 4 BUF170 56 INSERT INTO GENERALLOAD (LINE, RECORDTYPE, DATE1, TEXT6)12 12 
22 18 ERPG_SITEINTERFACE70 4 BUF170 54 VALUES (2, '2', :$.UPLOADDATE, :$.APPOINTMENTOUTCOME);13 13 
22 18 ERPG_SITEINTERFACE70 4 BUF170 66 EXECUTE INTERFACE 'ERPG_CUSTNOTE', SQL.TMPFILE, '-nl', '-L', :GEN;14 14 
22 18 ERPG_SITEINTERFACE70 4 BUF170 44 SELECT * FROM GENERALLOAD TABS 'C:\tmp\GEN';15 15 
22 18 ERPG_SITEINTERFACE70 4 BUF170 17 :ERPG_ERROR = '';16 16 
22 18 ERPG_SITEINTERFACE70 4 BUF170 14 :CUSTNOTE = 0;17 17 
22 18 ERPG_SITEINTERFACE70 4 BUF170 14 SELECT MESSAGE18 18 
22 18 ERPG_SITEINTERFACE70 4 BUF170 16 INTO :ERPG_ERROR19 19 
22 18 ERPG_SITEINTERFACE70 4 BUF170 12 FROM ERRMSGS20 20 
22 18 ERPG_SITEINTERFACE70 4 BUF170 37 WHERE TYPE = 'i' AND USER = SQL.USER;21 21 
22 18 ERPG_SITEINTERFACE70 4 BUF170 18 SELECT ATOI (KEY1)22 22 
22 18 ERPG_SITEINTERFACE70 4 BUF170 14 INTO :CUSTNOTE23 23 
22 18 ERPG_SITEINTERFACE70 4 BUF170 16 FROM GENERALLOAD24 24 
22 18 ERPG_SITEINTERFACE70 4 BUF170 23 WHERE RECORDTYPE = '2';25 25 
22 18 ERPG_SITEINTERFACE70 4 BUF170 25 UPDATE ERPG_SITEINTERFACE26 26 
22 18 ERPG_SITEINTERFACE70 4 BUF170 28 SET ERRORDESC = :ERPG_ERROR,27 27 
22 18 ERPG_SITEINTERFACE70 4 BUF170 40 ERRORS = (:ERPG_ERROR <> '' ? 'Y' : ''),28 28 
22 18 ERPG_SITEINTERFACE70 4 BUF170 20 CUSTNOTE = :CUSTNOTE29 29 
22 18 ERPG_SITEINTERFACE70 4 BUF170 21 WHERE LINE = :$.LINE;30 30 
22 18 ERPG_SITEINTERFACE70 4 BUF170 11 LABEL 9999;31 31 
22 18 ERPG_SITEINTERFACE70 4 BUF170 19 UNLINK GENERALLOAD;32 32 
22 18 ERPG_SITEINTERFACE70 4 BUF170 10 LABEL 999;33 33 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE18 ADDITIONALCOMMENTS0 18 ADDITIONALCOMMENTS0 0 120 0 160 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE15 APPOINTMENTDATE0 15 APPOINTMENTDATE0 0 8 0 80 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE18 APPOINTMENTOUTCOME0 18 APPOINTMENTOUTCOME0 0 3 0 140 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE15 APPOINTMENTTYPE0 15 APPOINTMENTTYPE0 0 6 0 90 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE16 ATTENDEDFOLLOWUP0 16 ATTENDEDFOLLOWUP0 0 3 0 150 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE7 CUSTDES0 7 CUSTDES0 0 20 0 70 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE8 CUSTNAME0 8 CUSTNAME0 0 20 0 60 0 0 0 0 0 0 0 1 
24 9062 9 ERPG_BUF167 
8 18 ERPG_SITEINTERFACE70 8 CUSTNAME9 ERPG_BUF167 
14 18 ERPG_SITEINTERFACE70 8 CUSTNAME9 ERPG_BUF167 11 :LINE =  0;1 1 
14 18 ERPG_SITEINTERFACE70 8 CUSTNAME9 ERPG_BUF167 20 SELECT MAX(LINE) + 12 2 
14 18 ERPG_SITEINTERFACE70 8 CUSTNAME9 ERPG_BUF167 10 INTO :LINE3 3 
14 18 ERPG_SITEINTERFACE70 8 CUSTNAME9 ERPG_BUF167 23 FROM ERPG_SITEINTERFACE4 4 
14 18 ERPG_SITEINTERFACE70 8 CUSTNAME9 ERPG_BUF167 18 WHERE :$.LINE = 0;5 5 
14 18 ERPG_SITEINTERFACE70 8 CUSTNAME9 ERPG_BUF167 16 :$.LINE = :LINE;6 6 
24 19093 15 ERPG_POST-FIELD67 
8 18 ERPG_SITEINTERFACE70 8 CUSTNAME15 ERPG_POST-FIELD67 
14 18 ERPG_SITEINTERFACE70 8 CUSTNAME15 ERPG_POST-FIELD67 46 #INCLUDE ERPG_SITEINTERFACE/CUSTNAME/ERPG_BUF11 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE8 CUSTNOTE0 8 CUSTNOTE0 0 13 48 180 0 0 0 0 0 0 0 1 
18 9 DOCUMENTS1 
56 9 DOCUMENTS18 ���� ������ ������0 
19 9 DOCUMENTS1 3 DOC3 INT13 4 10 (ID) �����
19 9 DOCUMENTS2 5 WARHS3 INT13 4 9 (ID) ����
19 9 DOCUMENTS3 5 DOCNO4 CHAR16 16 5 �����
19 9 DOCUMENTS4 7 CURDATE4 DATE8 4 5 �����
19 9 DOCUMENTS5 7 TOWARHS3 INT13 4 10 (ID) �����
19 9 DOCUMENTS6 4 TYPE4 CHAR1 1 5 �����
19 9 DOCUMENTS7 4 USER3 INT13 4 10 (ID) �����
19 9 DOCUMENTS8 4 CUST3 INT13 4 13 (ID) ���� '��
19 9 DOCUMENTS9 6 DOCREF4 CHAR16 16 7 ��� '��
19 9 DOCUMENTS10 3 ORD3 INT13 4 10 (ID) �����
19 9 DOCUMENTS11 4 PDOC3 INT13 4 17 (ID) ������ �����
19 9 DOCUMENTS12 5 LORRY3 INT13 4 10 (ID) �����
33 9 DOCUMENTS5 LORRY0 48 
19 9 DOCUMENTS13 5 UDATE4 DATE14 4 8 ����� .�
19 9 DOCUMENTS14 4 FLAG4 CHAR1 1 3 ���
19 9 DOCUMENTS15 9 FORSERIAL3 INT13 4 14 (ID) �"�� ����
33 9 DOCUMENTS9 FORSERIAL0 48 
19 9 DOCUMENTS16 5 IVALL4 CHAR1 1 5 �����
19 9 DOCUMENTS17 6 CANCEL4 CHAR1 1 5 �����
19 9 DOCUMENTS18 5 FINAL4 CHAR1 1 4 ����
19 9 DOCUMENTS19 7 PRINTED4 CHAR1 1 5 �����
19 9 DOCUMENTS20 8 SHIPTYPE3 INT13 4 16 (ID) ����� �����
33 9 DOCUMENTS8 SHIPTYPE0 48 
19 9 DOCUMENTS21 11 EXTFILEFLAG4 CHAR2 2 7 ?������
19 9 DOCUMENTS22 7 BOOKNUM4 CHAR16 16 11 ����� �����
19 9 DOCUMENTS23 8 DESTCODE3 INT13 4 8 (ID) ���
33 9 DOCUMENTS8 DESTCODE0 48 
19 9 DOCUMENTS24 6 STORNO4 CHAR1 1 12 ?����� �����
19 9 DOCUMENTS25 12 COMPANALYSIS4 CHAR1 1 12 ?���� ������
19 9 DOCUMENTS26 4 PACK3 INT13 4 10 (ID) �����
33 9 DOCUMENTS4 PACK0 48 
19 9 DOCUMENTS27 6 WEIGHT4 REAL10 8 10 ����� ����
33 9 DOCUMENTS6 WEIGHT2 50 
19 9 DOCUMENTS28 2 PL4 CHAR1 1 8 PL �����
19 9 DOCUMENTS29 6 QPRICE4 REAL16 8 9 ���� ����
33 9 DOCUMENTS6 QPRICE2 50 
19 9 DOCUMENTS30 3 VAT4 REAL16 8 4 �"��
33 9 DOCUMENTS3 VAT2 50 
19 9 DOCUMENTS31 8 TOTPRICE4 REAL16 8 14 �"�� ���� ����
33 9 DOCUMENTS8 TOTPRICE2 50 
19 9 DOCUMENTS32 7 PERCENT4 REAL8 8 14 (%) ����� ����
33 9 DOCUMENTS7 PERCENT2 50 
19 9 DOCUMENTS33 8 CURRENCY3 INT13 4 9 (ID) ����
33 9 DOCUMENTS8 CURRENCY0 48 
19 9 DOCUMENTS34 8 VATPRICE4 REAL16 8 15 ����� ���� �"��
33 9 DOCUMENTS8 VATPRICE2 50 
19 9 DOCUMENTS35 12 ADJPRICEFLAG4 CHAR1 1 13 ?������ �����
19 9 DOCUMENTS36 5 PHONE3 INT13 4 12 (ID) ��� ���
33 9 DOCUMENTS5 PHONE0 48 
19 9 DOCUMENTS37 10 PARENTSERN3 INT13 4 13 (ID) �� �����
33 9 DOCUMENTS10 PARENTSERN0 48 
19 9 DOCUMENTS38 6 RMADOC3 INT13 4 14 (ID) RMA �����
33 9 DOCUMENTS6 RMADOC0 48 
19 9 DOCUMENTS39 5 PLIST3 INT13 4 11 (ID) ������
33 9 DOCUMENTS5 PLIST0 48 
19 9 DOCUMENTS40 7 SHIPPER3 INT13 4 18 (ID) ��/��/�����.�
33 9 DOCUMENTS7 SHIPPER0 48 
19 9 DOCUMENTS41 7 DETAILS5 RCHAR24 24 5 �����
19 9 DOCUMENTS42 2 IV3 INT13 4 12 (ID) �������
33 9 DOCUMENTS2 IV0 48 
19 9 DOCUMENTS43 5 AGENT3 INT13 4 9 (ID) ����
33 9 DOCUMENTS5 AGENT0 48 
19 9 DOCUMENTS44 8 IDCODEPT4 CHAR16 16 19 ������� - �� ������
19 9 DOCUMENTS45 11 ERP_IMPTERM3 INT13 8 14 (ID) ���� ����
33 9 DOCUMENTS11 ERP_IMPTERM0 48 
19 9 DOCUMENTS46 11 ERP_EXPFILE3 INT13 8 13 (ID) ���� ���
33 9 DOCUMENTS11 ERP_EXPFILE0 48 
19 9 DOCUMENTS47 11 HIGH_LENGTH4 REAL17 8 11 ������ ����
33 9 DOCUMENTS11 HIGH_LENGTH2 50 
19 9 DOCUMENTS48 10 HIGH_WIDTH4 REAL17 8 9 ���� ����
33 9 DOCUMENTS10 HIGH_WIDTH2 50 
19 9 DOCUMENTS49 11 HIGH_HEIGHT4 REAL17 8 9 ���� ����
33 9 DOCUMENTS11 HIGH_HEIGHT2 50 
19 9 DOCUMENTS50 11 ERP_ENDCUST3 INT13 8 9 ���� ����
33 9 DOCUMENTS11 ERP_ENDCUST0 48 
20 9 DOCUMENTS65 1 
21 9 DOCUMENTS1 3 DOC1 
20 9 DOCUMENTS85 2 
21 9 DOCUMENTS2 5 DOCNO1 
21 9 DOCUMENTS2 4 TYPE2 
20 9 DOCUMENTS78 3 
21 9 DOCUMENTS3 7 CURDATE1 
20 9 DOCUMENTS78 4 
21 9 DOCUMENTS4 3 ORD1 
20 9 DOCUMENTS78 5 
21 9 DOCUMENTS5 7 BOOKNUM1 
20 9 DOCUMENTS78 6 
21 9 DOCUMENTS6 7 CURDATE3 
21 9 DOCUMENTS6 4 CUST1 
21 9 DOCUMENTS6 5 IVALL2 
20 9 DOCUMENTS78 7 
21 9 DOCUMENTS7 7 CURDATE2 
21 9 DOCUMENTS7 4 TYPE1 
20 9 DOCUMENTS78 8 
21 9 DOCUMENTS8 4 TYPE1 
21 9 DOCUMENTS8 4 CUST3 
21 9 DOCUMENTS8 4 PDOC2 
20 9 DOCUMENTS78 9 
21 9 DOCUMENTS9 7 TOWARHS2 
21 9 DOCUMENTS9 4 TYPE1 
13 18 ERPG_SITEINTERFACE70 9 DOCUMENTS5 DOCNO0 5 DOCNO0 8 Document16 0 170 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE9 ERRORDESC0 9 ERRORDESC0 0 120 0 50 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE6 ERRORS0 6 ERRORS0 0 1 0 40 10 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE4 LINE0 4 LINE0 0 13 48 10 0 0 82 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE6 LOADED0 6 LOADED0 0 1 0 30 10 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE10 PATIENTDOB0 10 PATIENTDOB0 0 8 0 110 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE13 PATIENTGENDER0 13 PATIENTGENDER0 0 6 0 120 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE9 PATIENTID0 9 PATIENTID0 0 24 0 100 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE10 PHYSIONAME0 10 PHYSIONAME0 0 40 0 130 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE10 UPLOADDATE0 10 UPLOADDATE0 0 8 0 20 0 0 0 0 0 0 0 1 
13 18 ERPG_SITEINTERFACE70 18 ERPG_SITEINTERFACE3 DOC0 3 DOC0 0 13 48 99 0 72 0 0 9 DOCUMENTS3 DOC10 1 
18 11 GENERALLOAD0 
56 11 GENERALLOAD16 ����� ����� ����0 
19 11 GENERALLOAD1 10 RECORDTYPE4 CHAR3 3 9 ����� ���
19 11 GENERALLOAD2 4 LINE3 INT17 4 4 ����
33 11 GENERALLOAD4 LINE0 48 
19 11 GENERALLOAD3 6 LOADED4 CHAR1 1 5 ?����
19 11 GENERALLOAD4 4 KEY14 CHAR20 20 6 1 ����
19 11 GENERALLOAD5 4 KEY24 CHAR20 20 6 2 ����
19 11 GENERALLOAD6 4 KEY34 CHAR20 20 6 3 ����
19 11 GENERALLOAD7 5 TEXT14 CHAR32 32 5 TEXT1
19 11 GENERALLOAD8 5 TEXT24 CHAR48 48 5 TEXT2
19 11 GENERALLOAD9 5 TEXT34 CHAR48 48 5 TEXT3
19 11 GENERALLOAD10 4 INT13 INT17 4 4 INT1
33 11 GENERALLOAD4 INT10 48 
19 11 GENERALLOAD11 4 INT23 INT17 4 4 INT2
33 11 GENERALLOAD4 INT20 48 
19 11 GENERALLOAD12 4 INT33 INT17 4 4 INT3
33 11 GENERALLOAD4 INT30 48 
19 11 GENERALLOAD13 5 CHAR14 CHAR1 1 5 CHAR1
19 11 GENERALLOAD14 5 CHAR24 CHAR1 1 5 CHAR2
19 11 GENERALLOAD15 5 CHAR34 CHAR1 1 5 CHAR3
19 11 GENERALLOAD16 5 CHAR44 CHAR1 1 5 CHAR4
19 11 GENERALLOAD17 4 TEXT5 RCHAR100 100 10 ����� ����
19 11 GENERALLOAD18 4 INT43 INT17 4 4 INT4
33 11 GENERALLOAD4 INT40 48 
19 11 GENERALLOAD19 4 INT53 INT17 4 4 INT5
33 11 GENERALLOAD4 INT50 48 
19 11 GENERALLOAD20 4 INT63 INT17 4 4 INT6
33 11 GENERALLOAD4 INT60 48 
19 11 GENERALLOAD21 4 INT73 INT17 4 4 INT7
33 11 GENERALLOAD4 INT70 48 
19 11 GENERALLOAD22 4 INT83 INT17 4 4 INT8
33 11 GENERALLOAD4 INT80 48 
19 11 GENERALLOAD23 5 REAL14 REAL16 8 5 REAL1
33 11 GENERALLOAD5 REAL15 53 
19 11 GENERALLOAD24 5 REAL24 REAL16 8 5 REAL2
33 11 GENERALLOAD5 REAL22 50 
19 11 GENERALLOAD25 5 REAL34 REAL22 8 5 REAL3
33 11 GENERALLOAD5 REAL35 53 
19 11 GENERALLOAD26 5 REAL44 REAL22 8 5 REAL4
33 11 GENERALLOAD5 REAL45 53 
19 11 GENERALLOAD27 5 REAL54 REAL16 8 5 REAL5
33 11 GENERALLOAD5 REAL52 50 
19 11 GENERALLOAD28 5 REAL64 REAL16 8 5 REAL6
33 11 GENERALLOAD5 REAL62 50 
19 11 GENERALLOAD29 5 TEXT44 CHAR22 22 5 TEXT4
19 11 GENERALLOAD30 5 TEXT54 CHAR22 22 5 TEXT5
19 11 GENERALLOAD31 5 TEXT64 CHAR20 20 5 TEXT6
19 11 GENERALLOAD32 5 DATE14 DATE14 4 5 DATE1
19 11 GENERALLOAD33 5 DATE24 DATE14 4 5 DATE2
19 11 GENERALLOAD34 5 DATE34 DATE14 4 5 DATE3
19 11 GENERALLOAD35 5 DATE44 DATE14 4 5 DATE4
19 11 GENERALLOAD36 5 TEXT74 CHAR100 100 5 TEXT7
19 11 GENERALLOAD37 5 TEXT94 CHAR56 56 5 TEXT9
19 11 GENERALLOAD38 5 TEXT84 CHAR32 32 5 TEXT8
19 11 GENERALLOAD39 4 KEY44 CHAR20 20 6 4 ����
19 11 GENERALLOAD40 5 CHAR54 CHAR1 1 5 CHAR5
19 11 GENERALLOAD41 5 CHAR64 CHAR1 1 5 CHAR6
19 11 GENERALLOAD42 5 CHAR74 CHAR1 1 5 CHAR7
19 11 GENERALLOAD43 5 CHAR84 CHAR1 1 5 CHAR8
19 11 GENERALLOAD44 4 INT93 INT17 4 4 INT9
33 11 GENERALLOAD4 INT90 48 
19 11 GENERALLOAD45 5 INT103 INT17 4 5 INT10
33 11 GENERALLOAD5 INT100 48 
19 11 GENERALLOAD46 5 CHAR94 CHAR1 1 5 CHAR9
19 11 GENERALLOAD47 6 CHAR104 CHAR1 1 6 CHAR10
19 11 GENERALLOAD48 6 CHAR114 CHAR1 1 6 CHAR11
19 11 GENERALLOAD49 6 CHAR124 CHAR1 1 6 CHAR12
19 11 GENERALLOAD50 6 CHAR134 CHAR1 1 6 CHAR13
19 11 GENERALLOAD51 6 CHAR144 CHAR1 1 6 CHAR14
19 11 GENERALLOAD52 6 CHAR154 CHAR1 1 6 CHAR15
19 11 GENERALLOAD53 6 CHAR164 CHAR1 1 6 CHAR16
19 11 GENERALLOAD54 6 CHAR174 CHAR1 1 6 CHAR17
19 11 GENERALLOAD55 6 CHAR184 CHAR1 1 6 CHAR18
19 11 GENERALLOAD56 6 CHAR194 CHAR1 1 6 CHAR19
19 11 GENERALLOAD57 6 CHAR204 CHAR1 1 6 CHAR20
19 11 GENERALLOAD58 5 REAL74 REAL17 8 5 REAL7
33 11 GENERALLOAD5 REAL79 57 
19 11 GENERALLOAD59 5 INT113 INT17 8 5 INT11
33 11 GENERALLOAD5 INT110 48 
19 11 GENERALLOAD60 5 INT123 INT17 8 5 INT12
33 11 GENERALLOAD5 INT120 48 
19 11 GENERALLOAD61 5 INT133 INT17 8 5 INT13
33 11 GENERALLOAD5 INT130 48 
19 11 GENERALLOAD62 5 INT143 INT17 8 5 INT14
33 11 GENERALLOAD5 INT140 48 
19 11 GENERALLOAD63 5 INT153 INT17 8 5 INT15
33 11 GENERALLOAD5 INT150 48 
19 11 GENERALLOAD64 6 TEXT104 CHAR20 20 6 TEXT10
19 11 GENERALLOAD65 6 TEXT114 CHAR20 20 6 TEXT11
19 11 GENERALLOAD66 6 TEXT124 CHAR20 20 6 TEXT12
19 11 GENERALLOAD67 6 TEXT134 CHAR20 20 6 TEXT13
19 11 GENERALLOAD68 6 TEXT144 CHAR20 20 6 TEXT14
19 11 GENERALLOAD69 6 TEXT154 CHAR20 20 6 TEXT15
19 11 GENERALLOAD70 6 TEXT165 RCHAR24 24 6 TEXT16
19 11 GENERALLOAD71 6 TEXT174 CHAR20 20 6 TEXT17
19 11 GENERALLOAD72 5 REAL84 REAL16 8 5 REAL8
33 11 GENERALLOAD5 REAL82 50 
19 11 GENERALLOAD73 5 REAL94 REAL16 8 5 REAL9
33 11 GENERALLOAD5 REAL92 50 
19 11 GENERALLOAD74 6 REAL104 REAL16 8 6 REAL10
33 11 GENERALLOAD6 REAL102 50 
19 11 GENERALLOAD75 6 REAL114 REAL16 8 6 REAL11
33 11 GENERALLOAD6 REAL112 50 
19 11 GENERALLOAD76 6 TEXT184 CHAR20 20 6 TEXT18
19 11 GENERALLOAD77 6 CHAR214 CHAR1 1 6 CHAR21
19 11 GENERALLOAD78 6 CHAR224 CHAR1 1 6 CHAR22
19 11 GENERALLOAD79 6 CHAR234 CHAR1 1 6 CHAR23
19 11 GENERALLOAD80 6 CHAR244 CHAR1 1 6 CHAR24
19 11 GENERALLOAD81 6 CHAR254 CHAR1 1 6 CHAR25
20 11 GENERALLOAD85 1 
21 11 GENERALLOAD1 4 LINE1 
20 11 GENERALLOAD78 2 
21 11 GENERALLOAD2 4 INT11 
17 13 ERPG_CUSTNOTE12 ����� ������11 GENERALLOAD73 89 0 0 89 0 0 1000 
57 10 ���� �����1 
58 1 13 ERPG_CUSTNOTE73 
59 13 ERPG_CUSTNOTE73 1 19 CUSTOMERS70 0 
59 13 ERPG_CUSTNOTE73 1 29 CUSTNOTES70 0 
60 13 ERPG_CUSTNOTE73 5 DATE19 CUSTNOTES70 7 CURDATE1 0 0 0 0 0 
60 13 ERPG_CUSTNOTE73 5 TEXT69 CUSTOMERS70 8 CUSTNAME1 0 0 0 0 0 
60 13 ERPG_CUSTNOTE73 7 TEXT6@29 CUSTNOTES70 15 CUSTNOTETYPEDES2 0 0 0 0 0 
18 13 ERPG_CUSTSHIP0 
56 13 ERPG_CUSTSHIP18 Customer Shipments0 
19 13 ERPG_CUSTSHIP1 10 HEADERLINE3 INT13 8 11 Header Line
33 13 ERPG_CUSTSHIP10 HEADERLINE0 48 
19 13 ERPG_CUSTSHIP2 4 LINE3 INT13 8 4 Line
33 13 ERPG_CUSTSHIP4 LINE0 48 
19 13 ERPG_CUSTSHIP3 5 WARHS3 INT13 8 17 Sending Warehouse
33 13 ERPG_CUSTSHIP5 WARHS0 48 
19 13 ERPG_CUSTSHIP4 7 PARTNUM4 CHAR24 24 11 Part Number
19 13 ERPG_CUSTSHIP5 6 AMOUNT4 REAL12 8 6 Amount
33 13 ERPG_CUSTSHIP6 AMOUNT3 51 
19 13 ERPG_CUSTSHIP6 6 LOADED4 CHAR1 1 7 Loaded?
19 13 ERPG_CUSTSHIP7 6 ERRORS4 CHAR1 1 7 Errors?
19 13 ERPG_CUSTSHIP8 8 ERRORDES4 CHAR120 120 17 Error Description
19 13 ERPG_CUSTSHIP9 8 CUSTNAME4 CHAR20 20 16 CUSTOMERS NUMBER
20 13 ERPG_CUSTSHIP85 1 
21 13 ERPG_CUSTSHIP1 10 HEADERLINE1 
21 13 ERPG_CUSTSHIP1 4 LINE2 
17 13 ERPG_CUSTSHIP18 Customer Shipments13 ERPG_CUSTSHIP70 0 0 0 78 4 ERPG0 0 
57 10 ���� �����1 
58 1 13 ERPG_CUSTSHIP70 
24 9067 9 ERPG-BUF170 
11 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 26 /*open shipping document*/1 1 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 30 UNLINK AND REMOVE GENERALLOAD;2 2 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 40 SELECT SQL.TMPFILE INTO :GEN FROM DUMMY;3 3 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 25 LINK GENERALLOAD TO :GEN;4 4 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 29 GOTO 9999 WHERE :RETVAL <= 0;5 5 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 57 INSERT INTO GENERALLOAD (LINE, RECORDTYPE, TEXT6, TEXT10)6 6 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 44 VALUES (1, '1', :$$.CUSTNAME, :$.WARHSNAME);7 7 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 55 INSERT INTO GENERALLOAD (LINE, RECORDTYPE, TEXT6, INT1)8 8 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 52 SELECT SQL.LINE + 1, '2', PARTNUM, INTQUANT (AMOUNT)9 9 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 18 FROM ERPG_CUSTSHIP10 10 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 28 WHERE HEADERLINE = :$$.LINE;11 11 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 51 EXECUTE INTERFACE 'ERPG_DOC_D' , SQL.TMPFILE,'-nl',12 12 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 11 '-L', :GEN;13 13 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 45 SELECT * FROM GENERALLOAD TABS 'C:\tmp\GENE';14 14 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 28 :ERPG_ERROR = ''; :DOC = '';15 15 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 14 SELECT MESSAGE16 16 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 16 INTO :ERPG_ERROR17 17 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 12 FROM ERRMSGS18 18 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 37 WHERE TYPE = 'i' AND USER = SQL.USER;19 19 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 25 UPDATE ERPG_SITEINTERFACE20 20 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 41 SET ERRORDESC = :ERPG_ERROR, ERRORS = 'Y'21 21 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 21 WHERE LINE = :$$.LINE22 22 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 22 AND :ERPG_ERROR <> '';23 23 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 10 :KEY1 = 0;24 24 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 18 SELECT ATOI (KEY1)25 25 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 10 INTO :KEY126 26 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 16 FROM GENERALLOAD27 27 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 15 WHERE LINE = 1;28 28 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 25 UPDATE ERPG_SITEINTERFACE29 29 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 15 SET DOC = :KEY130 30 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 22 WHERE LINE = :$$.LINE;31 31 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 11 LABEL 9999;32 32 
22 13 ERPG_CUSTSHIP70 9 ERPG-BUF170 30 UNLINK AND REMOVE GENERALLOAD;33 33 
24 9071 14 ERPG-POST-FORM70 
11 13 ERPG_CUSTSHIP70 14 ERPG-POST-FORM70 
22 13 ERPG_CUSTSHIP70 14 ERPG-POST-FORM70 32 #INCLUDE ERPG_CUSTSHIP/ERPG-BUF11 1 
13 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP6 AMOUNT0 6 AMOUNT0 0 12 51 60 0 0 0 0 0 0 0 1 
13 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP8 CUSTNAME0 8 CUSTNAME0 0 20 0 30 0 0 0 0 0 0 0 1 
24 9062 9 ERPG_BUF167 
8 13 ERPG_CUSTSHIP70 8 CUSTNAME9 ERPG_BUF167 
14 13 ERPG_CUSTSHIP70 8 CUSTNAME9 ERPG_BUF167 45 SELECT WAREHOUSES.WARHSNAME, WAREHOUSES.WARHS1 1 
14 13 ERPG_CUSTSHIP70 8 CUSTNAME9 ERPG_BUF167 27 INTO :$.WARHSNAME, :$.WARHS2 2 
14 13 ERPG_CUSTSHIP70 8 CUSTNAME9 ERPG_BUF167 41 FROM CUSTOMERS, WAREHOUSES, ERPG_CUSTSHIP3 3 
14 13 ERPG_CUSTSHIP70 8 CUSTNAME9 ERPG_BUF167 49 WHERE ERPG_CUSTSHIP.CUSTNAME = CUSTOMERS.CUSTNAME4 4 
14 13 ERPG_CUSTSHIP70 8 CUSTNAME9 ERPG_BUF167 36 AND CUSTOMERS.CUST = WAREHOUSES.CUST5 5 
14 13 ERPG_CUSTSHIP70 8 CUSTNAME9 ERPG_BUF167 41 AND ERPG_CUSTSHIP.CUSTNAME = :$.CUSTNAME;6 6 
24 19093 15 ERPG_POST-FIELD67 
8 13 ERPG_CUSTSHIP70 8 CUSTNAME15 ERPG_POST-FIELD67 
14 13 ERPG_CUSTSHIP70 8 CUSTNAME15 ERPG_POST-FIELD67 41 #INCLUDE ERPG_CUSTSHIP/CUSTNAME/ERPG_BUF11 1 
13 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP8 ERRORDES0 8 ERRORDES0 0 120 0 90 0 0 0 0 0 0 0 1 
13 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP6 ERRORS0 6 ERRORS0 0 1 0 80 10 0 0 0 0 0 0 1 
13 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP10 HEADERLINE0 10 HEADERLINE0 0 13 48 99 0 72 0 0 0 0 0 1 
15 13 ERPG_CUSTSHIP70 10 HEADERLINE0 70 8 :$$.LINE0 
13 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP4 LINE0 4 LINE0 0 13 48 20 0 0 82 0 0 0 0 1 
13 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP6 LOADED0 6 LOADED0 0 1 0 70 10 0 0 0 0 0 0 1 
13 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP7 PARTNUM0 7 PARTNUM0 0 24 0 50 0 0 0 0 0 0 0 1 
24 9062 9 ERPG_BUF167 
8 13 ERPG_CUSTSHIP70 7 PARTNUM9 ERPG_BUF167 
14 13 ERPG_CUSTSHIP70 7 PARTNUM9 ERPG_BUF167 11 :LINE =  0;1 1 
14 13 ERPG_CUSTSHIP70 7 PARTNUM9 ERPG_BUF167 16 SELECT MAX(LINE)2 2 
14 13 ERPG_CUSTSHIP70 7 PARTNUM9 ERPG_BUF167 10 INTO :LINE3 3 
14 13 ERPG_CUSTSHIP70 7 PARTNUM9 ERPG_BUF167 18 FROM ERPG_CUSTSHIP4 4 
14 13 ERPG_CUSTSHIP70 7 PARTNUM9 ERPG_BUF167 17 WHERE :$.LINE = 05 5 
14 13 ERPG_CUSTSHIP70 7 PARTNUM9 ERPG_BUF167 26 AND HEADERLINE = :$$.LINE;6 6 
14 13 ERPG_CUSTSHIP70 7 PARTNUM9 ERPG_BUF167 20 :$.LINE = :LINE + 1;7 7 
24 19093 15 ERPG_POST-FIELD67 
8 13 ERPG_CUSTSHIP70 7 PARTNUM15 ERPG_POST-FIELD67 
14 13 ERPG_CUSTSHIP70 7 PARTNUM15 ERPG_POST-FIELD67 40 #INCLUDE ERPG_CUSTSHIP/PARTNUM/ERPG_BUF11 1 
18 10 WAREHOUSES0 
56 10 WAREHOUSES6 ������0 
19 10 WAREHOUSES1 5 WARHS3 INT13 4 9 (ID) ����
19 10 WAREHOUSES2 9 WARHSNAME4 CHAR4 4 4 ����
19 10 WAREHOUSES3 4 TYPE4 CHAR1 1 11 D/F/W �����
19 10 WAREHOUSES4 8 WARHSDES5 RCHAR32 32 9 ���� ����
19 10 WAREHOUSES5 7 LOCNAME4 CHAR14 14 5 �����
19 10 WAREHOUSES6 8 LEADTIME4 TIME6 4 16 ������ ����� ���
33 10 WAREHOUSES8 LEADTIME0 48 
19 10 WAREHOUSES7 7 ADDRESS5 RCHAR80 80 5 �����
19 10 WAREHOUSES8 5 STATE5 RCHAR40 40 3 ���
19 10 WAREHOUSES9 3 ZIP4 CHAR10 10 5 �����
19 10 WAREHOUSES10 5 PHONE4 CHAR20 20 10 ����� ����
19 10 WAREHOUSES11 6 BUDGET3 INT13 4 10 (ID) �����
33 10 WAREHOUSES6 BUDGET0 48 
19 10 WAREHOUSES12 7 COUNTRY3 INT13 4 8 (ID) ���
33 10 WAREHOUSES7 COUNTRY0 48 
19 10 WAREHOUSES13 8 COSTFLAG4 CHAR1 1 14 ���� ���� ����
19 10 WAREHOUSES14 10 EXTINVFLAG4 CHAR1 1 20 ������� ������ �����
19 10 WAREHOUSES15 10 INVACCOUNT3 INT13 4 15 (ID) ���� �����
33 10 WAREHOUSES10 INVACCOUNT0 48 
19 10 WAREHOUSES16 10 COGACCOUNT3 INT13 4 20 (ID) ������ ���� .��
33 10 WAREHOUSES10 COGACCOUNT0 48 
19 10 WAREHOUSES17 10 VARACCOUNT3 INT13 4 19 (ID) ���� ����� .��
33 10 WAREHOUSES10 VARACCOUNT0 48 
19 10 WAREHOUSES18 10 DISACCOUNT3 INT13 4 17 (ID) ������ �����
33 10 WAREHOUSES10 DISACCOUNT0 48 
19 10 WAREHOUSES19 11 SMPLACCOUNT3 INT13 4 17 (ID) ������ �����
33 10 WAREHOUSES11 SMPLACCOUNT0 48 
19 10 WAREHOUSES20 11 GEXPACCOUNT3 INT13 4 20 (ID) ������ .��� .��
33 10 WAREHOUSES11 GEXPACCOUNT0 48 
19 10 WAREHOUSES21 10 INCACCOUNT3 INT13 4 17 (ID) ������ �����
33 10 WAREHOUSES10 INCACCOUNT0 48 
19 10 WAREHOUSES22 14 NOTFORPLANFLAG4 CHAR1 1 17 ������ ���� �����
19 10 WAREHOUSES23 5 COSTC3 INT13 4 14 (ID) ���� ����
33 10 WAREHOUSES5 COSTC0 48 
19 10 WAREHOUSES24 13 COSTTRANSFLAG4 CHAR1 1 20 ������ ������ ������
19 10 WAREHOUSES25 11 INC2ACCOUNT3 INT13 4 19 (ID) ������ .�� .��
33 10 WAREHOUSES11 INC2ACCOUNT0 48 
19 10 WAREHOUSES26 11 INC3ACCOUNT3 INT13 4 20 (ID) �"�� ������ .��
33 10 WAREHOUSES11 INC3ACCOUNT0 48 
19 10 WAREHOUSES27 6 BONDED4 CHAR1 1 11 ?����� ����
19 10 WAREHOUSES28 8 KITWARHS3 INT13 4 9 ���� ����
33 10 WAREHOUSES8 KITWARHS0 48 
19 10 WAREHOUSES29 12 LABORACCOUNT3 INT13 4 20 (ID)����� ���� �����
33 10 WAREHOUSES12 LABORACCOUNT0 48 
19 10 WAREHOUSES30 15 OVERHEADACCOUNT3 INT13 4 20 (ID)����� ���� �����
33 10 WAREHOUSES15 OVERHEADACCOUNT0 48 
19 10 WAREHOUSES31 15 PRICEVARACCOUNT3 INT13 4 20 (ID)���� ����� �����
33 10 WAREHOUSES15 PRICEVARACCOUNT0 48 
19 10 WAREHOUSES32 15 COGLABORACCOUNT3 INT13 4 20 (ID)����� COGS �����
33 10 WAREHOUSES15 COGLABORACCOUNT0 48 
19 10 WAREHOUSES33 18 COGOVERHEADACCOUNT3 INT13 4 20 (ID)����� COGS �����
33 10 WAREHOUSES18 COGOVERHEADACCOUNT0 48 
19 10 WAREHOUSES34 3 FAX4 CHAR20 20 3 ���
19 10 WAREHOUSES35 12 LOGCOUNTFLAG4 CHAR1 1 9 ���� ����
19 10 WAREHOUSES36 9 ASSETFLAG4 CHAR1 1 10 ?���� ����
19 10 WAREHOUSES37 9 EWARHSDES4 CHAR32 32 18 (������) ���� ����
19 10 WAREHOUSES38 8 ADDRESSA4 CHAR80 80 14 (������) �����
19 10 WAREHOUSES39 6 STATEA4 CHAR40 40 12 (������) ���
19 10 WAREHOUSES40 6 PHONEB3 INT13 4 12 (ID) ��� ���
33 10 WAREHOUSES6 PHONEB0 48 
19 10 WAREHOUSES41 7 STATEID3 INT13 4 10 (ID) �����
33 10 WAREHOUSES7 STATEID0 48 
19 10 WAREHOUSES42 13 FRANCHISEFLAG4 CHAR1 1 6 ?�����
19 10 WAREHOUSES43 4 PROJ3 INT13 4 11 (ID) ������
33 10 WAREHOUSES4 PROJ0 48 
19 10 WAREHOUSES44 8 MAINFLAG4 CHAR1 1 17 ������� ���� ����
19 10 WAREHOUSES45 8 LOANFLAG4 CHAR1 1 12 ?������ ����
19 10 WAREHOUSES46 8 SELLFLAG4 CHAR1 1 12 ?������ ����
19 10 WAREHOUSES47 10 ISSUEWARHS3 INT13 4 14 (ID) ���� ����
33 10 WAREHOUSES10 ISSUEWARHS0 48 
19 10 WAREHOUSES48 4 CUST3 INT13 4 10 (ID) �����
33 10 WAREHOUSES4 CUST0 48 
19 10 WAREHOUSES49 4 SORT3 INT3 4 8 ���� ���
33 10 WAREHOUSES4 SORT0 48 
19 10 WAREHOUSES50 11 CUSTINVFLAG4 CHAR1 1 15 ?���� ��� �����
19 10 WAREHOUSES51 12 TRANSITWARHS3 INT13 4 14 (ID) ���� ����
33 10 WAREHOUSES12 TRANSITWARHS0 48 
19 10 WAREHOUSES52 13 ISTRANSITFLAG4 CHAR1 1 10 ?���� ����
19 10 WAREHOUSES53 8 INACTIVE4 CHAR1 1 8 ?���� ��
19 10 WAREHOUSES54 13 SALESRATEFLAG4 CHAR1 1 20 ������ ���� ����� ��
19 10 WAREHOUSES55 9 WARHSTYPE3 INT13 4 13 (ID) ���� ���
33 10 WAREHOUSES9 WARHSTYPE0 48 
19 10 WAREHOUSES56 11 INV2ACCOUNT3 INT13 8 15 (ID)������ ����
33 10 WAREHOUSES11 INV2ACCOUNT0 48 
19 10 WAREHOUSES57 5 AISLE4 CHAR2 2 4 ����
19 10 WAREHOUSES58 4 ROWW4 CHAR2 2 3 ���
19 10 WAREHOUSES59 5 LEVEL4 CHAR2 2 4 ����
19 10 WAREHOUSES60 6 STZONE3 INT13 8 15 (ID) ����� ����
33 10 WAREHOUSES6 STZONE0 48 
19 10 WAREHOUSES61 12 ENTRANCEFLAG4 CHAR1 1 5 �����
19 10 WAREHOUSES62 8 EXITFLAG4 CHAR1 1 5 �����
19 10 WAREHOUSES63 8 ORIGFLAG4 CHAR1 1 11 ?���� �����
19 10 WAREHOUSES64 5 REAL14 REAL13 8 20 ���� ������ �� REAL1
33 10 WAREHOUSES5 REAL12 50 
19 10 WAREHOUSES65 4 INT33 INT3 8 19 ���� ������ �� INT3
33 10 WAREHOUSES4 INT30 48 
19 10 WAREHOUSES66 8 PIKORDER3 INT6 8 9 ����� ���
33 10 WAREHOUSES8 PIKORDER0 48 
19 10 WAREHOUSES67 8 VELOCITY4 CHAR1 1 11 ������ ����
19 10 WAREHOUSES68 4 INT13 INT13 8 19 ���� ������ �� INT1
33 10 WAREHOUSES4 INT10 48 
19 10 WAREHOUSES69 8 PUTORDER3 INT6 8 10 ������ ���
33 10 WAREHOUSES8 PUTORDER0 48 
19 10 WAREHOUSES70 4 CELL4 CHAR2 2 2 ��
19 10 WAREHOUSES71 14 ADCCHECKLETTER4 CHAR2 2 10 ������ ���
19 10 WAREHOUSES72 12 WTASKPATTERN3 INT13 8 20 ID ���� ������ �����
33 10 WAREHOUSES12 WTASKPATTERN0 48 
19 10 WAREHOUSES73 5 REAL24 REAL13 8 20 ���� ������ �� REAL2
33 10 WAREHOUSES5 REAL24 52 
19 10 WAREHOUSES74 4 INT23 INT13 8 19 ���� ������ �� INT2
33 10 WAREHOUSES4 INT20 48 
19 10 WAREHOUSES75 12 LABELPATTERN3 INT13 8 17 (ID) ������ �����
33 10 WAREHOUSES12 LABELPATTERN0 48 
19 10 WAREHOUSES76 11 ASSEMBLYACC3 INT13 8 17 (ID) ������ �����
33 10 WAREHOUSES11 ASSEMBLYACC0 48 
19 10 WAREHOUSES77 13 CONVERSIONACC3 INT13 8 16 (ID) ����� �����
33 10 WAREHOUSES13 CONVERSIONACC0 48 
19 10 WAREHOUSES78 12 LABORAPPLACC3 INT13 8 20 (ID)������� ����� .�
33 10 WAREHOUSES12 LABORAPPLACC0 48 
19 10 WAREHOUSES79 11 LABORVARACC3 INT13 8 19 (ID)����� ������ ��
33 10 WAREHOUSES11 LABORVARACC0 48 
19 10 WAREHOUSES80 14 MATERIALVARACC3 INT13 8 19 (ID)�"� ������ ����
33 10 WAREHOUSES14 MATERIALVARACC0 48 
19 10 WAREHOUSES81 15 OVERHEADAPPLACC3 INT13 8 20 (ID)������ ����� .��
33 10 WAREHOUSES15 OVERHEADAPPLACC0 48 
19 10 WAREHOUSES82 14 OVERHEADVARACC3 INT13 8 20 (ID)������ ������ ��
33 10 WAREHOUSES14 OVERHEADVARACC0 48 
19 10 WAREHOUSES83 13 STDCOSTVARACC3 INT13 8 18 (ID)��� ���� �����
33 10 WAREHOUSES13 STDCOSTVARACC0 48 
19 10 WAREHOUSES84 7 BINTYPE3 INT13 8 14 (ID) ����� ���
33 10 WAREHOUSES7 BINTYPE0 48 
19 10 WAREHOUSES85 9 MAXTQUANT3 INT17 8 11 ������ ����
33 10 WAREHOUSES9 MAXTQUANT3 51 
19 10 WAREHOUSES86 12 FORKLIFTFLAG4 CHAR1 1 13 ? ���� ������
19 10 WAREHOUSES87 14 AUTOSTORAGENUM4 CHAR6 6 20 ��������� ������ .��
19 10 WAREHOUSES88 8 ADDRESS25 RCHAR80 80 14 2 ���� - �����
19 10 WAREHOUSES89 8 ADDRESS35 RCHAR80 80 14 3 ���� - �����
19 10 WAREHOUSES90 11 AUTOSHPFLAG4 CHAR1 1 20 ? .���� ������ �����
19 10 WAREHOUSES91 7 COMPANY3 INT13 8 12 (ID) ���� ��
33 10 WAREHOUSES7 COMPANY0 48 
19 10 WAREHOUSES92 8 PACKTYPE4 CHAR1 1 9 ����� ���
19 10 WAREHOUSES93 15 ERP_DFLT4STATUS4 CHAR1 1 20 ��� ����� ����� .�.�
20 10 WAREHOUSES65 1 
21 10 WAREHOUSES1 5 WARHS1 
20 10 WAREHOUSES85 2 
21 10 WAREHOUSES2 9 WARHSNAME1 
21 10 WAREHOUSES2 7 LOCNAME2 
20 10 WAREHOUSES78 3 
21 10 WAREHOUSES3 4 TYPE1 
20 10 WAREHOUSES78 4 
21 10 WAREHOUSES4 10 EXTINVFLAG1 
20 10 WAREHOUSES78 5 
21 10 WAREHOUSES5 7 LOCNAME1 
20 10 WAREHOUSES78 6 
21 10 WAREHOUSES6 6 BONDED1 
20 10 WAREHOUSES78 7 
21 10 WAREHOUSES7 8 WARHSDES1 
20 10 WAREHOUSES78 8 
21 10 WAREHOUSES8 4 PROJ1 
21 10 WAREHOUSES8 8 MAINFLAG2 
20 10 WAREHOUSES78 9 
21 10 WAREHOUSES9 9 EWARHSDES1 
20 10 WAREHOUSES78 10 
21 10 WAREHOUSES10 4 CUST1 
20 10 WAREHOUSES78 11 
21 10 WAREHOUSES11 6 STZONE1 
13 13 ERPG_CUSTSHIP70 10 WAREHOUSES9 WARHSNAME0 9 WARHSNAME0 17 Sending Warehouse4 0 40 0 0 0 0 0 0 0 1 
24 11 10 POST-FIELD67 
8 13 ERPG_CUSTSHIP70 9 WARHSNAME10 POST-FIELD67 
14 13 ERPG_CUSTSHIP70 9 WARHSNAME10 POST-FIELD67 12 SELECT WARHS1 1 
14 13 ERPG_CUSTSHIP70 9 WARHSNAME10 POST-FIELD67 13 INTO :$.WARHS2 2 
14 13 ERPG_CUSTSHIP70 9 WARHSNAME10 POST-FIELD67 15 FROM WAREHOUSES3 3 
14 13 ERPG_CUSTSHIP70 9 WARHSNAME10 POST-FIELD67 22 WHERE WARHSNAME = :$.@4 4 
14 13 ERPG_CUSTSHIP70 9 WARHSNAME10 POST-FIELD67 18 AND LOCNAME = '0';5 5 
13 13 ERPG_CUSTSHIP70 13 ERPG_CUSTSHIP5 WARHS0 5 WARHS0 0 13 48 99 0 72 0 0 10 WAREHOUSES5 WARHS0 1 
17 10 ERPG_DOC_D17 ����� ����� �����11 GENERALLOAD73 89 0 0 89 0 0 1000 
57 10 ���� �����1 
58 1 10 ERPG_DOC_D73 
59 10 ERPG_DOC_D73 1 111 DOCUMENTS_D70 0 
59 10 ERPG_DOC_D73 1 212 TRANSORDER_D70 0 
60 10 ERPG_DOC_D73 4 INT112 TRANSORDER_D70 6 TQUANT2 0 0 0 0 0 
60 10 ERPG_DOC_D73 6 TEXT1011 DOCUMENTS_D70 9 WARHSNAME2 0 0 0 0 0 
60 10 ERPG_DOC_D73 5 TEXT611 DOCUMENTS_D70 8 CUSTNAME1 0 0 0 0 0 
60 10 ERPG_DOC_D73 7 TEXT6@212 TRANSORDER_D70 8 PARTNAME1 0 0 0 0 0 
18 13 CUSTNOTETYPES0 
56 13 CUSTNOTETYPES11 ������ ����0 
19 13 CUSTNOTETYPES1 12 CUSTNOTETYPE3 INT13 4 14 (ID) ����� ���
33 13 CUSTNOTETYPES12 CUSTNOTETYPE0 48 
19 13 CUSTNOTETYPES2 15 CUSTNOTETYPEDES5 RCHAR8 8 9 ����� ���
19 13 CUSTNOTETYPES3 7 OUTLOOK4 CHAR1 1 15 ?Outlook-� ����
19 13 CUSTNOTETYPES4 10 MARKETFLAG4 CHAR1 1 5 ?����
19 13 CUSTNOTETYPES5 11 SENDMESSAGE4 CHAR1 1 10 ������ ���
19 13 CUSTNOTETYPES6 12 ERPG_OPEN_SH4 CHAR1 1 7 OPEN_SH
20 13 CUSTNOTETYPES65 1 
21 13 CUSTNOTETYPES1 12 CUSTNOTETYPE1 
20 13 CUSTNOTETYPES85 2 
21 13 CUSTNOTETYPES2 15 CUSTNOTETYPEDES1 
1013 13 CUSTNOTETYPES70 13 CUSTNOTETYPES12 ERPG_OPEN_SH0 12 ERPG_OPEN_SH0 0 1 0 500 10 0 0 0 0 0 0 1 
12 18 ERPG_SITEINTERFACE70 13 ERPG_CUSTSHIP70 10 
EOF
echo End of Upgrade 7.0.999
